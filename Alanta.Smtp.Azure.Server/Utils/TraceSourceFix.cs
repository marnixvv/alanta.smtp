﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.WindowsAzure.ServiceRuntime;

namespace Alanta.Smtp.Azure.Server.Utils
{
    public static class AzureUtils
    {
        private const string DevFabricTraceListenerFullName = "Microsoft.ServiceHosting.Tools.DevelopmentFabric.Runtime.DevelopmentFabricTraceListener";

        public static TraceSource AddDevFabricTraceListener( this TraceSource traceSource )
        {
            var alreadyInTraceSource = GetDevFabricTraceListeners( traceSource.Listeners );

            if ( alreadyInTraceSource.Any() )
                return traceSource;

            var alreadyInTrace = GetDevFabricTraceListeners( Trace.Listeners );

            var devFabricTraceListener = alreadyInTrace.FirstOrDefault();
            if ( devFabricTraceListener != null )
            {
                traceSource.Listeners.Add( devFabricTraceListener );
            }
            return traceSource;
        }

        private static IEnumerable<TraceListener> GetDevFabricTraceListeners( TraceListenerCollection listeners )
        {
            var result = from TraceListener listener in listeners.Cast<TraceListener>()
                         where IsDevFabricTraceListener( listener )
                         select listener;

            return result;
        }

        private static bool IsDevFabricTraceListener( TraceListener listener )
        {
            return ( string.Equals( listener.GetType().FullName, DevFabricTraceListenerFullName, StringComparison.OrdinalIgnoreCase ) );
        }

        public static bool IsRunningInDevFabric()
        {
            Guid guidId;
            // Can't parse into guid? We're in Dev Fabric
            return  !Guid.TryParse( RoleEnvironment.DeploymentId, out guidId );
        }
    }
}
