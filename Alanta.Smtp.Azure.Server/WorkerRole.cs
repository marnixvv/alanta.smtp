﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Net;
using Alanta.Smtp.Azure.Server.Utils;
using Alanta.Smtp.Dns;
using Alanta.Smtp.Logging;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;

namespace Alanta.Smtp.Azure.Server
{
    public class WorkerRole : RoleEntryPoint
    {
        private SmtpServer _server;

        public WorkerRole()
        {
        }

        private TraceSource CreateTraceSource()
        {
            var traceSwitch = new SourceSwitch( "Alanta.Smtp", "Alanta SMTP Server" );
            var source = new TraceSource( "Alanta.Smtp" ) { Switch = traceSwitch };
            if( AzureUtils.IsRunningInDevFabric() )
            {
                source.AddDevFabricTraceListener();
            }
            return source;
        }

        public override void Run()
        {
            var endpoint = RoleEnvironment.CurrentRoleInstance.InstanceEndpoints["Smtp"].IPEndpoint;

            Trace.WriteLine( string.Format( "Smtp server starting on {0}:{1}", endpoint.Address, endpoint.Port ), "Information" );

            try
            {
	            var logger = new TraceLogger(CreateTraceSource());

					_server =
                  new SmtpServer(
                     new DefaultSmtpSessionFactory(new AzureDnsResolver("alanta.nl", new DefaultDnsResolver(new[] { "8.8.8.8" })), ctx => new PrefixLoggerDecorator( logger, ctx.Id.ToString() ) ), 
							logger, endpoint);
                _server.Run();
            }
            catch (Exception ex)
            {
                Trace.TraceError( ex.ToString() );
                throw;
            }
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections 
            ServicePointManager.DefaultConnectionLimit = 12;

            // For information on handling configuration changes
            // see the MSDN topic at http://go.microsoft.com/fwlink/?LinkId=166357.
            RoleEnvironment.Changing += RoleEnvironmentChanging;

            return base.OnStart();
        }

        public override void OnStop()
        {
            _server.Stop();
            base.OnStop();
        }

        private void RoleEnvironmentChanging( object sender, RoleEnvironmentChangingEventArgs e )
        {
            // If a configuration setting is changing
            if ( e.Changes.Any( change => change is RoleEnvironmentConfigurationSettingChange ) )
            {
                _server.Stop();
                // Set e.Cancel to true to restart this role instance
                e.Cancel = true;
            }
        }
    }
}
