﻿using System;
using System.Net;
using Alanta.Smtp.Dns;

namespace Alanta.Smtp.Azure.Server
{
    public class AzureDnsResolver : IDnsResolver
    {
        private readonly string _localHostName;
        private readonly IDnsResolver _fallbackResolver;

        public AzureDnsResolver( string localHostName, IDnsResolver fallbackResolver )
        {
            if ( localHostName == null ) throw new ArgumentNullException( "localHostName" );
            if ( fallbackResolver == null ) throw new ArgumentNullException( "fallbackResolver" );
            _localHostName = localHostName;
            _fallbackResolver = fallbackResolver;
        }

        public string Resolve( IPAddress ip )
        {
            if( ip.Equals( new IPAddress(new byte[]{ 127, 255, 0, 0 }) ) )
            {
                return _localHostName;
            }

            return _fallbackResolver.Resolve( ip );
        }

	    public IPAddress[] Resolve(string domain)
	    {
		    throw new NotImplementedException();
	    }

	    public MxEntry[] GetMxRecords( string domain )
       {
          return _fallbackResolver.GetMxRecords( domain );
       }

	    public string[] ReverseLookup(IPAddress ip)
	    {
			return _fallbackResolver.ReverseLookup( ip );
		}

	    public string[] GetTxtRecords(string domain)
	    {
			return _fallbackResolver.GetTxtRecords( domain );
		}
    }
}
