@echo off
::
:: Run this script from a VisualStudio command prompt. Syntax:
:: 
:: build [Configuration]
::
:: Where [Configuration] is a build configuration like Debug or Release (default).
::

setlocal
:: Ensure MSBuild is available
IF "%VisualStudioVersion%"=="" SET PATH=C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\;%PATH%


SET configuration=%1
IF "%configuration%"=="" (
  SET configuration=Release
)

:: Default target is build
SET target=%2
IF "%target%"=="" (
  SET target=build
)

set buildstarttime=%time%
ECHO %target% in %configuration% configuration
msbuild /t:restore "%~dp0\Alanta.Smtp.sln"
msbuild /t:%target% /p:Configuration=%configuration% "%~dp0\Alanta.Smtp.sln"

ECHO Started: %buildstarttime%
endlocal
