# Alanta.Smtp aka Project Tati #

The main goal of this project is to provide a comprehensive, standards compliant SMTP implementation for .NET.
The implementation can serve as a Mail Transfer Agent (MTA) for regular e-mail messaging or as a platform for building e-mail based application or messaging solutions.

![Build status](https://ci.appveyor.com/api/projects/status/bitbucket/marnixvv/alanta.smtp?svg=true)

## What can it do? ##
Current implementation supports:

* Basic SMTP commands:
    * EHLO/HELO
    * MAIL FROM
    * RCPT to
    * DATA
    * QUIT
    * STARTTLS
    * AUTH
    * RSET
* Receiving e-mail
* TLS
* Plain text authentication (over TLS only)
* 8-bit messages
* Forwarding e-mail to another MTA
* Stand-alone host
* Hosting in Azure

The project in it's current state is *NOT* suitable for production use as an MTA. When used with the relay functionality, it's basically an open relay.

## Roadmap ##
Features scheduled or currently being worked on:

* Improved tracing and logging
* Relay rules (incl. White/Black/Greylisting, MX checks etc.)
* Command timeouts
* Delivery Status Notifications (DSN)
* Performance and stress tests
* Samples & Docs
* Command pipelining
* Extended status codes

## How do I get set up? ##

* Current implementation is on .NET 4.6 / .NET Standard 1.6
* Development with any current .NET IDE (VS 2017, VS Code, Rider)
* On a Windows box, in a Visual Studio command box `build` to build, `package` to build the Nuget packages, `test` to run all tests.
* To build the Azure role, Azure SDK v2.9 is needed

## Contribution guidelines ##

* You're welcome to send pull requests. Please create an issue first and include a unit test with your pull request to verify your code.
* Massive refactorings, code cleanups etc. will be rejected unless explictly agreed upon
* Adding additional Nuget package dependencies to the main assemblies is strongly discouraged

### Who do I talk to? ##

Marnix van Valen
  on twitter : [@marnixvanvalen](https://twitter.com/marnixvanvalen)
  e-mail : marnix [at] alanta [dot] nl