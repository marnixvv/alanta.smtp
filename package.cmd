@echo off

setlocal
:: Ensure MSBuild is available
IF "%VisualStudioVersion%"=="" SET PATH=C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\;%PATH%

where /q gitversion
IF ERRORLEVEL 1 (
	echo Warning : GitVersion command line tool not found
	echo.
	IF '%_PKG_VERSION%'=='' SET _PKG_VERSION='1.0.0-beta'
	SET /P _PKG_VERSION="Versie [%_PKG_VERSION%] "
) ELSE (
	for /f %%i in ('gitversion /output json /showvariable semver') do set _PKG_VERSION=%%i
)

SET configuration=Release

:: Cleanup
if not exist "%~dp0Output" mkdir "%~dp0Output"

del /S /F /Q "%~dp0Output"

:: Build

call build.cmd %configuration% build /p:PackageVersion=%_PKG_VERSION%

:: Package

setlocal
SET BUILD_PARAMS=/t:pack /p:PackageOutputPath="%~dp0Output" /p:Configuration=%configuration%  /p:PackageVersion=%_PKG_VERSION%

msbuild "%~dp0Alanta.Dns\Alanta.Dns.csproj" %BUILD_PARAMS% 
msbuild "%~dp0Alanta.Sasl\Alanta.Sasl.csproj" %BUILD_PARAMS% 
msbuild "%~dp0Alanta.Smtp\Alanta.Smtp.csproj" %BUILD_PARAMS% 

endlocal