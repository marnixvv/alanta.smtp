﻿using System;
using System.Net;
using Alanta.DNS;
using Should;
using Xunit;

namespace Alanta.Dns.Test
{
    public class When_performing_a_dns_query
    {
		[Fact]
	    public void It_should_return_a_configuration_error_when_the_dns_server_is_unreachable()
	    {
		   // Arrange
			var resolver = new TcpDnsQueryResolver(new[] {new IPEndPoint(IPAddress.Parse("10.0.0.1"), 53)}, 2);
			resolver.OnVerbose += (o, args) => Console.WriteLine(args.Message);
			var sut = new DnsResolver( resolver );

			// Act
		    var result = sut.Query("google.com", QType.A);

		    // Assert
            Response.Timeout.Success.ShouldBeFalse();
			result.ShouldNotBeNull();
			Console.WriteLine( result.ErrorMessage );
			result.Success.ShouldBeFalse();
			result.ErrorMessage.ShouldNotBeEmpty();
	    }
    }
}
 