﻿using System;
using Alanta.DNS;
using Moq;
using Xunit;

namespace Alanta.Dns.Test
{
    public class When_caching_DNS_queries
    {
	 	[Fact]
	    public void It_should_not_store_errors()
	    {
		    // Arrange
		    var mock = new Mock<IDnsQueryResolver>( MockBehavior.Loose );
		    mock.Setup(m => m.GetResponse(It.IsAny<Request>()))
				.Returns(new Response {ErrorMessage = "Oops"}).Verifiable();

			 var sut = new CachingDnsResolver( mock.Object );

			 // Act - query twice, result should not be cached so call to DNS Query resolver should happen twice
		    var result1 = sut.Query("example.com", QType.A);
			 var result2 = sut.Query("example.com", QType.A);

			// Assert
			mock.Verify( m => m.GetResponse(It.IsAny<Request>()), Times.Exactly(2));
	    }

		[Fact]
		public void It_should_store_succesfull_queries()
		{
			// Arrange
			var mock = new Mock<IDnsQueryResolver>(MockBehavior.Loose);
			mock.Setup(m => m.GetResponse(It.IsAny<Request>()))
			  .Returns(new Response
			  {
				  Header = { RCODE = RCode.NoError },
				  TimeStamp = DateTime.UtcNow,
				  Questions = { new Question( "example.com", QType.A, QClass.IN )},
				  Answers = { new AnswerRR { TTL = 2000 } }
			  }).Verifiable();

			var sut = new CachingDnsResolver(mock.Object);

			// Act - query twice, result should not be cached so call to DNS Query resolver should happen twice
			var result1 = sut.Query("example.com", QType.A);
			var result2 = sut.Query("example.com", QType.A);

			// Assert
			mock.Verify(m => m.GetResponse(It.IsAny<Request>()), Times.Exactly(1));
		}

		[Fact]
		public void It_should_not_return_expired_results()
		{
			// Arrange
			var mock = new Mock<IDnsQueryResolver>(MockBehavior.Loose);
			mock.Setup(m => m.GetResponse(It.IsAny<Request>()))
			  .Returns(new Response
			  {
				  Header = { RCODE = RCode.NoError },
				  TimeStamp = DateTime.UtcNow,
				  Questions = { new Question("example.com", QType.A, QClass.IN) },
				  Answers = { new AnswerRR { TTL = 2 } }
			  }).Verifiable();

			var sut = new CachingDnsResolver(mock.Object);

			// Act - query twice, result should not be cached so call to DNS Query resolver should happen twice
			var result1 = sut.Query("example.com", QType.A);
			// Mess with the space time continuum
			SystemTime.UtcNow = () => DateTime.UtcNow.AddSeconds(3);
			var result2 = sut.Query("example.com", QType.A);

			// Assert
			mock.Verify(m => m.GetResponse(It.IsAny<Request>()), Times.Exactly(2));
		}
	}
}
