using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;

namespace Alanta.DNS
{
	public class Response
	{
		/// <summary>
		/// List of Question records
		/// </summary>
		public List<Question> Questions { get; } = new List<Question>();
		/// <summary>
		/// List of AnswerRR records
		/// </summary>
		public List<AnswerRR> Answers { get; } = new List<AnswerRR>();
		/// <summary>
		/// List of AuthorityRR records
		/// </summary>
		public List<AuthorityRR> Authorities { get; } = new List<AuthorityRR>();
		/// <summary>
		/// List of AdditionalRR records
		/// </summary>
		public List<AdditionalRR> Additionals { get; } = new List<AdditionalRR>();

		public Header Header { get; set; }

		/// <summary>
		/// True if the query succesfully completed.
		/// </summary>
		public bool Success => Header?.RCODE == RCode.NoError;

		/// <summary>
		/// Error message, empty when no error
		/// </summary>
		public string ErrorMessage { get; set; }

		/// <summary>
		/// The Size of the message
		/// </summary>
		public int MessageSize { get; set; }

		/// <summary>
		/// UTC TimeStamp when cached
		/// </summary>
		public DateTime TimeStamp { get; set; }

		/// <summary>
		/// Server that delivered this response
		/// </summary>
		public IPEndPoint Server { get; set; }


		public Response() : this( new IPEndPoint(0,0), null )
		{
			Header = new Header();
		}

		public Response(IPEndPoint iPEndPoint, byte[] data)
		{
			ErrorMessage = "";
			Server = iPEndPoint;
			TimeStamp = SystemTime.UtcNow();
			MessageSize = data?.Length ?? 0;

			if (data != null)
			{
				RecordReader rr = new RecordReader(data);
				Header = new Header(rr);

				for (int intI = 0; intI < Header.QDCOUNT; intI++)
				{
					Questions.Add(new Question(rr));
				}

				for (int intI = 0; intI < Header.ANCOUNT; intI++)
				{
					Answers.Add(new AnswerRR(rr));
				}

				for (int intI = 0; intI < Header.NSCOUNT; intI++)
				{
					Authorities.Add(new AuthorityRR(rr));
				}
				for (int intI = 0; intI < Header.ARCOUNT; intI++)
				{
					Additionals.Add(new AdditionalRR(rr));
				}
			}
		}

		/// <summary>
		/// List of MX records in this response, sorted by preference (most preferred first).
		/// </summary>
		public RecordMX[] RecordsMX => Answers.Select(a => a.RECORD).OfType<RecordMX>().OrderByDescending( m => m.PREFERENCE ).ToArray();

        /// <summary>
		/// List of CERT records in this response.
		/// </summary>
        public RecordCERT[] RecordsCERT => Answers.Select(a => a.RECORD).OfType<RecordCERT>().ToArray();

        /// <summary>
        /// Gets all records of the specified type in  this response.
        /// </summary>
        public TRecord[] Records<TRecord>() where TRecord: Record 
			=> Answers.Select(a => a.RECORD).OfType<TRecord>().ToArray();
		
		/// <summary>
		/// All resource records in this response.
		/// </summary>
		public RR[] RecordsRR => Answers.Cast<RR>().Concat(Authorities).Concat(Additionals).ToArray();

		public static Response Timeout => new Response()
		{
		    Header = DNS.Header.Error(),
		    ErrorMessage = "Timeout occurred while executing query."
		};
	}
}
