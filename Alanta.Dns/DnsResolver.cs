using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

/*
 * Network Working Group                                     P. Mockapetris
 * Request for Comments: 1035                                           ISI
 *                                                            November 1987
 *
 *           DOMAIN NAMES - IMPLEMENTATION AND SPECIFICATION
 *
 */

namespace Alanta.DNS
{
	/// <summary>
	/// DnsResolver is the main class to do DNS query lookups
	/// </summary>
	public class DnsResolver : IDnsResolver
	{
		private ushort m_unique;

		private readonly IDnsQueryResolver _dnsQueryResolverImpl;


		/// <summary>
		/// Constructor of DnsResolver using DNS servers specified.
		/// </summary>
		/// <param name="dnsQueryResolverImpl"></param>
		public DnsResolver(IDnsQueryResolver dnsQueryResolverImpl)
		{
			if (dnsQueryResolverImpl == null) throw new ArgumentNullException(nameof(dnsQueryResolverImpl));	
			
			_dnsQueryResolverImpl = dnsQueryResolverImpl;

			m_unique = (ushort)(new Random()).Next(ushort.MinValue, ushort.MaxValue);
		}

		/// <summary>
		/// Do Query on specified DNS servers
		/// </summary>
		/// <param name="name">Name to query</param>
		/// <param name="qtype">Question type</param>
		/// <param name="qclass">Class type</param>
		/// <param name="recursion"></param>
		/// <returns>Response of the query</returns>
		public virtual Response Query(string name, QType qtype, QClass qclass = QClass.IN, bool recursion = true)
		{
			var question = new Question(name, qtype, qclass);

			var request = new Request
			{
				Header =
				{
					ID = m_unique,
					RD = recursion
				},
				Questions = { question }
			};
			var response = _dnsQueryResolverImpl.GetResponse(request);
			m_unique++;

			return response;
		}

		private IPHostEntry MakeEntry(string HostName)
		{
			IPHostEntry entry = new IPHostEntry {HostName = HostName};

			Response response = Query(HostName, QType.A, QClass.IN);

			// fill AddressList and aliases
			List<IPAddress> addressList = new List<IPAddress>();
			List<string> aliases = new List<string>();
			foreach (AnswerRR answerRR in response.Answers)
			{
				if (answerRR.Type == Type.A)
				{
					// answerRR.RECORD.ToString() == (answerRR.RECORD as RecordA).Address
					addressList.Add(IPAddress.Parse((answerRR.RECORD.ToString())));
					entry.HostName = answerRR.NAME;
				}
				else
				{
					if (answerRR.Type == Type.CNAME)
						aliases.Add(answerRR.NAME);
				}
			}
			entry.AddressList = addressList.ToArray();
			entry.Aliases = aliases.ToArray();

			return entry;
		}

		// MvV Todo: Support for async invocation

		/// <summary>
		///		Resolves an IP address to an System.Net.IPHostEntry instance.
		/// </summary>
		/// <param name="ip">An IP address.</param>
		/// <returns>
		///		An System.Net.IPHostEntry instance that contains address information about
		///		the host specified in address.
		///</returns>
		public IPHostEntry GetHostEntry(IPAddress ip)
		{
			Response response = Query(DnsUtilities.GetArpaFromIp(ip), QType.PTR, QClass.IN);
			return response.Answers.OfType<RecordPTR>().Select(a => MakeEntry(a.PTRDNAME)).FirstOrDefault()
				?? new IPHostEntry();
		}

		/// <summary>
		///		Resolves a host name or IP address to an System.Net.IPHostEntry instance.
		/// </summary>
		/// <param name="hostNameOrAddress">The host name or IP address to resolve.</param>
		/// <returns>
		///		An System.Net.IPHostEntry instance that contains address information about
		///		the host specified in hostNameOrAddress. 
		///</returns>
		public IPHostEntry GetHostEntry(string hostNameOrAddress)
		{
			IPAddress iPAddress;
			if (IPAddress.TryParse(hostNameOrAddress, out iPAddress))
				return GetHostEntry(iPAddress);
			else
				return MakeEntry(hostNameOrAddress);
		}
	}
}