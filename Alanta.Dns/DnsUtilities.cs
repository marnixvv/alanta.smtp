using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;

namespace Alanta.DNS
{
	public static class DnsUtilities
	{
		/// <summary>
		/// Default DNS port
		/// </summary>
		public const int DefaultPort = 53;

		/// <summary>
		/// Gets list of OPENDNS servers
		/// </summary>
		public static readonly IPEndPoint[] DefaultDnsServers =
			{
				new IPEndPoint(IPAddress.Parse("208.67.222.222"), DefaultPort),
				new IPEndPoint(IPAddress.Parse("208.67.220.220"), DefaultPort)
			};

		/// <summary>
		/// Gets a list of default DNS servers used on the Windows machine.
		/// </summary>
		/// <returns></returns>
		public static IPEndPoint[] GetDnsServers()
		{
			List<IPEndPoint> list = new List<IPEndPoint>();

			NetworkInterface[] adapters = NetworkInterface.GetAllNetworkInterfaces();
			foreach (NetworkInterface n in adapters)
			{
				if (n.OperationalStatus == OperationalStatus.Up)
				{
					IPInterfaceProperties ipProps = n.GetIPProperties();
					foreach (IPAddress ipAddr in ipProps.DnsAddresses)
					{
						IPEndPoint entry = new IPEndPoint(ipAddr, DefaultPort);
						if (!list.Contains(entry))
							list.Add(entry);
					}

				}
			}
			return list.ToArray();
		}

		/// <summary>
		/// Translates the IPV4 or IPV6 address into an arpa address
		/// </summary>
		/// <param name="ip">IP address to get the arpa address form</param>
		/// <returns>The 'mirrored' IPV4 or IPV6 arpa address</returns>
		public static string GetArpaFromIp(IPAddress ip)
		{
			if (ip.AddressFamily == AddressFamily.InterNetwork)
			{
				StringBuilder sb = new StringBuilder();
				sb.Append("in-addr.arpa.");
				foreach (byte b in ip.GetAddressBytes())
				{
					sb.Insert(0, string.Format("{0}.", b));
				}
				return sb.ToString();
			}
			if (ip.AddressFamily == AddressFamily.InterNetworkV6)
			{
				StringBuilder sb = new StringBuilder();
				sb.Append("ip6.arpa.");
				foreach (byte b in ip.GetAddressBytes())
				{
					sb.Insert(0, string.Format("{0:x}.", (b >> 4) & 0xf));
					sb.Insert(0, string.Format("{0:x}.", (b >> 0) & 0xf));
				}
				return sb.ToString();
			}
			return "?";
		}

		public static string GetArpaFromEnum(string strEnum)
		{
			StringBuilder sb = new StringBuilder();
			string Number = System.Text.RegularExpressions.Regex.Replace(strEnum, "[^0-9]", "");
			sb.Append("e164.arpa.");
			foreach (char c in Number)
			{
				sb.Insert(0, string.Format("{0}.", c));
			}
			return sb.ToString();
		}
	}
}