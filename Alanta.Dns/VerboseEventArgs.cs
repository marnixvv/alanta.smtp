using System;

namespace Alanta.DNS
{
	public class VerboseEventArgs : EventArgs
	{
		public string Message { get; }
		public VerboseEventArgs(string message)
		{
			this.Message = message;
		}
	}
}