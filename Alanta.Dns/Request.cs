using System.Collections.Generic;

namespace Alanta.DNS
{
	public class Request
	{
		public Header Header { get; }
		public List<Question> Questions { get; }

		public Request()
		{
			Header = new Header
			{
				OPCODE = OPCode.Query,
				QDCOUNT = 0
			};

			Questions = new List<Question>();
		}

		public byte[] Data
		{
			get
			{
				List<byte> data = new List<byte>();
				Header.QDCOUNT = (ushort)Questions.Count;
				data.AddRange(Header.Data);
				foreach (Question q in Questions)
					data.AddRange(q.Data);
				return data.ToArray();
			}
		}
	}
}
