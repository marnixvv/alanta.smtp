using System;

namespace Alanta.DNS
{
	public interface IDnsQueryResolver
	{
		Response GetResponse(Request request);


		/// <summary>
		/// Verbose messages from internal operations
		/// </summary>
		event EventHandler<VerboseEventArgs> OnVerbose;
	}
}