﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Alanta.Dns")]
[assembly: AssemblyDescription("DNS client to support Alanta.Smtp")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Alanta")]
[assembly: AssemblyProduct("Alanta.Smtp")]
[assembly: AssemblyCopyright("Copyright © Alanta  2016-2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("fb79a26c-c91c-40ef-95ac-ab57abba2294")]

