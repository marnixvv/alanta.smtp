﻿using System.Net;

namespace Alanta.DNS
{
	public interface IDnsResolver
	{
		IPHostEntry GetHostEntry(string hostNameOrAddress);
		IPHostEntry GetHostEntry(IPAddress ip);
		Response Query(string name, QType qtype, QClass qclass = QClass.IN, bool recursion = true);
	}
}