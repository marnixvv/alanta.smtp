using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace Alanta.DNS
{
	public class TcpDnsQueryResolver : IDnsQueryResolver
	{
		private readonly IPEndPoint[] _servers;
		private readonly uint _retries;
		private readonly TimeSpan _timeout;

		public TcpDnsQueryResolver(IPEndPoint[] servers, uint? retries = null, TimeSpan? timeout = null)
		{
			if (servers == null) throw new ArgumentNullException(nameof(servers));
			if (servers.Length == 0) throw new ArgumentException("Please specify at least one DNS server to query", nameof(servers));
			if (timeout != null && timeout.Value.TotalMilliseconds <= 0)
				throw new ArgumentOutOfRangeException(nameof(timeout), timeout, "The timeout value must be greater than 1 ms.");

			if (retries.HasValue && retries.Value < 1)
				throw new ArgumentOutOfRangeException(nameof(retries), "Number of retries must be >= 1");

			_servers = servers;
			_retries = retries ?? 3;
			_timeout = timeout ?? TimeSpan.FromSeconds(1);
		}

		private void Verbose(string format, params object[] args)
		{
			OnVerbose?.Invoke(this, new VerboseEventArgs(string.Format(format, args)));
		}

		/// <summary>
		/// Verbose messages from internal operations
		/// </summary>
		public event EventHandler<VerboseEventArgs> OnVerbose;

		public Response GetResponse(Request request)
		{
			//System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
			//sw.Start();

			for (int intAttempts = 0; intAttempts < _retries; intAttempts++)
			{
				for (int intDnsServer = 0; intDnsServer < _servers.Length; intDnsServer++)
				{
					var server = _servers[intDnsServer];

					TcpClient tcpClient = new TcpClient
					{
						ReceiveTimeout = (int)_timeout.TotalMilliseconds
					};

					try
					{
						var success = tcpClient.ConnectAsync(server.Address, server.Port).Wait(1000);

						if (!success || !tcpClient.Connected)
						{
							((IDisposable)tcpClient).Dispose();
							Verbose(string.Format(";; Connection to nameserver {0} failed", (intDnsServer + 1)));
							continue;
						}

						Stream bs =
#if DNXCORE50
							// MvV : BufferedStream is not supported in Core CLR yet
							// see https://github.com/dotnet/corefx/pull/4456
							tcpClient.GetStream();
#else
							new BufferedStream(tcpClient.GetStream());
#endif

						byte[] data = request.Data;
						bs.WriteByte((byte)((data.Length >> 8) & 0xff));
						bs.WriteByte((byte)(data.Length & 0xff));
						bs.Write(data, 0, data.Length);
						bs.Flush();

						Response TransferResponse = new Response();
						int intSoa = 0;
						int intMessageSize = 0;

						//Debug.WriteLine("Sending "+ (request.Length+2) + " bytes in "+ sw.ElapsedMilliseconds+" mS");

						while (true)
						{
							int intLength = bs.ReadByte() << 8 | bs.ReadByte();
							if (intLength <= 0)
							{
								((IDisposable)tcpClient).Dispose();
								Verbose(string.Format(";; Connection to nameserver {0} failed", (intDnsServer + 1)));
								break;
							}

							intMessageSize += intLength;

							data = new byte[intLength];
							bs.Read(data, 0, intLength);
							Response response = new Response(_servers[intDnsServer], data);

							//Debug.WriteLine("Received "+ (intLength+2)+" bytes in "+sw.ElapsedMilliseconds +" mS");

							if (!response.Success)
								return response;

							if (response.Questions[0].QType != QType.AXFR)
							{
								return response;
							}

							// Zone transfer!!

							if (TransferResponse.Questions.Count == 0)
								TransferResponse.Questions.AddRange(response.Questions);
							TransferResponse.Answers.AddRange(response.Answers);
							TransferResponse.Authorities.AddRange(response.Authorities);
							TransferResponse.Additionals.AddRange(response.Additionals);

							if (response.Answers[0].Type == Type.SOA)
								intSoa++;

							if (intSoa == 2)
							{
								TransferResponse.Header.QDCOUNT = (ushort)TransferResponse.Questions.Count;
								TransferResponse.Header.ANCOUNT = (ushort)TransferResponse.Answers.Count;
								TransferResponse.Header.NSCOUNT = (ushort)TransferResponse.Authorities.Count;
								TransferResponse.Header.ARCOUNT = (ushort)TransferResponse.Additionals.Count;
								TransferResponse.MessageSize = intMessageSize;
								return TransferResponse;
							}
						}
					} // try
					catch (SocketException)
					{
						continue; // next try
					}
					finally
					{
						// close the socket
						((IDisposable)tcpClient).Dispose();
					}
				}
			}
			return Response.Timeout;
		}
	}
}