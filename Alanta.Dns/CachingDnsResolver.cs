using System;
using System.Collections.Generic;

namespace Alanta.DNS
{
	public class CachingDnsResolver : DnsResolver
	{
		private readonly Dictionary<string, Response> _responseCache;

		public CachingDnsResolver(IDnsQueryResolver dnsQueryResolverImpl ) : base( dnsQueryResolverImpl )
		{
			_responseCache = new Dictionary<string, Response>();
		}

		public override Response Query(string name, QType qtype, QClass qclass = QClass.IN, bool recursion = true)
		{
			Question question = new Question(name, qtype, qclass);
			Response response = SearchInCache(question);
			if (response != null)
				return response;

			response = base.Query(name, qtype, qclass, recursion);
			AddToCache(response);

			return response;
		}

		/// <summary>
		/// Clear the resolver cache
		/// </summary>
		public void ClearCache()
		{
			lock (_responseCache)
			{
				_responseCache.Clear();
			}
		}

		private static string CreateCacheKey(Question question)
		{
			return CreateCacheKey(question.QClass, question.QType, question.QName);
		}

		private static string CreateCacheKey(QClass qclass, QType qtype, string qname)
		{
			return qclass + "-" + qtype + "-" + qname;
		}

		private Response SearchInCache(Question question)
		{
			string strKey = CreateCacheKey(question);

			Response response;

			lock (_responseCache)
			{
				if (!_responseCache.ContainsKey(strKey))
					return null;

				response = _responseCache[strKey];
			}
			int timeLived = (int) Math.Round((SystemTime.UtcNow() - response.TimeStamp).TotalSeconds);
			foreach (RR rr in response.RecordsRR)
			{
				if (rr.TTL < timeLived) 
					return null; // out of date
			}
			return response;
		}

		private void AddToCache(Response response)
		{
			// No question, no caching
			if (response.Questions.Count == 0)
				return;

			// Only cached non-error responses
			if (response.Header.RCODE != RCode.NoError)
				return;

			Question question = response.Questions[0]; // MvV - todo : ouch, what about other questions?

			string strKey = CreateCacheKey(question);

			lock (_responseCache) // MvV - todo : this is not properly synchronized !!!
			{
				if (_responseCache.ContainsKey(strKey))
				{
					_responseCache[strKey] = response;
				}
				else
				{
					_responseCache.Add(strKey, response);
				}
			}
		}

	
	}
}