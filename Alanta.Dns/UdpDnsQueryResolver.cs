using System;
using System.Net;
using System.Net.Sockets;

namespace Alanta.DNS
{
	public class UdpDnsQueryResolver : IDnsQueryResolver
	{
		private readonly IPEndPoint[] _servers;
		private readonly uint _retries;
		private readonly TimeSpan _timeout;

		public UdpDnsQueryResolver(IPEndPoint[] servers, uint? retries = null, TimeSpan? timeout = null)
		{
			if (servers == null) throw new ArgumentNullException(nameof(servers));
			if(servers.Length == 0) throw new ArgumentException("Please specify at least one DNS server to query", nameof(servers));
			if( timeout != null && timeout.Value.TotalMilliseconds <= 0)
				throw new ArgumentOutOfRangeException(nameof(timeout), timeout, "The timeout value must be greater than 1 ms.");

			if (retries.HasValue && retries.Value < 1)
				throw new ArgumentOutOfRangeException(nameof(retries), "Number of retries must be >= 1");

			_servers = servers;
			_retries = retries ?? 3;
			_timeout = timeout ?? TimeSpan.FromSeconds(1);
		}

		private void Verbose(string format, params object[] args)
		{
			OnVerbose?.Invoke(this, new VerboseEventArgs(string.Format(format, args)));
		}

		/// <summary>
		/// Verbose messages from internal operations
		/// </summary>
		public event EventHandler<VerboseEventArgs> OnVerbose;

		public Response GetResponse(Request request)
		{
			for (int intAttempts = 0; intAttempts < _retries; intAttempts++)
			{
				for (var intDnsServer = 0; intDnsServer < _servers.Length; intDnsServer++)
				{
					var server = _servers[intDnsServer];
					try
					{
						using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp))
						{
							// RFC1035 max. size of a UDP datagram is 512 bytes
							byte[] responseMessage = new byte[512];
							socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, (int)_timeout.TotalMilliseconds);
							// MvV todo : make this async
							socket.SendTo(request.Data, server);
							int intReceived = socket.Receive(responseMessage);
							byte[] data = new byte[intReceived]; // MvV: todo: should cap at 512 bytes or throw
							Array.Copy(responseMessage, data, intReceived);
							return new Response(server, data);
						}
					}
					catch (SocketException)
					{
						Verbose(string.Format(";; Connection to nameserver {0} failed", (intDnsServer + 1)));
						continue; // next try
					}
						// MvV : todo what about other exceptions?
				}
			}
			return Response.Timeout;
		}
	}
}