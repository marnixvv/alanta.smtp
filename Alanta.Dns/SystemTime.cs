﻿using System;

namespace Alanta.DNS
{
	/// <summary>
	/// System time access (ambient context). The main purpose of this class is to enable 
	/// an easy way to control how the current date and time are determined in this library,
	/// for example for testing caching. 
	/// </summary>
	public static class SystemTime
	{
		public static Func<DateTime> UtcNow { get; set; } = () => DateTime.UtcNow;

		public static void Reset()
		{
			UtcNow = () => DateTime.UtcNow;
		}
	}
}
