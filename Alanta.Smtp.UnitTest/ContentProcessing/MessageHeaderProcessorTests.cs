﻿using System;
using System.Text;
using Alanta.Smtp.ContentProcessing;
using Should;
using Xunit;

namespace Alanta.Smtp.UnitTest.ContentProcessing
{
   public class MessageContentProcessorTests
   {
      [Fact]
      public void It_should_extract_the_value_of_the_Return_Path_header_if_its_not_the_first_header()
      {
         // setup
         var content = new StringBuilder( "Received: from some other server\r\nReturn-Path: <bogus@example.com>\r\nTo: <bogus@example.com>\r\n" );

         var processor = new MessageContentProcessor( content );

         // act
         var value = processor.PrepareForDelivery( string.Empty, false, "TraceInfo" );

         // verify
         value.ToString().ShouldStartWith( "Return-Path: <bogus@example.com>" );
      }

      [Fact]
      public void It_should_ignore_message_body()
      {
         // setup
         var content = new StringBuilder( "Some-header: <bogus@example.com>\r\nTo: <bogus@example.com>\r\n\r\nReturn-Path: <bogus@example.com>" );

         var processor = new MessageContentProcessor( content );

         // act
         var value = processor.PrepareForDelivery( "<bogus@example.com>", false, "TraceInfo" );

         // verify
         value.ToString().ShouldStartWith( "Return-Path: <bogus@example.com>\r\n" );
      }

      [Fact]
      public void It_should_replace_return_path_header()
      {
         // setup
         var content = new StringBuilder( 
@"Return-Path: <mailout@maillist.codeproject.com>
Received: from u.alanta.nl ([unix socket])
	 by u.alanta.nl (Cyrus v2.3.16-Fedora-RPM-2.3.16-6.el6_1.4) with LMTPA;
	 Wed, 18 Jul 2012 01:09:37 -0400
X-Sieve: CMU Sieve 2.3
Received: from u.alanta.nl (localhost.localdomain [127.0.0.1])
	by u.alanta.nl (Postfix) with ESMTP id B3FBE4861
	for <marnix@alanta.nl>; Wed, 18 Jul 2012 01:09:37 -0400 (EDT)
" );

         var processor = new MessageContentProcessor( content );

         // act
         var prepared = processor.PrepareForDelivery( "<marnix@alanta.nl>", true, null );

         // verify
         var lines = prepared.ToString().Split( new[] { "\r\n" }, StringSplitOptions.None );
         lines[0].ShouldEqual( "Return-Path: <marnix@alanta.nl>" );
         lines[1].ShouldEqual( "Received: from u.alanta.nl ([unix socket])" );
      }

      [Fact]
      public void It_should_insert_traceinfo_at_top()
      {
         // setup
         var content = new StringBuilder(
@"Return-Path: <marnix@alanta.nl>
Received: from u.alanta.nl ([unix socket])
 by u.alanta.nl (Cyrus v2.3.16-Fedora-RPM-2.3.16-6.el6_1.4) with LMTPA; Wed, 18 Jul 2012 01:09:37 -0400
X-Sieve: CMU Sieve 2.3
Received: from u.alanta.nl (localhost.localdomain [127.0.0.1]) by u.alanta.nl (Postfix) with ESMTP id B3FBE4861 for <marnix@alanta.nl>; Wed, 18 Jul 2012 01:09:37 -0400 (EDT)
" );

         var processor = new MessageContentProcessor( content );

         // act
         var prepared = processor.PrepareForDelivery( "<>", false, @"from localhost (localhost.localdomain [127.0.0.1])
 by test.alanta.nl (Alanta.Smtp) with ESMTP id B3FBE4861
 for <marnix@alanta.nl>; Wed, 18 Jul 2012 01:09:37 -0400 (EDT)" );

         // verify
         var lines = prepared.ToString().Split( new[] { "\r\n" }, StringSplitOptions.None );
         lines[1].ShouldEqual( "Received: from localhost (localhost.localdomain [127.0.0.1])" );
         lines[2].ShouldEqual( " by test.alanta.nl (Alanta.Smtp) with ESMTP id B3FBE4861" );
         lines[3].ShouldEqual( " for <marnix@alanta.nl>; Wed, 18 Jul 2012 01:09:37 -0400 (EDT)" );
         lines[4].ShouldEqual( "Received: from u.alanta.nl ([unix socket])" );
      }
   }
}
