﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alanta.Smtp.ContentProcessing;
using Alanta.Smtp.Dns;
using Moq;
using Xunit;

namespace Alanta.Smtp.UnitTest.ContentProcessing
{
   public class TraceInformationTests
   {
      [Fact]
      public void It_should_correclty_format_received_date()
      {
         // Arrange
         var sut = new TraceInformation(new Mock<IDnsResolver>().Object);

         var message = new SmtpMessage
         {
            Received = new DateTime(2015, 7, 30, 19, 14, 24, DateTimeKind.Utc)
         };
         
         // Act
         var result = sut.GetReceivedHeaderTemplate(new SmtpMessage(), "test@example.com");

         // Assert

      }
   }
}
