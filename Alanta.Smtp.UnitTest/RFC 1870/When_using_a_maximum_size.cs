﻿using System;
using System.Threading.Tasks;
using Alanta.Smtp.UnitTest.Tools;
using Should;
using Xunit;

namespace Alanta.Smtp.UnitTest.RFC_1870
{
   public class When_using_a_maximum_size : Alanta.Smtp.UnitTest.SessionTests.SessionTestBase
   {
      [Fact]
      public void It_should_return_a_the_maximum_size_on_EHLO()
      {
         // setup
         using( var connection = new AsyncFakeConnection())
         {
            var session = CreateSession( connection, context => context.MaximumMessageSize = 10240000 );

            // act
            var task = Task.Factory.StartNew( session.Process );
            var result = connection.SendAndReceive( "EHLO test.example.com", true );

            // verify
            result.ShouldMatch( @"\r\n250.SIZE 10240000\r\n" );

            connection.Close();
            task.WaitForCompletion();
         }
      }

      [Fact]
      public void It_should_return_no_maximum_size_on_EHLO()
      {
         // setup
         using ( var connection = new AsyncFakeConnection() )
         {
            var session = CreateSession( connection, context => context.MaximumMessageSize = null );

            // act
            var task = Task.Factory.StartNew( session.Process );
            var result = connection.SendAndReceive( "EHLO test.example.com", true );

            // verify
            result.ShouldNotContain( "SIZE", StringComparison.OrdinalIgnoreCase );

            connection.Close();
            task.WaitForCompletion();
         }
      }
   }
}
