﻿using System;
using System.Text;
using Alanta.Sasl;
using Moq;
using Should;
using Xunit;

namespace Alanta.Smtp.UnitTest.SASL
{
   public class When_using_plain_mechanism
   {
      [Fact]
      public void It_should_authenticate_and_authorize()
      {
         // Arrrange
         var authenticationMock = new Mock<IAuthenticationService>();
         authenticationMock.Setup( a => a.Authenticate( "test", "pass" ) ).Returns( true ).Verifiable();
         var authorizationMock = new Mock<IAuthorizationService>();
         authorizationMock.Setup( a => a.Authorize( "test", "test" ) ).Returns( true ).Verifiable();

         // Act
         var sut = new Alanta.Sasl.Server.PlainMechanism( authenticationMock.Object, authorizationMock.Object);
         SaslResult authorized = SaslResult.Unknown;
         sut.AuthorizationResult += (sender, args) => { authorized = args.Result; };

         bool complete;
         var challenge = sut.GetChallenge(Convert.ToBase64String(Encoding.UTF8.GetBytes("test\0pass")), out complete);

         // Assert
         complete.ShouldBeTrue();
         challenge.ShouldBeNull();
         authorized.ShouldEqual( SaslResult.Success );
         authenticationMock.VerifyAll();
         authorizationMock.VerifyAll();
      }
   }
}
