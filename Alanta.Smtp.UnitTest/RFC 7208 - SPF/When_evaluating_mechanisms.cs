﻿using Alanta.Smtp.Rules.SenderPolicyFramework;
using Should;
using Xunit;

namespace Alanta.Smtp.UnitTest.RFC_7208___SPF
{
	public class When_evaluating_mechanisms
	{
		[Fact]
		public void All_should_match_all_domains()
		{
			// Arrange
			var all = Mechanism.All(Qualifier.pass);

			// Act
			var result = all.Evaluate();

			// Assert
			result.ShouldEqual( Result.Pass );
		}
	}
}
