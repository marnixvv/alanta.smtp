﻿using System.Net;
using Alanta.Smtp.Dns;
using Alanta.Smtp.Rules.SenderPolicyFramework;
using Moq;
using Should;
using Xunit;

namespace Alanta.Smtp.UnitTest.RFC_7208___SPF
{
	public class When_verifiying_access_with_spf
	{
		[Fact]
		public void It_should_ignore_domains_without_SPF_records()
		{
			// Arrange
			var resolver = new Mock<IDnsResolver>();
			resolver.ResolveDomain("alanta.nl", "169.254.1.1");
			
			var meteredResolver = new MeteredDnsResolverDecorator( resolver.Object, 10 );
			var rule = new Check_Policy_Service( resolver.Object );
			
			// Act
			var result = rule.CheckHost(IPAddress.Loopback, "jimmy@alanta.nl", "alanta.nl", meteredResolver);

			// Assert
			result.ShouldEqual( Result.None );
			resolver.Verify( r => r.GetTxtRecords("alanta.nl"), Times.Once() );
		}

		[Fact]
		public void It_should_ignore_invalid_domains()
		{
			// Arrange
			var resolver = new Mock<IDnsResolver>();
			
			var meteredResolver = new MeteredDnsResolverDecorator( resolver.Object, 10 );
			var rule = new Check_Policy_Service( resolver.Object );

			// Act
			var result = rule.CheckHost( IPAddress.Loopback, "jimmy@nowhere", "nowhere", meteredResolver );

			// Assert
			result.ShouldEqual( Result.None );
			resolver.Verify( r => r.GetTxtRecords( It.IsAny<string>() ), Times.Never() );
		}

		[Fact]
		public void It_should_ignore_unresolvable_domains()
		{
			// Arrange
			var resolver = new Mock<IDnsResolver>();

			var meteredResolver = new MeteredDnsResolverDecorator( resolver.Object, 10 );
			var rule = new Check_Policy_Service( resolver.Object );

			// Act
			var result = rule.CheckHost( IPAddress.Loopback, "jimmy@nowhere.com", "nowhere.com", meteredResolver );

			// Assert
			result.ShouldEqual( Result.None );
			resolver.Verify( r => r.Resolve( "nowhere.com" ), Times.Once() );
			resolver.Verify( r => r.GetTxtRecords( It.IsAny<string>() ), Times.Never() );
		}

		[Fact]
		public void It_should_accept_based_on_all_rule()
		{
			// Arrange
			var resolver = new Mock<IDnsResolver>();
			resolver.ResolveDomain( "alanta.nl", "169.254.1.1" );
			resolver.Setup(r => r.GetTxtRecords("alanta.nl")).Returns(new[] {"v=spf1 +all"});

			var meteredResolver = new MeteredDnsResolverDecorator( resolver.Object, 10 );
			var rule = new Check_Policy_Service( resolver.Object );

			// Act
			var result = rule.CheckHost( IPAddress.Loopback, "jimmy@alanta.nl", "alanta.nl", meteredResolver );

			// Assert
			result.ShouldEqual( Result.Pass );
			resolver.Verify( r => r.GetTxtRecords( "alanta.nl" ), Times.Once() );
		}

		[Fact]
		public void It_should_areject_based_on_negative_all_rule()
		{
			// Arrange
			var resolver = new Mock<IDnsResolver>();
			resolver.ResolveDomain( "alanta.nl", "169.254.1.1" );
			resolver.Setup( r => r.GetTxtRecords( "alanta.nl" ) ).Returns( new[] { "v=spf1 -all" } );

			var meteredResolver = new MeteredDnsResolverDecorator( resolver.Object, 10 );
			var rule = new Check_Policy_Service( resolver.Object );

			// Act
			var result = rule.CheckHost( IPAddress.Loopback, "jimmy@alanta.nl", "alanta.nl", meteredResolver );

			// Assert
			result.ShouldEqual( Result.Fail );
			resolver.Verify( r => r.GetTxtRecords( "alanta.nl" ), Times.Once() );
		}
	}

	public static class DnsResolverSetup
	{

		public static Mock<IDnsResolver> ResolveDomain( this Mock<IDnsResolver> resolver, string domain, string ip )
		{
			resolver.Setup( r => r.Resolve( domain ) ).Returns( new[] { IPAddress.Parse( ip ) } );
			return resolver;
		}

	}
}
