﻿using System.Net;
using Alanta.Smtp.Dns;
using Alanta.Smtp.Rules.SenderPolicyFramework;
using Moq;
using Should;
using Xunit;

namespace Alanta.Smtp.UnitTest.RFC_7208___SPF
{
	// RFC 7208 section 5.5

	public class When_validating_an_ip_address
	{
		[Fact]
		public void It_should_verify_an_address_with_a_reverse_lookup_path()
		{
			// Arrange
			var ip = IPAddress.Parse( "6.6.6.6" );

			var resolver = new Mock<IDnsResolver>();
			resolver.Setup(r => r.ReverseLookup(ip)).Returns(new[] {"ns1.google.com"});
			resolver.Setup(r => r.Resolve("ns1.google.com")).Returns(new[] {ip});

			var helper = new DnsHelper( new MeteredDnsResolverDecorator( resolver.Object, 10 ) );

			// Act
			string validateName;
			var result = helper.Verify( ip, out validateName );

			// Assert
			result.ShouldEqual( VerificationResult.Succedeed );
		}

		[Fact]
		public void It_should_not_verify_an_address_when_the_number_of_lookups_is_exceeded()
		{
			// Arrange
			var ip = IPAddress.Parse( "6.6.6.6" );

			var resolver = new Mock<IDnsResolver>();
			resolver.Setup( r => r.ReverseLookup( ip ) ).Returns( new[] { "a.b.c", "c.d.e", "ns1.google.com" } );
			resolver.Setup( r => r.Resolve( "ns1.google.com" ) ).Returns( new[] { ip } );

			var helper = new DnsHelper( new MeteredDnsResolverDecorator( resolver.Object, 2 ) );

			// Act
			string validateName;
			var result = helper.Verify( ip, out validateName );

			// Assert
			result.ShouldEqual( VerificationResult.DnsLookupLimitExceeded );
		}

		[Fact]
		public void It_should_not_verify_an_address_without_a_reverse_path()
		{
			// Arrange
			var ip = IPAddress.Parse( "6.6.6.6" );

			var resolver = new Mock<IDnsResolver>();
			resolver.Setup( r => r.ReverseLookup( ip ) ).Returns( new[] { "ns1.google.com" } );
			resolver.Setup( r => r.Resolve( "ns1.google.com" ) ).Returns( new[] { IPAddress.Parse("8.8.8.8")  } );

			var helper = new DnsHelper( new MeteredDnsResolverDecorator( resolver.Object, 10 ) );

			// Act
			string validateName;
			var result = helper.Verify( ip, out validateName );

			// Assert
			result.ShouldEqual( VerificationResult.Failed );
		}
	}
}
