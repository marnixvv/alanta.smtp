﻿using System.Linq;
using System.Net;
using Alanta.Smtp.Dns;
using Alanta.Smtp.Rules.SenderPolicyFramework;
using Moq;
using Should;
using Xunit;

namespace Alanta.Smtp.UnitTest.RFC_7208___SPF
{
	public class When_expanding_macros
	{
		private readonly string _domain = "email.example.com";
		private readonly string _sender = "strong-bad@email.example.com";
		private readonly IPAddress _ip = IPAddress.Parse("192.0.2.3");
		//private readonly IPAddress _ipv6 = IPAddress.Parse( "2001:db8::cb01" );

		[Theory,
		 InlineData("%{s}", "strong-bad@email.example.com"),
		 InlineData("%{o}", "email.example.com"),
		 InlineData("%{d}", "email.example.com"),
		 InlineData("%{d4}", "email.example.com"),
		 InlineData("%{d3}", "email.example.com"),
		 InlineData("%{d2}", "example.com"),
		 InlineData("%{d1}", "com"),
		 InlineData("%{dr}", "com.example.email"),
		 InlineData("%{d2r}", "example.email"),
		 InlineData("%{l}", "strong-bad"),
		 InlineData("%{l-}", "strong.bad"),
		 InlineData("%{lr}", "strong-bad"),
		 InlineData("%{lr-}", "bad.strong"),
		 InlineData("%{l1r-}", "strong"),
		 InlineData(@"%{ir}.%{v}._spf.%{d2}", "3.2.0.192.in-addr._spf.example.com"),
		 InlineData(@"%{lr-}.lp._spf.%{d2}", "bad.strong.lp._spf.example.com"),
		 InlineData(@"%{lr-}.lp.%{ir}.%{v}._spf.%{d2}", "bad.strong.lp.3.2.0.192.in-addr._spf.example.com"),
		 InlineData(@"%{ir}.%{v}.%{l1r-}.lp._spf.%{d2}", "3.2.0.192.in-addr.strong.lp._spf.example.com"),
		 InlineData("%{d2}.trusted-domains.example.net", "example.com.trusted-domains.example.net")]
		public void It_should_correctly_subsititute(string input, string output)
		{
			// Arrange
			var expander = new MacroExpander(new Mock<IDnsHelper>().Object);

			// Act
			var result = expander.Expand(input, _sender, _ip, _domain);

			// Assert
			result.ShouldEqual(output);
		}

		[Theory,
		 InlineData("%{ir}.%{v}._spf.%{d2}", "1.0.b.c.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.8.b.d.0.1.0.0.2.ip6._spf.example.com")]
		public void It_should_correctly_subsititute_with_ipv6(string input, string output)
		{
			// Arrange
			var expander = new MacroExpander(new Mock<IDnsHelper>().Object);

			// Act
			var result = expander.Expand(input, _sender, IPAddress.Parse("2001:db8::cb01"), _domain);

			// Assert
			result.ShouldEqual(output);
		}

		

		
	}
}
