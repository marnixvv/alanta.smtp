﻿using System.Linq;
using Alanta.Smtp.Rules.SenderPolicyFramework;
using Should;
using Xunit;

namespace Alanta.Smtp.UnitTest.RFC_7208___SPF
{
	public class When_parsing_SPF_records
	{
		[Theory,
			InlineData( null ),
			InlineData( "this is a bogus record" ),
			InlineData( "v=spf10 This is an undefined version" )]
		public void It_should_ignore_non_spf_records( string record )
		{
			// Arrange

			// Act
			var result = SpfRecordParser.Parse( record );

			// Assert
			result.ShouldBeNull();
		}

		[Fact]
		public void It_should_parse_minimal_valid_spf_record()
		{
			// Arrange

			// Act
			var result = SpfRecordParser.Parse( "v=spf1" );

			// Assert
			result.ShouldNotBeNull();
		}

		[Fact]
		public void It_should_parse_include_mechanism()
		{
			// Arrange

			// Act
			var result = SpfRecordParser.Parse( "v=spf1 include:alanta.nl" );

			// Assert
			result.ShouldNotBeNull();
			result.Count().ShouldEqual( 1 );
			result.First().ShouldBeType<Mechanism>();
		}

		[Fact]
		public void It_should_parse_all_mechanism()
		{
			// Arrange

			// Act
			var result = SpfRecordParser.Parse( "v=spf1 -all" );

			// Assert
			result.ShouldNotBeNull();
			result.Count().ShouldEqual( 1 );
			var mechanism = result.First().ShouldBeType<Mechanism>();
			mechanism.Qualifier.ShouldEqual( Qualifier.fail );
			mechanism.Name.ShouldEqual( "all" );
		}

		[Fact]
		public void It_should_parse_a_mechanism()
		{
			// Arrange

			// Act
			var result = SpfRecordParser.Parse( "v=spf1 a:example.com -all" );

			// Assert
			result.ShouldNotBeNull();
			result.Count().ShouldEqual( 2 );
			var mechanism = result.First().ShouldBeType<Mechanism>();
			mechanism.Qualifier.ShouldEqual( Qualifier.neutral );
			mechanism.Name.ShouldEqual( "a" );
		}

		[Fact]
		public void It_should_parse_mx_mechanism()
		{
			// Arrange

			// Act
			var result = SpfRecordParser.Parse( "v=spf1 mx:example.com -all" );

			// Assert
			result.ShouldNotBeNull();
			result.Count().ShouldEqual( 2 );
			var mechanism = result.First().ShouldBeType<Mechanism>();
			mechanism.Qualifier.ShouldEqual( Qualifier.neutral );
			mechanism.Name.ShouldEqual( "mx" );
		}

		[Fact]
		public void It_should_parse_ptr_mechanism()
		{
			// Arrange

			// Act
			var result = SpfRecordParser.Parse( "v=spf1 ptr:example.com -all" );

			// Assert
			result.ShouldNotBeNull();
			result.Count().ShouldEqual( 2 );
			var mechanism = result.First().ShouldBeType<Mechanism>();
			mechanism.Qualifier.ShouldEqual( Qualifier.neutral );
			mechanism.Name.ShouldEqual( "ptr" );
		}

		[Fact]
		public void It_should_parse_ip4_mechanism()
		{
			// Arrange

			// Act
			var result = SpfRecordParser.Parse( "v=spf1 -ip4:192.168.0.0/16 -all" );

			// Assert
			result.ShouldNotBeNull();
			result.Count().ShouldEqual( 2 );
			var mechanism = result.First().ShouldBeType<Mechanism>();
			mechanism.Qualifier.ShouldEqual( Qualifier.fail );
			mechanism.Name.ShouldEqual( "ip4" );
		}

		[Fact]
		public void It_should_parse_ip6_mechanism()
		{
			// Arrange

			// Act
			var result = SpfRecordParser.Parse( "v=spf1 -ip6:3ffe:1900:4545:3:200:f8ff:fe21:67cf/16 -all" );

			// Assert
			result.ShouldNotBeNull();
			result.Count().ShouldEqual( 2 );
			var mechanism = result.First().ShouldBeType<Mechanism>();
			mechanism.Qualifier.ShouldEqual( Qualifier.fail );
			mechanism.Name.ShouldEqual( "ip6" );
		}

		[Fact]
		public void It_should_parse_exp_modifier()
		{
			// Arrange

			// Act
			var result = SpfRecordParser.Parse( "v=spf1 -all exp=test" );

			// Assert
			result.ShouldNotBeNull();
			result.Count().ShouldEqual( 2 );
			var modifier = result.Last().ShouldBeType<Modifier>();
			modifier.Name.ShouldEqual( "exp" );
		}

		[Fact]
		public void It_should_parse_redirect_modifier()
		{
			// Arrange

			// Act
			var result = SpfRecordParser.Parse( "v=spf1 -all redirect=test" );

			// Assert
			result.ShouldNotBeNull();
			result.Count().ShouldEqual( 2 );
			var modifier = result.Last().ShouldBeType<Modifier>();
			modifier.Name.ShouldEqual( "redirect" );
		}
	}
}
