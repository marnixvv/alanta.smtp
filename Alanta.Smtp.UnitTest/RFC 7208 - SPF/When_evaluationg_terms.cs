using System.Net;
using Alanta.Smtp.Dns;
using Alanta.Smtp.Rules.SenderPolicyFramework;
using Moq;
using Should;
using Xunit;

namespace Alanta.Smtp.UnitTest.RFC_7208___SPF
{
	public class When_evaluationg_terms
	{
		[Theory,
		 InlineData("v=spf1 +all redirect=test1 redirect=test2"),
		 InlineData( "v=spf1 +all exp=test1 exp=test2" )]
		public void Duplicate_modifiers_should_cause_permanent_error(string spfRecord)
		{
			// Arrange
			var record = SpfRecordParser.Parse(spfRecord);

			// Act
			var result = record.Evaluate( new IPAddress( new byte[] { 127,0,0,1 } ), "test@example.com", "example.com", new MeteredDnsResolverDecorator( new Mock<IDnsResolver>().Object, 100 ) );

			// Assert
			result.ShouldEqual( Result.Permerror );
		}

	}
}