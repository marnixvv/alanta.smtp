﻿using System.Threading.Tasks;
using Alanta.Smtp.UnitTest.SessionTests;
using Alanta.Smtp.UnitTest.Tools;
using Should;
using Xunit;

namespace Alanta.Smtp.UnitTest.RFC_1652___8_Bit_Mime
{
   public class When_receiving_8bit_mime : SessionTestBase
   {
      [Fact]
      public void It_should_return_8BITMIME_after_EHLO()
      {
         // setup
         using( var connection = new AsyncFakeConnection())
         {
            var session = CreateSession( connection );

            // act
            var task = Task.Factory.StartNew( session.Process );
            var result = connection.SendAndReceive( "EHLO test.example.com", true );

            // verify
            result.ShouldMatch( "\r\n250.8BITMIME\r\n" );

            connection.Close();
            task.WaitForCompletion();
         }
      }

      [Fact]
      public void It_should_accept_body_parameter_on_MAILFROM()
      {
         // setup
         using( var connection = new AsyncFakeConnection( new[]{"EHLO test.example.com"}))
         {
            var session = CreateSession( connection );

            // act
            var task = Task.Factory.StartNew( session.Process );
            var result = connection.SendAndReceive( "MAIL FROM:<test@example.com> BODY=8BITMIME", true );

            // verify
            result.ShouldMatch( "250.*Sender and 8BITMIME OK" );

            connection.Close();
            task.WaitForCompletion();
         }
      }

      [Fact]
      public void It_should_accept_7BIT_body_argument_on_MAILFROM()
      {
         // setup
         using ( var connection = new AsyncFakeConnection( new[] { "EHLO test.example.com" } ) )
         {
            var session = CreateSession( connection );

            // act
            var task = Task.Factory.StartNew( session.Process );
            var result = connection.SendAndReceive( "MAIL FROM:<test@example.com> BODY=7BIT", true );

            // verify
            result.ShouldMatch( "250.*Sender OK" );

            connection.Close();
            task.WaitForCompletion();
         }
      }

      [Fact]
      public void It_should_accept_no_body_argument_on_MAILFROM()
      {
         // setup
         using ( var connection = new AsyncFakeConnection( new[] { "EHLO test.example.com" } ) )
         {
            var session = CreateSession( connection );

            // act
            var task = Task.Factory.StartNew( session.Process );
            var result = connection.SendAndReceive( "MAIL FROM:<test@example.com>", true );

            // verify
            result.ShouldMatch( "250.*Sender OK" );

            connection.Close();
            task.WaitForCompletion();
         }
      }

      [Fact]
      public void It_should_not_accept_an_illegal_body_argument_on_MAILFROM()
      {
         // setup
         using ( var connection = new AsyncFakeConnection( new[] { "EHLO test.example.com" } ) )
         {
            var session = CreateSession( connection );

            // act
            var task = Task.Factory.StartNew( session.Process );
            var result = connection.SendAndReceive( "MAIL FROM:<test@example.com> BODY=bogus", true );

            // verify
            result.ShouldStartWith("501" );

            connection.Close();
            task.WaitForCompletion();
         }
      }

      [Fact]
      public void It_should_mention_8BITMIME_in_response_to_DATA()
      {
         //354 Send 8BITMIME message, ending in CRLF.CRLF.
         // setup
         using ( var connection = new AsyncFakeConnection( new[] { "EHLO test.example.com", "MAIL FROM:<text@example.com> BODY=8BITMIME", "RCPT TO:<you@example.com>" } ) )
         {
            var session = CreateSession( connection );

            // act
            var task = Task.Factory.StartNew( session.Process );
            var result = connection.SendAndReceive( "DATA", true );

            // verify
            result.ShouldMatch( "^354 .*8BITMIME.*$" );

            connection.Close();
            task.WaitForCompletion();
         }
      }
   }
}
