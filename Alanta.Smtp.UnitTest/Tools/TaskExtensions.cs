﻿using System;
using System.Threading.Tasks;

namespace Alanta.Smtp.UnitTest.Tools
{
   public static class TaskExtensions
   {
      public static void WaitForCompletion( this Task task )
      {
         try
         {
            task.Wait();
         }
         catch ( Exception ex )
         {
            var skip = false;
            var inner = ex.InnerException;
            while ( null != inner )
            {
               if ( inner is OperationCanceledException )
               {
                  skip = true;
                  break;
               }
               inner = inner.InnerException;
            }
            if ( !skip )
               throw;
         }
      }
   }
}
