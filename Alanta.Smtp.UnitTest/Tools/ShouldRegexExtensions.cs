﻿using System;
using System.Text.RegularExpressions;
using Should.Core.Assertions;

namespace Alanta.Smtp.UnitTest.Tools
{
   public static class ShouldRegexExtensions
   {
      public static string ShouldMatch( this string value, string expression )
      {
         if ( expression == null )
            throw new ArgumentNullException( nameof(expression) );

         Assert.True( value != null, string.Format( "NULL does not match regular expression '{0}'", expression ) );

         var regex = new Regex( expression, RegexOptions.IgnoreCase );
         Assert.True( regex.IsMatch( value ),  string.Format("'{0}' does not match regular expression '{1}'", value, expression ) );

         return value;
      }

      public static string ShouldNotMatch( this string value, string expression )
      {
         if ( expression == null )
            throw new ArgumentNullException( nameof(expression) );

         Assert.True( value != null, string.Format( "NULL does not match regular expression '{0}'", expression ) );

         var regex = new Regex( expression, RegexOptions.IgnoreCase );
         Assert.False( regex.IsMatch( value ), string.Format( "'{0}' does not match regular expression '{1}'", value, expression ) );

         return value;
      }
   }
}
