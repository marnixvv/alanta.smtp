using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;

namespace Alanta.Smtp.UnitTest.Tools
{
   internal class AsyncFakeConnection : IServerConnection
   {
      private readonly Action<string> _log;

      public AsyncFakeConnection() : this( null )
      {
      }

      public AsyncFakeConnection( IEnumerable<string> setup, Action<string> log = null )
      {
         _log = log;
         queue = new BlockingCollection<string>( new ConcurrentQueue<string>( setup ?? new string[0] ) );

         Connected = true;
      }

      private void Log(string message)
      {
         if ( null != _log )
            _log( message );
      }

      private readonly BlockingCollection<string> queue;
      private readonly BlockingCollection<string> received = new BlockingCollection<string>();
      private CancellationTokenSource ctSource = new CancellationTokenSource();

      private bool _dataReceived;

      string INetworkConnection.ReadLine()
      {
         var data = queue.Take( ctSource.Token );
         Log( "Client : " + data );
         return data;
      }

      void INetworkConnection.Send( string data )
      {
         if( Connected )
         {
            lock( _lockObject )
            {
               if( Connected )
               {
                  received.Add( data );
                  Log( "Server : "+data );
                  _dataReceived = true;
               }
            }
         }
      }

      public bool Connected { get; private set; }

      public void Disconnect()
      {
         lock ( _lockObject )
         {
            if ( !ctSource.IsCancellationRequested )
               ctSource.Cancel();
            Connected = false;
            Log( "Disconnect" );
         }
      }

      public Task<bool> StartTls( X509Certificate serverCertificate, SslProtocols allowedTlsProtocols )
      {
         return Task.FromResult(true);
      }

      public bool StartClientTls(string host)
      {
         throw new NotImplementedException();
      }

      public string Receive()
      {
         return received.Take();
      }

      public string SendAndReceive( string data, bool clear = false )
      {
         // Wait for outgoing buffer to be processed
         while( queue.Count != 0 || !_dataReceived)
         {
            Thread.Sleep( 10 );
         }

         if ( clear )
         {
            // clear out the received queue
            string removed;
            while ( received.TryTake( out removed, 250, ctSource.Token ) ) {}
         }

         lock (_lockObject)
         {
            _dataReceived = false;
            queue.Add(data, ctSource.Token);
         }
         return received.Take();
      }

      public void Close()
      {
         lock ( _lockObject )
         {
            Disconnect();
            if ( !queue.IsAddingCompleted )
               queue.CompleteAdding();
            if ( !received.IsAddingCompleted )
               received.CompleteAdding();
         }
      }

      public void Dispose()
      {
         Close();
      }

      private object _lockObject = new object();
   }
}