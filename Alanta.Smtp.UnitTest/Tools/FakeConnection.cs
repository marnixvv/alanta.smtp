using System.Collections.Generic;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Alanta.Smtp.UnitTest.Tools
{
   internal class FakeConnection : IServerConnection
   {
      public FakeConnection( IEnumerable<string> send )
      {
         sendEnumerator = send.GetEnumerator();
         Connected = true;
      }

      private readonly List<string> received = new List<string>();
      private readonly IEnumerator<string> sendEnumerator;

      string INetworkConnection.ReadLine()
      {
         if ( !sendEnumerator.MoveNext() )
         {
            Connected = false;
         }
         return sendEnumerator.Current;
      }

      void INetworkConnection.Send( string data )
      {
         received.Add( data );
      }

      public bool Connected { get; set; }

      public void Disconnect()
      {
         Connected = false;
      }

      public Task<bool> StartTls( X509Certificate serverCertificate, SslProtocols allowedTlsProtocols )
      {
         throw new System.NotImplementedException();
      }

      public bool StartClientTls(string host)
      {
         throw new System.NotImplementedException();
      }

      public void Dispose()
      {
         sendEnumerator.Dispose();
      }

      public IList<string> Received { get { return received; } }
   }
}