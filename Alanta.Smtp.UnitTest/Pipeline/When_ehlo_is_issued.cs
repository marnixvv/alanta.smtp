﻿using Alanta.Smtp.Commands;
using Moq;
using Xunit;

namespace Alanta.Smtp.UnitTest.Pipeline
{
   public class When_ehlo_is_issued
   {
      [Fact]
      public void It_should_set_the_protocol_to_ESMTP()
      {
         // setup
         var command = new EhloCommand();
         var context = new Mock<ISmtpSessionContext>( MockBehavior.Loose );

         // act
         command.ProcessCommand( context.Object, "EHLO test.example.com" );

         // verify
         context.VerifySet( c => c.ProtocolName = "ESMTP" );
      }

      [Fact]
      public void It_should_set_the_providedRemoteName()
      {
         // setup
         var command = new EhloCommand();
         var context = new Mock<ISmtpSessionContext>( MockBehavior.Loose );

         // act
         command.ProcessCommand( context.Object, "EHLO test.example.com" );

         // verify
         context.VerifySet( c => c.ProvidedRemoteName = "test.example.com" );
      }
   }
}
