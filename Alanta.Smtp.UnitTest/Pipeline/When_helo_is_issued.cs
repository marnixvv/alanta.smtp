﻿using Alanta.Smtp.Commands;
using Moq;
using Xunit;

namespace Alanta.Smtp.UnitTest.Pipeline
{
   public class When_helo_is_issued
   {
      [Fact]
      public void It_should_set_the_protocol_to_SMTP()
      {
         // setup
         var command = new HeloCommand();
         var context = new Mock<ISmtpSessionContext>( MockBehavior.Loose );

         // act
         command.ProcessCommand( context.Object, "HELO test.example.com" );

         // verify
         context.VerifySet( c => c.ProtocolName = "SMTP" );
      }

      [Fact]
      public void It_should_set_the_providedRemoteName()
      {
         // setup
         var command = new HeloCommand();
         var context = new Mock<ISmtpSessionContext>( MockBehavior.Loose );

         // act
         command.ProcessCommand( context.Object, "HELO test.example.com" );

         // verify
         context.VerifySet( c => c.ProvidedRemoteName = "test.example.com" );
      }
   }
}
