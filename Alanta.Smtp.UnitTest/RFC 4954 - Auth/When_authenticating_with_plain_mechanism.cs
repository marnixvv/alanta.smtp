﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Alanta.Sasl;
using Alanta.Smtp.Commands;
using Alanta.Smtp.Logging;
using Alanta.Smtp.UnitTest.SessionTests;
using Alanta.Smtp.UnitTest.Tools;
using Moq;
using Should;
using Xunit;

namespace Alanta.Smtp.UnitTest.RFC4954
{
   public class When_authenticating_with_plain_mechanism : SessionTestBase
   {
      private Mock<IAuthenticationService> _authenticationMock;
      private Mock<IAuthorizationService> _authorizationMock;

      /*
         C: EHLO client.example.com
         S: 250-smtp.example.com Hello client.example.com
         S: 250 AUTH GSSAPI DIGEST-MD5 PLAIN
         C: AUTH PLAIN
          (note: there is a single space following the 334
           on the following line)
         S: 334
         C: dGVzdAB0ZXN0ADEyMzQ=
         S: 235 2.7.0 Authentication successful
       * */
      [Fact]
      public void It_should_not_offer_plain_auth_when_connection()
      {
         // setup
         using ( var connection = new AsyncFakeConnection() )
         {
            var session = CreateSessionWithPlainAuth( connection );

            // act
            var task = Task.Factory.StartNew( session.Process );
            var result = connection.SendAndReceive( "EHLO test.example.com", true );

            // verify
            result.ShouldNotMatch( "\r\n250.AUTH.+PLAIN.*\r\n" );

            connection.Close();
            task.WaitForCompletion();
         }
      }

      [Fact]
      public void It_should_offer_plain_auth_when_connection_is_encrypted()
      {
         // setup
         using ( var connection = new AsyncFakeConnection() )
         {
            var session = CreateSessionWithPlainAuth( connection );
            session.SmtpSessionContext.StartTls();

            // act
            var task = Task.Factory.StartNew( session.Process );
            var result = connection.SendAndReceive( "EHLO test.example.com", true );

            // verify
            result.ShouldMatch( "\r\n250.AUTH.+PLAIN.*\r\n" );

            connection.Close();
            task.WaitForCompletion();
         }
      }

      [Fact]
      public void It_should_accept_plain_auth_mechanism()
      {
         // setup
         using ( var connection = new AsyncFakeConnection( new[] { "EHLO test.alanta.nl" } ) )
         {
            
            var session = CreateSessionWithPlainAuth( connection );
            session.SmtpSessionContext.StartTls();
            _authenticationMock.Setup(a => a.Authenticate("test", "pass")).Returns(true);
            _authorizationMock.Setup( a => a.Authorize( "test", "test" ) ).Returns( true );
              
            // act
            var task = Task.Factory.StartNew( session.Process );
            var result = connection.SendAndReceive("AUTH PLAIN", true);
            
            // verify
            result.ShouldEqual( "334 " );

            connection.Close();
            task.WaitForCompletion();
         }
      }

      [Fact]
      public void It_should_not_accept_plain_auth_mechanism_over_unencrypted_channel()
      {
         // setup
         using ( var connection = new AsyncFakeConnection( new[] { "EHLO test.alanta.nl" } ) )
         {
            var session = CreateSessionWithPlainAuth( connection );

            // act
            var task = Task.Factory.StartNew( session.Process );
            var result = connection.SendAndReceive( "AUTH PLAIN", true );

            // verify
            result.ShouldStartWith( "538 " );

            connection.Close();
            task.WaitForCompletion();
         }
      }

      [Fact]
      public void It_should_athenticate_with_plain_auth_mechanism()
      {
         // setup
         using ( var connection = new AsyncFakeConnection( new[] { "EHLO test.alanta.nl", "AUTH PLAIN" } ) )
         {
            var session = CreateSessionWithPlainAuth( connection );
            session.SmtpSessionContext.StartTls();
            _authenticationMock.Setup( a => a.Authenticate( "test", "pass" ) ).Returns( true );
            _authorizationMock.Setup( a => a.Authorize( "test", "test" ) ).Returns( true );

            // act
            var task = Task.Factory.StartNew( session.Process );
            var result = connection.SendAndReceive(Convert.ToBase64String(Encoding.UTF8.GetBytes("test\0pass")), true);
            
            // verify
            result.ShouldMatch( "235.*Authentication success.*" );

            connection.Close();
            task.WaitForCompletion();
         }
      }

      private SmtpSession CreateSessionWithPlainAuth(IServerConnection connection)
      {
         _authenticationMock = new Mock<IAuthenticationService>();
         _authorizationMock = new Mock<IAuthorizationService>();

         return CreateSession(connection,
            ctx =>
            {
               ctx.Pipeline.RegisterCommand(
                  new AuthCommand(
                  
                     new DefaultSaslFactory(new[]{"PLAIN"},_authenticationMock.Object, _authorizationMock.Object)
                  ));
               ((SmtpSessionContext)ctx).ServerCertificate = new X509Certificate();
            });

      }

   }
}
