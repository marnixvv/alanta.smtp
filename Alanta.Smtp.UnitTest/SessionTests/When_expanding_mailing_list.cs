using Alanta.Smtp.UnitTest.Tools;
using Should;
using Xunit;

namespace Alanta.Smtp.UnitTest.SessionTests
{
   public class When_expanding_mailing_list : SessionTestBase
   {
      [Fact]
      public void It_should_always_return_252()
      {
         // Setup
         var connection = new FakeConnection( new[] { "EHLO test.alanta.nl", "EXPN listname", "QUIT" } );
         var session = CreateSession( connection );

         // Act
         session.Process();

         // Verify
         connection.Received[2].ShouldStartWith( "252" );
      }

      [Fact]
      public void It_should_allow_EXPN_before_EHLO()
      {
         // Setup
         var connection = new FakeConnection( new[] { "EXPN listname", "QUIT" } );
         var session = CreateSession( connection );

         // Act
         session.Process();

         // Verify
         connection.Received[1].ShouldStartWith( "252" );
      }
   }
}


