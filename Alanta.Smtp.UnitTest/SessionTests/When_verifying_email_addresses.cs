using Alanta.Smtp.UnitTest.Tools;
using Should;
using Xunit;

namespace Alanta.Smtp.UnitTest.SessionTests
{
   public class When_verifying_email_addresses : SessionTestBase
   {
      [Fact]
      public void It_should_always_return_252()
      {
         // Setup
         var connection = new FakeConnection( new[] { "HELO test.alanta.nl", "VRFY test@alanta.nl", "QUIT" } );
         var session = CreateSession( connection );

         // Act
         session.Process();

         // Verify
         connection.Received[2].ShouldStartWith( "252" );
      }

      [Fact]
      public void It_should_allow_VRFY_before_helo_return_252()
      {
         // Setup
         var connection = new FakeConnection( new[] { "VRFY test@alanta.nl", "QUIT" } );
         var session = CreateSession( connection );

         // Act
         session.Process();

         // Verify
         connection.Received[1].ShouldStartWith( "252" );
      }
   }
}
