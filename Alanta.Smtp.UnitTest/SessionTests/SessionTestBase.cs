﻿using System;
using System.Net;
using Alanta.Smtp.ContentProcessing;
using Alanta.Smtp.Logging;
using Moq;

namespace Alanta.Smtp.UnitTest.SessionTests
{
   public abstract class SessionTestBase
   {
      protected Mock<IMessageProcessor> _mailStoreMock;

      protected SmtpSession CreateSession( IServerConnection connection, Action<ISmtpSessionContext> configure = null )
      {
         _mailStoreMock = new Mock<IMessageProcessor>( MockBehavior.Loose );
         var pipeline = SmtpPipelineBuilder.CreatePipeline().EnableTls();
         var context = new SmtpSessionContext( connection, pipeline ) { HostName = "test.example.com", RemoteIP = new IPAddress( new byte[] { 127, 0, 0, 1 } ) };
         if( configure != null )
         {
            configure( context );
         }
         return new SmtpSession(context, _mailStoreMock.Object, new ConsoleLogger() );
      }
   }
}
