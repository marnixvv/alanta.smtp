using System.Linq;
using System.Threading.Tasks;
using Alanta.Smtp.UnitTest.Tools;
using Should;
using Xunit;

namespace Alanta.Smtp.UnitTest.SessionTests
{
   public class BasicSmtpChat : SessionTestBase
   {
      [Fact]
      public void It_should_send_220_on_connect()
      {
         // Arrange
         var connection = new FakeConnection( new[] {"HELO test.alanta.nl", "QUIT"} );
         var session = CreateSession( connection );

         // Act
         session.Process();

         // Assert
         connection.Received[0].ShouldStartWith( "220" );
      }

      [Fact]
      public void It_should_accept_lower_case_commands()
      {
         // Arrange
         var connection = new FakeConnection( new[] { "helo test.alanta.nl", "quit" } );
         var session = CreateSession( connection );

         // Act
         session.Process();

         // Assert
         connection.Received[0].ShouldStartWith( "220" );
      }

      [Fact]
      public void It_should_send_221_on_quit()
      {
         // Arrange
         var connection = new FakeConnection( new[] { "HELO test.alanta.nl", "QUIT" } );
         var session = CreateSession( connection );

         // Act
         session.Process();

         // Assert
         connection.Received.Last().ShouldStartWith( "221" );
      }

      [Fact]
      public void It_should_respond_with_500_on_bogus_command()
      {
         // Arrange
         using ( var connection = new AsyncFakeConnection() )
         {
            var session = CreateSession( connection );

            // Act
            var task = Task.Factory.StartNew( session.Process );
            var result = connection.SendAndReceive( "BOGUS", true );

            // Assert
            result.ShouldStartWith( "500" );

            connection.Close();
            task.WaitForCompletion();
         }
      }
   }
}
