using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Alanta.Smtp.ContentProcessing;
using Alanta.Smtp.UnitTest.Tools;
using Moq;
using Should;
using Xunit;

namespace Alanta.Smtp.UnitTest.SessionTests
{
   public class When_receiving_email : SessionTestBase
   {
      [Fact]
      public void It_should_accept_the_message()
      {
         // Setup
         var connection = new FakeConnection( new[] { "EHLO test.alanta.nl", "MAIL FROM:<marnix@alanta.nl>", "RCPT TO: test@example.com", "DATA", "Subject: Bogus", "Whatever",".", "QUIT" } );
         var session = CreateSession( connection );

         // Act
         session.Process();

         // Verify
         connection.Received[0].ShouldStartWith( "220" );
         connection.Received[1].ShouldStartWith( "250" );
         connection.Received[2].ShouldStartWith( "250" );
         connection.Received[3].ShouldStartWith( "250" );
         connection.Received[4].ShouldStartWith( "354" );
         connection.Received[5].ShouldStartWith( "250" );
         connection.Received[6].ShouldStartWith( "221" );
      }

      [Fact]
      public void It_should_return_503_for_MAILFROM_after_RCPTTO()
      {
         // Setup
         var connection = new FakeConnection( new[] { "EHLO test.alanta.nl", "MAIL FROM:<marnix@alanta.nl>", "RCPT TO: test@example.com", "MAIL FROM: bogus@yourdomain.com", "DATA", "Subject: Bogus", "Whatever", ".", "QUIT" } );
         var session = CreateSession( connection );

         // Act
         session.Process();

         // Verify
         connection.Received[0].ShouldStartWith( "220" );
         connection.Received[1].ShouldStartWith( "250" );
         connection.Received[2].ShouldStartWith( "250" );
         connection.Received[3].ShouldStartWith( "250" );
         connection.Received[4].ShouldStartWith( "503" );
         connection.Received[5].ShouldStartWith( "354" );
         connection.Received[6].ShouldStartWith( "250" );
         connection.Received[7].ShouldStartWith( "221" );
      }

      [Fact]
      public void It_should_return_503_for_RCPTTO_before_MAILFROM()
      {
         // Setup
         var connection = new FakeConnection( new[] { "EHLO test.alanta.nl", "RCPT TO:<test@example.com>", "QUIT" } );
         var session = CreateSession( connection );

         // Act
         session.Process();

         // Verify
         connection.Received[0].ShouldStartWith( "220" );
         connection.Received[1].ShouldStartWith( "250" );
         connection.Received[2].ShouldStartWith( "503" );
         connection.Received[3].ShouldStartWith( "221" );
      }

      [Fact]
      public void It_should_return_503_for_DATA_before_RCPTTO()
      {
         // Setup
         var connection = new FakeConnection( new[] { "EHLO test.alanta.nl", "MAIL FROM: <marnix@alanta.nl>", "DATA", "QUIT" } );
         var session = CreateSession( connection );

         // Act
         session.Process();

         // Verify
         connection.Received[0].ShouldStartWith( "220" );
         connection.Received[1].ShouldStartWith( "250" );
         connection.Received[2].ShouldStartWith( "250" );
         connection.Received[3].ShouldStartWith( "503" );
         connection.Received[4].ShouldStartWith( "221" );
      }

      [Fact]
      public void It_should_return_250_for_NOOP()
      {
         // Setup
         var connection = new FakeConnection( new[] { "NOOP", "QUIT" } );
         var session = CreateSession( connection );

         // Act
         session.Process();

         // Verify
         connection.Received[1].ShouldStartWith( "250" );
      }

      [Fact]
      public void It_should_return_feature_list_for_EHLO()
      {
         // Setup
         var connection = new FakeConnection( new[] { "EHLO test.example.nl", "QUIT" } );
         var session = CreateSession( connection );

         // Act
         session.Process();

         // Verify
         connection.Received[1].ShouldStartWith( "250" );
      }

      [Fact]
      public void It_should_reset_to_ready_state_when_RSET_is_issued_after_RCPT_TO()
      {
         // Setup
         var connection = new AsyncFakeConnection( new[] { "EHLO test.example.nl", "MAIL FROM:<test@example.com>", "RCPT TO: test@example.com" } );
         var session = CreateSession( connection );

         // Act
         var task = Task.Factory.StartNew( session.Process );
         var result = connection.SendAndReceive( "RSET", true );
         var result2 = connection.SendAndReceive( "MAIL FROM:<test@example.com>" );

         // Verify
         result.ShouldStartWith( "250" );
         result2.ShouldStartWith( "250" );
         connection.Close();
         task.WaitForCompletion();
      }

      [Fact]
      public void It_should_reset_to_ready_state_when_RSET_is_issued_after_MAIL_FROM()
      {
         // Setup
         var connection = new AsyncFakeConnection( new[] { "EHLO test.example.nl", "MAIL FROM:<test@example.com>" } );
         var session = CreateSession( connection );

         // Act
         var task = Task.Factory.StartNew( session.Process );
         var result = connection.SendAndReceive( "RSET", true );
         var result2 = connection.SendAndReceive( "MAIL FROM: <test@example.com>" );

         // Verify
         result.ShouldStartWith( "250" );
         result2.ShouldStartWith( "250" );
         connection.Close();
         task.WaitForCompletion();
      }

      [Fact]
      public void It_should_return_250_on_RSET_after_EHLO()
      {
         // Setup
         var connection = new AsyncFakeConnection( new[] { "EHLO test.example.nl" } );
         var session = CreateSession( connection );

         // Act
         var task = Task.Factory.StartNew( session.Process );
         var result = connection.SendAndReceive( "RSET", true );
         var result2 = connection.SendAndReceive( "MAIL FROM:<test@example.com>" );

         // Verify
         result.ShouldStartWith( "250" );
         result2.ShouldStartWith( "250" );
         connection.Close();
         task.WaitForCompletion();
      }

      [Fact]
      public void It_should_return_250_on_RSET_before_EHLO()
      {
         // Setup
         var connection = new AsyncFakeConnection();
         var session = CreateSession( connection );

         // Act
         var task = Task.Factory.StartNew( session.Process );
         var result = connection.SendAndReceive( "RSET", true );
         var result2 = connection.SendAndReceive( "EHLO test.example.nl" );

         // Verify
         result.ShouldStartWith( "250" );
         result2.ShouldStartWith( "250" );
         connection.Close();
         task.WaitForCompletion();
      }

      [Fact]
      public void It_should_correctly_parse_recipient_email_addresses()
      {
         // setup
         using ( var connection = new AsyncFakeConnection( new[] { "EHLO test.example.com", "MAIL FROM:<text@example.com> BODY=8BITMIME", "RCPT TO:<you@example.com>", "DATA", "blablah\n", ".\n" } ) )
         {
            var session = CreateSession( connection );
            _mailStoreMock
               .Setup( s => s.Submit( It.IsAny<SmtpMessage>() ) )
               .Callback( ( SmtpMessage m ) =>
                             {
                                m.To.ShouldContain( "you@example.com" );
                                m.From.ShouldEqual( "text@example.com" );
                             } )
               .Verifiable();


            // act
            var task = Task.Factory.StartNew( session.Process );
            connection.SendAndReceive( "QUIT" );


            connection.Close();
            task.WaitForCompletion();
         }

         // verify
         _mailStoreMock.Verify( s => s.Submit( It.IsAny<SmtpMessage>() ) );

      }

      [Fact]
      public void It_should_ignore_forwarding_recipient_email_addresses()
      {
         // setup

         using ( var connection = new AsyncFakeConnection( new[] { "EHLO test.example.com", "MAIL FROM:<text@example.com> BODY=8BITMIME", "RCPT TO:<@test.nl:you@example.com>", "DATA", "blablah\n", ".\n" } ) )
         {
            var session = CreateSession( connection );
            _mailStoreMock
               .Setup( s => s.Submit( It.IsAny<SmtpMessage>() ) )
               .Callback( ( SmtpMessage m ) =>
               {
                  m.To.ShouldContain( "you@example.com" );
                  m.From.ShouldEqual( "text@example.com" );
               } )
               .Verifiable();

            // act
            var task = Task.Factory.StartNew( session.Process );
            connection.SendAndReceive( "QUIT" );


            // verify
            connection.Close();
            task.WaitForCompletion();
         }

         _mailStoreMock.Verify();

      }

      [Fact]
      public void It_should_accept_mail_to_postmaster()
      {
         // setup

         using ( var connection = new AsyncFakeConnection( new[] { "EHLO test.example.com", "MAIL FROM:<text@example.com> BODY=8BITMIME", "RCPT TO:<postmaster>", "DATA", "blablah\n", ".\n" } ) )
         {
            var session = CreateSession( connection );
            _mailStoreMock
               .Setup( s => s.Submit( It.IsAny<SmtpMessage>() ) )
               .Callback( ( SmtpMessage m ) =>
               {
                  m.To.ShouldContain( "postmaster" );
                  m.From.ShouldEqual( "text@example.com" );
               } )
               .Verifiable();

            // act
            var task = Task.Factory.StartNew( session.Process );
            connection.SendAndReceive( "QUIT" );

            // verify
            connection.Close();
            task.WaitForCompletion();
         }

         _mailStoreMock.Verify();

      }
   }
}
