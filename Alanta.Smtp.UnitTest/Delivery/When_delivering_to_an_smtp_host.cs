﻿using System;
using System.Collections.Generic;
using System.Text;
using Alanta.Smtp.ContentProcessing;
using Alanta.Smtp.Delivery;
using Alanta.Smtp.Dns;
using Alanta.Smtp.Logging;
using Moq;
using Xunit;

namespace Alanta.Smtp.UnitTest.Delivery
{
   public class When_delivering_to_an_smtp_host
   {
      [Fact]
      public void It_should_deliver_test_at_alanta_nl_to_mx_alanta_nl()
      {
         // Arrange
         var message = new SmtpMessage {Content = new StringBuilder(), Received = DateTime.UtcNow};
         var dnsResolver = new Mock<IDnsResolver>();
         dnsResolver.Setup(d => d.GetMxRecords("alanta.nl")).Returns(new[] {new MxEntry("u.alanta.nl", 20)}).Verifiable();
         var traceInfo = new TraceInformation(dnsResolver.Object);

         var agent = new SmtpDeliveryAgent(traceInfo,dnsResolver.Object, new ConsoleLogger());
         
         // Act
         agent.Deliver( message, "test@alanta.nl" );

         // Assert
          dnsResolver.VerifyAll();
      }
   }
}
