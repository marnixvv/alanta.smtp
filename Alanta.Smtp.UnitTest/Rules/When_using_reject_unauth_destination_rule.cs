﻿using Alanta.Smtp.Rules;
using Moq;
using Should;
using Xunit;

namespace Alanta.Smtp.UnitTest.Rules
{
   public class When_using_reject_unauth_destination_rule
   {
      [Theory,
      InlineData("alanta.nl", new[]{"jimmy.bob@bogus.com"}),
      InlineData("alanta.nl", new[]{"chris.sharpe@alanta.nl","jimmy.bob@bogus.com"})]
      public void It_should_reject_if_any_recipient_is_in_a_domain_not_relayed_for(string relaydomain, string[] recipients)
      {
         // Arrange
         var rules = new RuleSet(RuleResult.Accepted)
         {
            new Reject_Unauth_Destination()
         };

         var context = new Mock<IRuleContext>(MockBehavior.Strict);
         var relayDomains = new LookupTable
         {
            new DomainLookupRule(relaydomain, "OK")
         };
         context.SetupGet(c => c.RelayDomains).Returns(relayDomains);
         context.SetupGet( c => c.To ).Returns( recipients );

         // Act
         var result = rules.Evaluate(context.Object);

         // Assert
         result.ShouldEqual(RuleResult.Rejected);
      }

      [Theory,
      InlineData( new[]{"alanta.nl"}, new[] { "marnix@alanta.nl" } ),
      InlineData( new[] { "alanta.nl" }, new[] { "marnix@backoffice.alanta.nl" } ),
      InlineData( new[] {"alanta.nl", "bogus.com"}, new[] { "chris.sharpe@alanta.nl", "jimmy.bob@bogus.com" } )]
      public void It_should_not_reject_if_all_recipients_are_in_domains_relayed_for( string[] relaydomains, string[] recipients )
      {
         // Arrange
         var rules = new RuleSet( RuleResult.Accepted )
         {
            new Reject_Unauth_Destination()
         };

         var context = new Mock<IRuleContext>( MockBehavior.Strict );
         var relayDomains = new LookupTable();
         foreach (var domain in relaydomains)
         {
            relayDomains.Add( new DomainLookupRule( domain, "OK" ));
         }
         
         context.SetupGet( c => c.RelayDomains ).Returns( relayDomains );
         context.SetupGet( c => c.To ).Returns( recipients );

         // Act
         var result = rules.Evaluate( context.Object );

         // Assert
         result.ShouldEqual( RuleResult.Accepted );
      }
   }
}
