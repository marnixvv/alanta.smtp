﻿using Alanta.Smtp.Rules;
using Moq;
using Should;
using Xunit;

namespace Alanta.Smtp.UnitTest.Rules
{
  
   public class When_access_rules_are_setup
   {
      [Fact]
      public void It_should_accept_authenticated_session()
      {
         // Arrange
         var ruleSet = new RuleSet(RuleResult.Rejected)
         {
            new Accept_Authenticated()
         };
         var contextMock = new Mock<IRuleContext>();
         contextMock.Setup(c => c.IsAuthenticated).Returns(true);

         // Act
         var result = ruleSet.Evaluate(contextMock.Object);

         // Assert
         result.ShouldEqual(RuleResult.Accepted);

      }

      [Fact]
      public void It_should_accept_mail_for_allowed_domain()
      {
         // Arrange
         var ruleSet = new RuleSet( RuleResult.Rejected )
         {
            new Accept_Mail_For_Domain("alanta.nl")
         };
         var contextMock = new Mock<IRuleContext>();
         contextMock.Setup( c => c.To ).Returns( new[]{ "info@alanta.nl" } );

         // Act
         var result = ruleSet.Evaluate( contextMock.Object );

         // Assert
         result.ShouldEqual( RuleResult.Accepted );

      }

      [Fact]
      public void It_should_not_accept_mail_for_other_domain()
      {
         // Arrange
         var ruleSet = new RuleSet( RuleResult.Rejected )
         {
            new Accept_Mail_For_Domain("alanta.nl")
         };
         var contextMock = new Mock<IRuleContext>();
         contextMock.Setup( c => c.To ).Returns( new string[]{ "info@example.com" } );

         // Act
         var result = ruleSet.Evaluate( contextMock.Object );

         // Assert
         result.ShouldEqual( RuleResult.Rejected );

      }
   }
}
