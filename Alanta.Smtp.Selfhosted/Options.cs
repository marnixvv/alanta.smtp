using CommandLine;
using CommandLine.Text;

namespace Alanta.Smtp.Selfhosted
{
   class Options
   {
      public Options()
      {
          
      }

      [Option( 'c', "cert",
         HelpText = "Path to a PEM encoded certificate file for TLS." )]
      public string CertificateFile { get; set; }

      [Option( 'a', "address",
         HelpText = "IP address to bind the server to. Default is localhost." )]
      public string BindAddress { get; set; }

      [Option( 'p', "port",
         HelpText = "The port bind the server to. Default is 25." )]
      public int? Port { get; set; }

      [Option('o', "output",
         HelpText = "The directory to write e-mails to.")]
      public string OutputPath { get; set; }

      [ParserState]
      public IParserState LastParserState { get; set; }

      [HelpOption]
      public string GetUsage()
      {
         return HelpText.AutoBuild( this,
            ( HelpText current ) => HelpText.DefaultParsingErrorsHandler( this, current ) );
      }
   }
}