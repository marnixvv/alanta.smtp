﻿using System.Threading;

namespace Alanta.Smtp.IntegrationTest
{
	public static class IntegrationTestHelper
	{
		private static int port = 25;
		public static int UniquePort => Interlocked.Increment(ref port);
	}
}
