﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Alanta.Smtp.IntegrationTest
{
   public class GetLocalInterface
   {
      [Fact]
      public void Test()
      {
         foreach (var @interface in Configuration.Network.FindActiveInterfaces())
         {
            Console.WriteLine(@interface.Interace.Name);
            Console.WriteLine( "IPs :"+string.Join(", ", @interface.PublicAddresses.Select( a => a.ToString() )) );
            Console.WriteLine( "DNS :" + string.Join( ", ", @interface.DnsServers.Select( a => a.ToString() ) ) );
         }
      }

      
   }
}
