﻿using System.Net;
using System.Net.Mail;
using Alanta.Smtp.Logging;
using Xunit;

namespace Alanta.Smtp.IntegrationTest
{
	[Collection( "Server tests" )]
	public class When_delivering_email
   {
      [Fact]
      public void It_should_accept_email_to_postmaster()
      {
			// setup
			var port = IntegrationTestHelper.UniquePort;
            var ip = new IPEndPoint(IPAddress.Loopback, port);

            // run server
            var server = new SmtpServer(new DefaultSmtpSessionFactory(), 
                                     new ConsoleLogger(),
                                      ip);

         try
         {
            server.Start();

            // act

            // use .net smtp client to deliver e-mail
            var message = new System.Net.Mail.MailMessage()
                             {
                                From = new MailAddress( "test@alanta.nl" ),
                                To = {"postmaster@alanta.nl"},
                                Body = "Testing!",
                                Subject = "It_should_accept_email_to_postmaster"
                             };

            var smtpClient = new SmtpClient( "localhost" ){ UseDefaultCredentials = false, Port = port };
            smtpClient.Send( message );

         }
         finally 
         {
            server.Stop();   
         }

         // verify

         // assert that mail was accepted         
      }

      [Fact]
      public void It_should_support_starttls()
      {
         // setup
			var port = IntegrationTestHelper.UniquePort;

			// This prevents SmptClient from applying default server certificate validation checks.
			System.Net.ServicePointManager.ServerCertificateValidationCallback = ( sender, certificate, chain, errors ) => true;
            // Force TLS 1.2
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

         // run server
         var server = new SmtpServer( new DefaultSmtpSessionFactory { Certificate = Tls.Certificates.LoadFromPem( Certificate.PEM ) },
                                     new ConsoleLogger(),
                                     new IPEndPoint( IPAddress.Loopback, port ) ) ;
         try
         {
            server.Start();

            // act

            // use .net smtp client to deliver e-mail
            var message = new System.Net.Mail.MailMessage()
            {
               From = new MailAddress( "test@alanta.nl" ),
               To = { "postmaster@alanta.nl" },
               Body = "Testing!",
               Subject = "It_should_accept_email_to_postmaster"
            };

            var smtpClient = new SmtpClient( "localhost" ) { UseDefaultCredentials = false, EnableSsl = true, Port = port };
            smtpClient.Send( message );

         }
         finally
         {
            server.Stop();
         }

         // verify

         // assert that mail was accepted

      }

      [Fact]
      public void It_should_support_starttls_with_internal_client()
      {
			// setup
			var port = IntegrationTestHelper.UniquePort;

			// This prevents SmptClient from applying default server certificate validation checks.
			System.Net.ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, errors) => true;

         // run server
         var server = new SmtpServer(new DefaultSmtpSessionFactory { Certificate = Tls.Certificates.LoadFromPem(Certificate.PEM) },
                                     new PrefixLoggerDecorator( new ConsoleLogger(), "[Server] " ),
                                     new IPEndPoint(IPAddress.Loopback, port));
         try
         {
            server.Start();

            // act

            // use .net smtp client to deliver e-mail
            using (var client = new Alanta.Smtp.Delivery.SmtpSender(new PrefixLoggerDecorator(new ConsoleLogger(), "[Client] ")))
            {
               client.DeliverWithTLS("localhost", "marnix@alanta.nl", "marnix@alanta.nl", "Test", port);
            }

         }
         finally
         {
            server.Stop();
         }

         // verify

         // assert that mail was accepted

      }
   }
}
