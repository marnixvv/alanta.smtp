﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Alanta.Sasl;
using Alanta.Smtp.Logging;
using Xunit;

namespace Alanta.Smtp.IntegrationTest
{
	public class When_using_tls_and_plain_auth
   {
      [Fact]
      public void it_should_accept_mail_for_authenticated_user()
      {
         // setup

         // This prevents SmptClient from applying default server certificate validation checks.
         System.Net.ServicePointManager.ServerCertificateValidationCallback = ( sender, certificate, chain, errors ) => true;
         // Force TLS 1.2
         System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

         // run server
         var authentication = new SimpleAuthenticationService();
         authentication.Add("Bruce","Schneier");

         var server = new SmtpServer(
            new DefaultSmtpSessionFactory
            {
               Certificate = Tls.Certificates.LoadFromPem(Certificate.PEM),
               AuthenticationService = authentication,
              
            },
            new ConsoleLogger(),
            new IPEndPoint(IPAddress.Loopback, 25));
         

         try
         {
            server.Start();

            // act

            // use .net smtp client to deliver e-mail
            var message = new System.Net.Mail.MailMessage()
            {
               From = new MailAddress( "test@alanta.nl" ),
               To = { "pinkie@alanta.nl" },
               Body = "Testing!",
               Subject = "Whatever"
            };

            var smtpClient = new SmtpClient( "localhost" ) { UseDefaultCredentials = false, EnableSsl = true, Credentials = new NetworkCredential( "Dick", "Schneier" )};
            smtpClient.Send( message );

         }
         finally
         {
            server.Stop();
         }

         // verify

         // assert that mail was accepted


      }
   }
}
