using System;
using System.Linq;
using System.Net;
using Alanta.Smtp.Dns;
using Should;
using Xunit;

namespace Alanta.Smtp.IntegrationTest.DNS
{
	public class When_using_the_window_DNS_resolver
	{

		[Fact]
		public void It_should_resolve_mxs_for_alanta_nl()
		{
			// Arrange
			var dnsResolver = new DefaultDnsResolver(new[] { "8.8.8.8" });

			// Act
			var result = dnsResolver.GetMxRecords("alanta.nl");

			// Assert
			result.ShouldNotBeNull();
			result.Length.ShouldBeInRange(1, 20);
			Console.WriteLine(String.Join(", ", result.Select(r => r.Name)));
		}
	}
}