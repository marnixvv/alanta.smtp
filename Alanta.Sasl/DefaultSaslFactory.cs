﻿using System;
using System.Collections.Generic;
using System.Linq;
using Alanta.Sasl.Server;

namespace Alanta.Sasl
{
   /// <summary>
   /// Creates SASL Server mechanisms and handles configuration of supported mechanisms.
   /// </summary>
   public class DefaultSaslFactory : ISaslFactory
   {
      private readonly IAuthenticationService _authenticationService;
      private readonly IAuthorizationService _authorizationService;
      private readonly List<MechanismDescriptor> _mechanisms;

      /// <summary>
      /// Creates a factory for all implemented mechanisms (currently PLAIN only).
      /// </summary>
      /// <param name="authenticationService">The authentication service.</param>
      /// <param name="authorizationService">The authorization service, or <c>null</c> to use the DefaultAuthorizationService.</param>
      public DefaultSaslFactory(IAuthenticationService authenticationService, IAuthorizationService authorizationService = null) 
         : this(new [] {"PLAIN"}, authenticationService, authorizationService ?? new DefaultAuthorizationService())
      {
      }

      /// <summary>
      /// Creates a factory for the specified  mechanisms.
      /// </summary>
      /// <param name="mechanisms">The list of mechanisms to support.</param>
      /// <param name="authenticationService">The authentication service, must not be null.</param>
      /// <param name="authorizationService">The authorization service, must not be null.</param>
      public DefaultSaslFactory( string[] mechanisms, IAuthenticationService authenticationService, IAuthorizationService authorizationService)
      {
         if (mechanisms == null || mechanisms.Length == 0) throw new ArgumentException("Please specify at least 1 mechanism", nameof(mechanisms));
         if (authenticationService == null) throw new ArgumentNullException(nameof(authenticationService));
         if (authorizationService == null) throw new ArgumentNullException(nameof(authorizationService));
         _authenticationService = authenticationService;
         _authorizationService = authorizationService;
         _mechanisms = mechanisms.Select(SetupMechanism).ToList();
      }

      private MechanismDescriptor SetupMechanism(string name)
      {
         if( string.IsNullOrWhiteSpace(name)) throw new ArgumentException("Mechanism name must not be null, empty or whitespace.", nameof(name));

         switch (name.ToUpperInvariant())
         {
            case "PLAIN":
               return new MechanismDescriptor
               {
                  Name = "PLAIN",
                  RequiresEncryptedChannel = true,
                  Factory = (auth, authz) => new PlainMechanism(auth, authz)
               };
               //break;
            default:
               throw new SaslException("Unknown mechanism : "+name);
               //break;
         }
      }

      public SaslServerMechanism GetMechanism(string name)
      {
         if( string.IsNullOrWhiteSpace(name))
            throw new ArgumentException("name must not be null or empty.", nameof(name));

         var mechanism = _mechanisms.FirstOrDefault(m => string.Equals(m.Name, name, StringComparison.OrdinalIgnoreCase));
         if (null == mechanism) throw new SaslException("Mechanism not supported: " + name);

         return mechanism.Factory(_authenticationService, _authorizationService);
      }

      public string[] GetSupportedMechanisms(bool requiresSecureChannel)
      {
         return _mechanisms.Where(m => requiresSecureChannel || !m.RequiresEncryptedChannel)
            .Select(m => m.Name)
            .ToArray();
      }

      private class MechanismDescriptor
      {
         public string Name { get; set; }
         public bool RequiresEncryptedChannel { get; set; }
         public Func<IAuthenticationService, IAuthorizationService, SaslServerMechanism> Factory { get; set; } 
      }
   }
}
