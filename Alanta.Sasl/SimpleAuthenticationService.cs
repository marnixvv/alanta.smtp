﻿using System;
using System.Collections.Generic;

namespace Alanta.Sasl
{
   /// <summary>
   /// A really naive implementation of an authentication service.
   /// </summary>
   public class SimpleAuthenticationService : IAuthenticationService
   {
      private readonly Dictionary<string,string> _users = new Dictionary<string, string>( StringComparer.OrdinalIgnoreCase ); 

      public bool Authenticate(string userId, string password)
      {
         string storedPassword;
         return _users.TryGetValue(userId, out storedPassword) && string.Equals(password, storedPassword, StringComparison.Ordinal);
      }

      public void Add( string userId, string password )
      {
         if (string.IsNullOrWhiteSpace(userId)) 
            throw new ArgumentException("userId must not be null, empty or all whitespace.", nameof(userId));
         if ( string.IsNullOrWhiteSpace( password ) )
            throw new ArgumentException( "password must not be null, empty or all whitespace.", nameof(password) );

         if ( _users.ContainsKey( userId ) )
            _users[userId] = password;
         else
         {
            _users.Add( userId, password );
         }
      }
   }
}