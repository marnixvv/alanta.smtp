﻿namespace Alanta.Sasl
{
   public interface IAuthenticationService
   {
      bool Authenticate(string userId, string password);
   }
}
