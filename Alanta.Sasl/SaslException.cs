﻿using System;

// Taken from s22.Sasl   
// https://github.com/smiley22/S22.Sasl

/* The MIT License

Copyright (c) 2013-2014 Torben Könke
Copyright (c) 2015 Alanta, Marnix van Valen

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom 
the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE 
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

namespace Alanta.Sasl
{
   /// <summary>
   /// The exception is thrown when a Sasl-related error or unexpected condition occurs.
   /// </summary>
   internal class SaslException : Exception
   {
      /// <summary>
      /// Initializes a new instance of the SaslException class
      /// </summary>
      public SaslException() : base() { }

      /// <summary>
      /// Initializes a new instance of the SaslException class with its message
      /// string set to <paramref name="message"/>.
      /// </summary>
      /// <param name="message">A description of the error. The content of message is intended
      /// to be understood by humans.</param>
      public SaslException( string message ) : base( message ) { }

      /// <summary>
      /// Initializes a new instance of the SaslException class with its message
      /// string set to <paramref name="message"/> and a reference to the inner exception that
      /// is the cause of this exception.
      /// </summary>
      /// <param name="message">A description of the error. The content of message is intended
      /// to be understood by humans.</param>
      /// <param name="inner">The exception that is the cause of the current exception.</param>
      public SaslException( string message, Exception inner ) : base( message, inner ) { }
   }
}
