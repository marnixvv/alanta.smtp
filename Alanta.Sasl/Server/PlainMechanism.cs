﻿using System;
using System.Text;

namespace Alanta.Sasl.Server
{
   public class PlainMechanism : SaslServerMechanism
   {
      private readonly IAuthenticationService _authenticationService;
      private readonly IAuthorizationService _authorizationService;

      public PlainMechanism( IAuthenticationService authenticationService, IAuthorizationService authorizationService )
         : base( "PLAIN" )
      {
         if ( authenticationService == null )
            throw new ArgumentNullException( nameof(authenticationService) );
         if ( authorizationService == null )
            throw new ArgumentNullException( nameof(authorizationService) );
         _authenticationService = authenticationService;
         _authorizationService = authorizationService;
      }

      public override bool RequiresTls { get { return true; } }

      protected override byte[] GetChallenge(byte[] challenge, out bool isComplete)
      {
         if (challenge == null || challenge.Length == 0)
         {
            // initial challange is blank
            isComplete = false;
            return null;
         }

         // PLAIN mechanism has no round trip so we're done
         isComplete = true;

         // decode
         string challengeString;
         try
         {
            challengeString = Encoding.UTF8.GetString( challenge, 0, challenge.Length );
         }
         catch (Exception ex)
         {
            throw new SaslException("Invalid response; unable to decode.", ex);
         }

         var parts = challengeString.Split('\0');

         if( parts.Length < 2 || parts.Length > 3 )
            throw new SaslException( "Invalid response; unexpected content." );

         var userId = parts.Length == 2 ? parts[0] : parts[1];
         var authorizationId = parts.Length == 2 ? null : parts[0];
         var password = parts.Length == 2 ? parts[1] : parts[2];

         // verify
         var result = Verify(authorizationId, userId, password);
         FireAuthorizationResult(result);
         return null;
      }

      private SaslResult Verify( string authorizeAsId, string userId, string password )
      {
         if ( userId == null )
            throw new ArgumentNullException( nameof(userId) );
         if ( password == null )
            throw new ArgumentNullException( nameof(password) );

         if ( !_authenticationService.Authenticate( userId, password ) )
         {
            return SaslResult.BadOrUnknownCredentials; // incorrect password
         }

         if ( authorizeAsId == null )
         {
            authorizeAsId = userId;//DeriveAuthzid( userId );
            if ( string.IsNullOrEmpty( authorizeAsId ) )
            {
               return SaslResult.BadOrUnknownCredentials; // could not derive authorizeAsId
            }
         }

         if ( !_authorizationService.Authorize( userId, authorizeAsId ) )
         {
            return SaslResult.NotAuthorized; // not authorized
         }

         return SaslResult.Success;
      }


   }
}