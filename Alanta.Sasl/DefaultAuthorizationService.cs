using System;

namespace Alanta.Sasl
{
   public class DefaultAuthorizationService : IAuthorizationService
   {
      public bool Authorize(string userId, string authorizeAsId)
      {
         return string.Equals(userId, authorizeAsId, StringComparison.OrdinalIgnoreCase);
      }
   }
}