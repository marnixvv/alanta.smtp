namespace Alanta.Sasl
{
   public interface IAuthorizationService
   {
      bool Authorize(string userId, string authorizeAsId);
   }
}