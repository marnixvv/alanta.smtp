namespace Alanta.Sasl
{
   public interface ISaslFactory
   {
      /// <summary>
      /// Get a mechanism instance.
      /// </summary>
      /// <param name="name">The name of the mechanism.</param>
      /// <returns>A new instance of a mechanism.s</returns>
      SaslServerMechanism GetMechanism(string name);

      /// <summary>
      /// Returns a list of supported mechanisms.
      /// </summary>
      /// <param name="requiresSecureChannel"><c>true</c> to include mechanisms that require a secure communication channel (i.e. TLS).</param>
      /// <returns>A list of mechanism names.</returns>
      string[] GetSupportedMechanisms(bool requiresSecureChannel);
   }
}