﻿using System;

namespace Alanta.Sasl
{
   /// <summary>
   /// The abstract base class from which all classes implementing a Sasl
   /// authentication mechanism must derive.
   /// </summary>
   public abstract class SaslServerMechanism
   {
      /// <summary>
      /// Create a new instace of this mechanism.
      /// </summary>
      /// <param name="name">IANA registered name of the mechanism.</param>
      protected SaslServerMechanism( string name )
      {
         if ( string.IsNullOrWhiteSpace( name ) )
            throw new ArgumentException( "Name must not be null, empty or whitespace.", nameof(name) );
         Name = name;
      }

      /// <summary>
      /// IANA name of the authentication mechanism.
      /// </summary>
      public string Name { get; }

		public virtual bool RequiresTls => false;

	   public event EventHandler<SaslEventArgs> AuthorizationResult;

      protected void FireAuthorizationResult(SaslResult result)
      {
	      AuthorizationResult?.Invoke( this, new SaslEventArgs { Result = result } );
      }

	   /// <summary>
      /// Gets the response to a challenge.
      /// </summary>
      /// <param name="challenge"></param>
      /// <param name="isComplete"></param>
      /// <returns>The client response to the specified challenge.</returns>
      protected abstract byte[] GetChallenge( byte[] challenge, out bool isComplete );


      /// <summary>
      /// Retrieves the base64-encoded client response for the specified
      /// base64-encoded challenge sent by the server.
      /// </summary>
      /// <param name="response">A base64-encoded string representing a response
      /// sent by the client.</param>
      /// <param name="isComplete"></param>
      /// <returns>A base64-encoded string representing the server challenge to the
      /// client response, or the initial challenge.</returns>
      /// <remarks>The IMAP, POP3 and SMTP authentication commands expect challenges
      /// and responses to be base64-encoded. This method automatically decodes the
      /// server challenge before passing it to the Sasl implementation and
      /// encodes the client response to a base64-string before returning it to the
      /// caller.</remarks>
      /// <exception cref="SaslException">The server challenge could not be retrieved.
      /// Refer to the inner exception for error details.</exception>
      public string GetChallenge( string response, out bool isComplete )
      {
         try
         {
            byte[] data = string.IsNullOrEmpty( response ) ? new byte[0] :
               Convert.FromBase64String( response );
            byte[] challenge = GetChallenge( data, out isComplete );
            return challenge != null ? Convert.ToBase64String( challenge ) : null;
         }
         catch ( Exception e )
         {
            throw new SaslException( "The challenge-response could not be retrieved.", e );
         }
      }

   }

   public class SaslEventArgs : EventArgs
   {
      public SaslResult Result { get; set; }
      public string AuthorizedUser { get; set; }
   }

   public enum SaslResult
   {
      Unknown = 0,
      Success = 1,
      BadOrUnknownCredentials = 2,
      NotAuthorized = 3
   }
}