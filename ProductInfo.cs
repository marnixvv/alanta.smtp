﻿using System.Reflection;

#if DEBUG
[assembly: AssemblyConfiguration( "Debug" )]
#else
[assembly: AssemblyConfiguration( "Release" )]
#endif
[assembly: AssemblyCompany( "Alanta" )]
[assembly: AssemblyProduct( "Alanta.Smtp" )]
[assembly: AssemblyCopyright( "Copyright © Alanta 2013-2017" )]
[assembly: AssemblyTrademark( "" )]
[assembly: AssemblyCulture( "" )]

[assembly: AssemblyVersion( "0.1.1.0" )]
[assembly: AssemblyFileVersion( "0.1.1.0" )]
