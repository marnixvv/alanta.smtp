namespace Alanta.Smtp.Routing
{
   public abstract class MessageRouteBase
   {
      public abstract bool CanHandleMessage( MessageRoutingContext message );
      public abstract void HandleMessage( MessageRoutingContext message );
   }
}