using System;
using Alanta.Smtp.ContentProcessing;

namespace Alanta.Smtp.Routing
{
   public class MessageRoutingContext
   {
      public MessageRoutingContext( SmtpMessage message, string recipient, IMessageProcessor messageProcessor )
      {
         if ( message == null ) throw new ArgumentNullException( nameof(message) );
         if ( recipient == null ) throw new ArgumentNullException( nameof(recipient) );
         if ( messageProcessor == null ) throw new ArgumentNullException( nameof(messageProcessor) );
         Message = message;
         Recipient = recipient;
         MessageProcessor = messageProcessor;
      }

      public string Recipient { get; private set; }
      public SmtpMessage Message { get; private set; }
      public IMessageProcessor MessageProcessor { get; private set; }
   }
}