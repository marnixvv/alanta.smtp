﻿using System;
using Alanta.Smtp.Delivery;

namespace Alanta.Smtp.Routing
{
   public class DeliveryAgentRoute : MessageRouteBase
   {
      private readonly IDeliveryAgent _deliveryAgent;

      public DeliveryAgentRoute( IDeliveryAgent deliveryAgent)
      {
         if ( deliveryAgent == null ) throw new ArgumentNullException( nameof(deliveryAgent) );
         _deliveryAgent = deliveryAgent;
      }

      public override bool CanHandleMessage( MessageRoutingContext context )
      {
         return true;
      }

      public override void HandleMessage( MessageRoutingContext context )
      {
         _deliveryAgent.Deliver( context.Message, context.Recipient );
      }
   }
}
