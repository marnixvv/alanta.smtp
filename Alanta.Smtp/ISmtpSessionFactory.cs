﻿using System.Net;

namespace Alanta.Smtp
{
   public interface ISmtpSessionFactory
   {
      SmtpSession CreateSession( IServerConnection connection, IPAddress localAddress, IPAddress remoteAddress );
   }
}