using System;
using System.IO;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Alanta.Smtp.Logging;

namespace Alanta.Smtp
{
   public class NetworkConnection : IServerConnection
   {
      private readonly Socket _socket;
      private readonly ILogger _log;
      private Stream _stream;
      private StreamReader _streamReader;
      private const SslProtocols DefaultTlsProtocols = SslProtocols.Tls12;

      public NetworkConnection( Socket socket, ILogger log )
      {
         if ( socket == null ) throw new ArgumentNullException( nameof(socket) );
         if ( log == null ) throw new ArgumentNullException( nameof(log) );
         _socket = socket;
         _log = log;

         _stream = new NetworkStream( _socket );
         _streamReader = new StreamReader( _stream );
      }

      public string ReadLine()
      {
         var line = _streamReader.ReadLine();
         if ( !string.IsNullOrEmpty( line ) )
         {
            _log.Debug( "Received : " + line );
         }
         return line;
      }

      public void Send( string data )
      {
         if( !data.EndsWith( "\r\n" ))
         {
            data = data + "\r\n";
         }

         _log.Debug( "Sent : " + data );
         var buffer = Encoding.ASCII.GetBytes( data );
         _stream.Write( buffer, 0, buffer.Length );
      }

      public bool Connected
      {
         get { return _socket.Connected; }
      }

      public void Disconnect()
      {
         if ( _socket.Connected )
         {
            _log.Debug( "Disconnecting {0}", _socket.LocalEndPoint );
            _socket.Shutdown( SocketShutdown.Send );
	         _socket.Dispose();
         }
      }

      /// <summary>
      /// Start TLS for the server end of a connection.
      /// </summary>
      /// <param name="serverCertificate">The server certificate.</param>
      /// <param name="allowedTlsProtocols">The allowed TLS protocols. Defaults to TLS 1.2 since older versions are considered insecure.</param>
      /// <returns><c>true</c> if TLS was started succesfully.</returns>
      public async Task<bool> StartTls( X509Certificate serverCertificate, SslProtocols allowedTlsProtocols = 0 )
      {
         var sslStream = new SslStream( _stream, true, ( sender, certificate, chain, errors ) => true );
         try
         {
            await sslStream.AuthenticateAsServerAsync( serverCertificate, false, allowedTlsProtocols != 0 ? allowedTlsProtocols : DefaultTlsProtocols, true );
            _log.Info( "TLS negotiation completed." );
            _log.Info( "Protocol: {0}", sslStream.SslProtocol );
#if !NETCORE
				_log.Info( "IsEncrypted: {0}", sslStream.IsEncrypted );
            _log.Info( "IsServer: {0}", sslStream.IsServer );
            _log.Info( "IsAuthenticated: {0}", sslStream.IsAuthenticated );
#endif

                _stream = sslStream;
            _streamReader = new StreamReader( _stream );
         }
         catch (AuthenticationException e)
         {
            _log.Error( "SSL Auth error: "+e.Message);
            return false;
         }
         return true;
      }


      /// <summary>
      /// Start TLS for the client end of a connection.
      /// </summary>
      /// <param name="host">The server name we're connecting to. This will be verified against the certificates presented
      /// by the server.</param>
      /// <returns></returns>
      public async Task<bool> StartClientTls( string host )
      {
         var sslStream = new SslStream(_stream, false, (sender, certificate, chain, errors) => true, null);
         try
         {
            await sslStream.AuthenticateAsClientAsync(host, new X509CertificateCollection(), SslProtocols.Tls12, false);
            _log.Info("TLS negotiation completed.");
#if !NETCORE
				_log.Info("IsEncrypted: {0}", sslStream.IsEncrypted);
            _log.Info("IsServer: {0}", sslStream.IsServer);
            _log.Info("IsAuthenticated: {0}", sslStream.IsAuthenticated);
#endif
            _stream = sslStream;
            _streamReader = new StreamReader(_stream);
         }
         catch (AuthenticationException e)
         {
            _log.Error("SSL Auth error: " + e.Message);
            return false;
         }
         return true;
      }

      void IDisposable.Dispose()
      {
         Dispose( true );
         GC.SuppressFinalize( this );
      }

      protected virtual void Dispose( bool disposing )
      {
         if ( !_disposed )
         {
            if ( disposing )
            {
               // Free other state (managed objects).
               _streamReader.Dispose();
            }
            // Free your own state (unmanaged objects).
            // Set large fields to null.
            Disconnect();

            _disposed = true;
         }
      }

      ~NetworkConnection()
      {
         Dispose( false );
      }

      private bool _disposed;
   }
}