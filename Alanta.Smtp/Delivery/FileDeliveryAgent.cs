using Alanta.Smtp.ContentProcessing;

namespace Alanta.Smtp.Delivery
{
    public class FileDeliveryAgent : IDeliveryAgent
    {
        private readonly TraceInformation _traceInformation;
        private readonly string _basePath;

        public FileDeliveryAgent(TraceInformation traceInformation, string basePath)
        {
            _traceInformation = traceInformation;
            _basePath = basePath;
        }

        public void Deliver(SmtpMessage message, string recipient)
        {
            // do delivery
            var contentProcessor = new MessageContentProcessor(message.Content);
            var path = System.IO.Path.Combine(_basePath, message.MessageId + ".txt");

            System.IO.File.WriteAllText(path, contentProcessor.PrepareForDelivery(message.From, false, _traceInformation.GetReceivedHeaderTemplate(message, recipient)).ToString());
        }
    }
}