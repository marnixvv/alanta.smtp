using System;
using Alanta.Smtp.ContentProcessing;

namespace Alanta.Smtp.Delivery
{
    public class ConsoleDeliveryAgent : IDeliveryAgent
    {
      private readonly TraceInformation _traceInformation;

      public ConsoleDeliveryAgent( TraceInformation traceInformation )
      {
         _traceInformation = traceInformation;
      }

      public void Deliver( SmtpMessage message, string recipient )
      {
         // do delivery
         var contentProcessor = new MessageContentProcessor( message.Content );

         Console.WriteLine( contentProcessor.PrepareForDelivery( message.From, false,
                                              _traceInformation.GetReceivedHeaderTemplate( message, recipient ) ) );
         
         
      }
   }
}
