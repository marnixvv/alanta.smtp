using Alanta.Smtp.ContentProcessing;

namespace Alanta.Smtp.Delivery
{
    public interface IDeliveryAgent
    {
        void Deliver( SmtpMessage message, string recipient );
    }
}