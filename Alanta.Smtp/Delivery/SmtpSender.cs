﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using Alanta.Smtp.Logging;

namespace Alanta.Smtp.Delivery
{
   public class SmtpSender : IDisposable
   {
      private readonly ILogger _logger;
      private TcpClient _client;

      public SmtpSender( ILogger logger )
      {
         if (logger == null) throw new ArgumentNullException(nameof(logger));
         _logger = logger;
      }

      public void DeliverWithTLS(string remoteHost, string from, string to, string contents, int port = 0)
      {
         using (var connection = OpenConnection( remoteHost, port == 0 ? 465 : port ))
         {
            connection.ReadLine();
            connection.Send("EHLO "+ Environment.GetEnvironmentVariable( "COMPUTERNAME" ) );

            if (!Succeeded(connection, "250"))
            {
               Quit(connection);
               return;
            }

            connection.Send("STARTTLS");

            if (!Succeeded(connection, "220"))
            {
               Quit(connection);
               return;
            }

            if (!connection.StartClientTls(remoteHost).Result) // Todo: make this async all the way down
            {
               _logger.Error("TLS failed - aborted sending.");
               connection.Disconnect();
               return;
            }

            connection.Send("EHLO " + Environment.GetEnvironmentVariable( "COMPUTERNAME" ) );
            if (!Succeeded(connection, "250"))
            {
               Quit(connection);
               return;
            }

            connection.Send(string.Format("MAIL FROM:<{0}>", from));
            if (!Succeeded(connection, "250"))
            {
               Quit(connection);
               return;
            }

            connection.Send(string.Format("RCPT TO:<{0}>", to));
            if (!Succeeded(connection, "250"))
            {
               Quit(connection);
               return;
            }

            connection.Send("DATA");
            if (!Succeeded(connection, "354"))
            {
               Quit(connection);
               return;
            }
            connection.Send(contents);
            connection.Send(".");

            if (!Succeeded(connection, "250"))
            {
               Quit(connection);
               return;
            }
            Quit( connection );
         }
      }

      private void Quit(INetworkConnection connection)
      {
         connection.Send("QUIT");
      }

      private bool Succeeded(INetworkConnection connection, string expectedStatus )
      {
         string response = null;
         do
         {
            response = connection.ReadLine();
            var match = Regex.Match(response, "[0-9]+ ");
            if (match.Success)
            {
               return (match.Value.Trim() == expectedStatus );
            }
         } while ( !string.IsNullOrWhiteSpace(response) );

         return false;
      }

      private IClientConnection OpenConnection(string hostname, int port)
      {
	      var socket = new Socket(SocketType.Stream, ProtocolType.Tcp);
	      socket.Connect(hostname, port);
			_client = new TcpClient(AddressFamily.InterNetwork) { Client = socket };
         return new ClientConnection( _client.Client, _logger );
      }

      public void Dispose()
      {
         if( null != _client )
            ((IDisposable)_client).Dispose();
      }
   }
}
