﻿using System;
using System.Linq;
using Alanta.Smtp.ContentProcessing;
using Alanta.Smtp.Dns;
using Alanta.Smtp.Logging;

namespace Alanta.Smtp.Delivery
{
   public class SmtpDeliveryAgent
   {
      private readonly TraceInformation _traceInformation;
      private readonly IDnsResolver _dnsResolver;
      private readonly ILogger _logger;

      public SmtpDeliveryAgent( TraceInformation traceInformation, IDnsResolver dnsResolver, ILogger logger )
      {
         if (traceInformation == null) throw new ArgumentNullException(nameof(traceInformation));
         if (dnsResolver == null) throw new ArgumentNullException(nameof(dnsResolver));
         if (logger == null) throw new ArgumentNullException(nameof(logger));
         _traceInformation = traceInformation;
         _dnsResolver = dnsResolver;
         _logger = logger;
      }

      public void Deliver( SmtpMessage message, string recipient )
      {
         // do delivery
         var contentProcessor = new MessageContentProcessor( message.Content );

			_logger.Info( "{0}", contentProcessor.PrepareForDelivery( message.From, false,
                                              _traceInformation.GetReceivedHeaderTemplate( message, recipient ) ) );

         // parse recipient to find destination domain
         var destinationHost = GetRemoteHostFor(EmailAddress.Domain( recipient ));
         
         // connect to the server using ssl

         // do the SMTP chat thing

         // deliver and get out
      }

      public string GetRemoteHostFor(string hostPart)
      {
         // use DNS to figure out MX for recipient
         var mxList = _dnsResolver.GetMxRecords( hostPart );
         if (mxList != null && mxList.Length > 0)
         {
            return mxList.First().Name;
         }
         _logger.Debug("No MX records found for '{0}', using that as the destination address.", hostPart);
         return hostPart;
      }
   }
}
