﻿using System.Threading.Tasks;

namespace Alanta.Smtp.Delivery
{
   public interface IClientConnection : INetworkConnection
   {
      Task<bool> StartClientTls(string host);
   }
}