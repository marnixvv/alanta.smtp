using System;
using System.Collections.Generic;
using System.Net;

namespace Alanta.Smtp.Rules
{
   /// <summary>
   /// Provides a read-only snapshot of an SMTP session for rule processing.
   /// </summary>
   public interface IRuleContext
   {
      /// <summary>
      /// True if the connection is encrypted (TLS).
      /// </summary>
      bool IsEncrypted { get; }
      /// <summary>
      /// True if the client is authenticated.
      /// </summary>
      bool IsAuthenticated { get; }
      /// <summary>
      /// The authorized user name of the client.
      /// </summary>
      string AuthorizedUser { get; }

      /// <summary>
      /// Current state of the SMTP session.
      /// </summary>
      SmtpSession.States State { get; }
      /// <summary>
      /// The maximum message size, if configured.
      /// </summary>
      uint? MaximumMessageSize { get; }

      /// <summary>
      /// The SMTP server host name. Used in the EHLO/HELO greeter.
      /// </summary>
      string HostName { get; }

      /// <summary>
      /// The protocol name. ESMTP or SMTP.
      /// </summary>
      string ProtocolName { get; }

      /// <summary>
      /// The hostname provided by the remote SMTP client. Not to be trusted!
      /// </summary>
      string ProvidedRemoteName { get; }

      /// <summary>
      /// The remote IP address.
      /// </summary>
      IPAddress LocalIP { get; }

      /// <summary>
      /// The remote IP address.
      /// </summary>
      IPAddress RemoteIP { get; }

      /// <summary>
      /// The internal tracking id of the message transaction. Mostly useful for logging.
      /// </summary>
      Guid MessageId { get; }

      /// <summary>
      /// The sender of the message as specified in the MAIL FROM command.
      /// </summary>
      string From { get; }

      /// <summary>
      /// The list of recipients, as specified in the RCPT TO command(s).
      /// </summary>
      IReadOnlyCollection<string> To { get; }

      /// <summary>
      /// The lookup table for domains this MTA is allowed to relay for.
      /// </summary>
      ILookupTable RelayDomains { get; }

      //void RaiseSmtpResult(string result);
   }
}