﻿namespace Alanta.Smtp.Rules
{
   public interface ILookupRule
   {
      string Value { get; }
      bool IsMatch(string valueToMatch);
   }
}