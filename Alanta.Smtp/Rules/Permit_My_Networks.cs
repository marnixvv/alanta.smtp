using System.Net;

namespace Alanta.Smtp.Rules
{
   public class Permit_My_Networks : IAccessRule
   {
      public RuleResult? Evaluate( IRuleContext ruleContext )
      {
         if ( ruleContext.RemoteIP.Equals( IPAddress.Loopback ) 
            || ruleContext.LocalIP.Equals( ruleContext.RemoteIP ) )
            return RuleResult.Accepted;

         return null;
      }
   }
}