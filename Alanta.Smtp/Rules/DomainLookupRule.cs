﻿using System;

namespace Alanta.Smtp.Rules
{
   public class DomainLookupRule : ILookupRule
   {
	   public DomainLookupRule(string domain, string value)
      {
         if (domain == null) throw new ArgumentNullException(nameof(domain));
         Domain = domain;
         Value = value;
      }

      public string Domain { get; }

	   public string Value { get; }

	   public bool IsMatch(string valueToMatch)
      {
         // very naive implementation, doesn't support punycode or anything like that
         return valueToMatch.EndsWith( Domain, StringComparison.OrdinalIgnoreCase );
      }
   }
}