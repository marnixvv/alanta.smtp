﻿using System.Text;
using System.Threading.Tasks;

namespace Alanta.Smtp.Rules
{
   public class Reject_Unauth_Destination : IAccessRule
   {
      /* reject_unauth_destination
Reject the request unless one of the following is true:
Postfix is mail forwarder: the resolved RCPT TO domain matches $relay_domains or a subdomain thereof, and contains no sender-specified routing (user@elsewhere@domain),
Postfix is the final destination: the resolved RCPT TO domain matches $mydestination, $inet_interfaces, $proxy_interfaces, $virtual_alias_domains, or $virtual_mailbox_domains, and contains no sender-specified routing (user@elsewhere@domain).
       */
      public RuleResult? Evaluate( IRuleContext ruleContext )
      {
         return !ruleContext.RelayDomains.MatchAll(ruleContext.To) ? RuleResult.Rejected : (RuleResult?)null;
      }
   }
}
