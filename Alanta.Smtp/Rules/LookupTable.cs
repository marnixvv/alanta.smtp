﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Alanta.Smtp.Rules
{
   /// <summary>
   /// An ordered set of lookup rules.
   /// </summary>
   /// <remarks>
   /// This is (loosly) based on Postfix lookup tables. 
   /// See http://www.postfix.org/DATABASE_README.html.
   /// </remarks>
   public class LookupTable : ILookupTable, IEnumerable<ILookupRule>
   {
      public LookupTable()
      {
         this._rules = new List<ILookupRule>();
      }

      public LookupTable( IEnumerable<ILookupRule> items )
      {
         if (items == null) throw new ArgumentNullException(nameof(items));
         this._rules = items.ToList();
      }

      public void Add(ILookupRule rule)
      {
         this._rules.Add(rule);
      }

      public bool TryLookup(string key, out string value)
      {
         foreach (var rule in _rules)
         {
            if (rule.IsMatch(key))
            {
                value = rule.Value;
               return true;
            }
         }

         value = null;
         return false;
      }

      public bool MatchAll(IReadOnlyCollection<string> keys)
      {
         if (keys == null || keys.Count == 0) 
            return true;

         foreach (var key in keys)
         {
            string value;
            if( !TryLookup(key, out value) )
               return false;
         }
         return true;
      }

      private readonly List<ILookupRule> _rules;


      public IEnumerator<ILookupRule> GetEnumerator()
      {
         return _rules.GetEnumerator();
      }

      IEnumerator IEnumerable.GetEnumerator()
      {
         return GetEnumerator();
      }
   }
}