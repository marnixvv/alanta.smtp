﻿using System;

namespace Alanta.Smtp.Rules
{
   public class KeyValueRule : ILookupRule
   {
	   public KeyValueRule(string key, string value)
      {
         if (key == null) throw new ArgumentNullException(nameof(key));
         Key = key;
         Value = value;
      }

      public string Key { get; }
	   public string Value { get; }

	   public bool IsMatch(string valueToMatch)
      {
         return string.Equals(Key, valueToMatch, StringComparison.OrdinalIgnoreCase);
      }
   }
}