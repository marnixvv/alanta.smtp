namespace Alanta.Smtp.Rules
{
   public enum RuleResult
   {
      Rejected = 0,
      Accepted = 1,
      Deferred = 2
   }
}