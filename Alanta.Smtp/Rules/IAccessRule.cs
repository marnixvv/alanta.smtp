namespace Alanta.Smtp.Rules
{
   public interface IAccessRule
   {
      RuleResult? Evaluate( IRuleContext ruleContext );
   }
}