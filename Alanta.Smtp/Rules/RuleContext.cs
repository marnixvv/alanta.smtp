﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Alanta.Smtp.Rules
{
   /// <summary>
   /// Provides a read-only snapshot of an SMTP session for rule processing.
   /// </summary>
   public class RuleContext : IRuleContext
   {
      public RuleContext(ISmtpSessionContext sessionContext, ILookupTable relayDomains, string recipient)
      {
         if (sessionContext == null) throw new ArgumentNullException(nameof(sessionContext));
         if (relayDomains == null) throw new ArgumentNullException(nameof(relayDomains));

         IsEncrypted = sessionContext.IsEncrypted;
         AuthorizedUser = sessionContext.AuthorizedUser;
         IsAuthenticated = sessionContext.IsAuthenticated;
         State = sessionContext.State;
         MaximumMessageSize = sessionContext.MaximumMessageSize;
         HostName = sessionContext.HostName;
         ProtocolName = sessionContext.ProtocolName;
         ProvidedRemoteName = sessionContext.ProvidedRemoteName;
         RemoteIP = sessionContext.RemoteIP;
         LocalIP = sessionContext.LocalIP;

         if (sessionContext.CurrentTransaction != null)
         {
            MessageId = sessionContext.CurrentTransaction.MessageId;
            From = sessionContext.CurrentTransaction.From;
            To = string.IsNullOrWhiteSpace(recipient) ? sessionContext.CurrentTransaction.To.ToArray() : new []{ recipient};
         }
         else
         {
            To = new string[0];
         }

         RelayDomains = relayDomains;
      }

      public bool IsEncrypted { get; }
      public bool IsAuthenticated { get; }
      public string AuthorizedUser { get; }
      public SmtpSession.States State { get; }
      public uint? MaximumMessageSize { get; }
      public string HostName { get; }
      public string ProtocolName { get; }
      public string ProvidedRemoteName { get; }
      public IPAddress LocalIP { get; }
      public IPAddress RemoteIP { get; }

      public Guid MessageId { get; }
      public string From { get; }
      public IReadOnlyCollection<string> To { get; }
      
      public ILookupTable RelayDomains { get; }
   }
}