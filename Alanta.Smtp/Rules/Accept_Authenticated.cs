namespace Alanta.Smtp.Rules
{
   // Postfix : permit_sasl_authenticated
   public class Accept_Authenticated : IAccessRule
   {
      public RuleResult? Evaluate( IRuleContext ruleContext )
      {
         if( ruleContext.IsAuthenticated )
            return RuleResult.Accepted;

         return null;
      }
   }
}