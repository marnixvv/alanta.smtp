﻿using System.Linq;

namespace Alanta.Smtp.Rules
{
   public class Accept_Mail_For_Domain : IAccessRule
   {
      private readonly string _domain;

      public Accept_Mail_For_Domain( string domain )
      {
         _domain = domain;
      }

      public RuleResult? Evaluate( IRuleContext ruleContext )
      {
         if (ruleContext.To.Any())
         {
            if (ruleContext.To.All(a => a.EndsWith(_domain)))
            {
               return RuleResult.Accepted;
            }
         }
         return null;
      }
   }
}