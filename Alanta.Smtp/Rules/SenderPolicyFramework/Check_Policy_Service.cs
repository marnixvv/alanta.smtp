﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Alanta.Smtp.Dns;

namespace Alanta.Smtp.Rules.SenderPolicyFramework
{
	public class Check_Policy_Service : IAccessRule
	{
		private readonly IDnsResolver _resolver;

		public Check_Policy_Service( IDnsResolver resolver )
		{
			if (resolver == null) throw new ArgumentNullException(nameof(resolver));
			_resolver = resolver;
		}

		public RuleResult? Evaluate(IRuleContext ruleContext)
		{
			var spfResult = CheckHost(ruleContext.RemoteIP, ruleContext.From, EmailAddress.Domain(ruleContext.From),
				new MeteredDnsResolverDecorator(_resolver, 10));

			switch (spfResult)
			{
				case Result.Pass:
					return RuleResult.Accepted;
				case Result.Permerror:
				case Result.Fail:
					return RuleResult.Rejected;
				case Result.Temperror:
				case Result.Softfail:
					return RuleResult.Deferred;
				default:
					return null;
			}
		}

		public Result CheckHost( IPAddress ip, string sender, string domain, IMeteredDnsResolver resolver )
		{
			if ( !EmailAddress.HasLocalPart( sender ) )
				sender = EmailAddress.SetLocalPart( sender, "postmaster" );

			// section 4.3 : invalid domain name
			if ( !DomainName.IsProperlyFormed( domain ) )
				return Result.None;

			// section 4.3 : unresolvable domain name
			if ( !resolver.Resolve( domain ).Any() )
				return Result.None;

			// fetch & parse spf record
			var txtRecords = _resolver.GetTxtRecords(domain);

			SpfRecord spfRecord = txtRecords.Select(SpfRecordParser.Parse).FirstOrDefault(r => r != null);

			if( spfRecord == null )
				return Result.None;

			return spfRecord.Evaluate(ip, sender, domain, resolver);
		}

	}
}
