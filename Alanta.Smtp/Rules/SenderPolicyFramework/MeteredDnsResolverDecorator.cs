﻿using System;
using System.Net;
using Alanta.Smtp.Dns;

namespace Alanta.Smtp.Rules.SenderPolicyFramework
{
	public interface IMeteredDnsResolver : IDnsResolver
	{
		bool LimitExceeded { get; }
   }

	public class MeteredDnsResolverDecorator : IMeteredDnsResolver
	{
		private readonly IDnsResolver _resolver;
		private readonly uint _maximumLookups;
		private uint _executedLookups;

		public MeteredDnsResolverDecorator( IDnsResolver resolver, uint maximumLookups )
		{
			if (resolver == null) throw new ArgumentNullException(nameof(resolver));
			_resolver = resolver;
			_maximumLookups = maximumLookups;
			_executedLookups = 0;
		}

		public bool LimitExceeded => _executedLookups >= _maximumLookups;

		public string Resolve(IPAddress ip)
		{
			_executedLookups++;
			return _resolver.Resolve(ip);
		}

		public IPAddress[] Resolve( string domain )
		{
			_executedLookups++;
			return _resolver.Resolve( domain );
		}

		public MxEntry[] GetMxRecords(string domain)
		{
			_executedLookups++;
			return _resolver.GetMxRecords(domain);
		}

		public string[] ReverseLookup(IPAddress ip)
		{
			_executedLookups++;
			return _resolver.ReverseLookup( ip );
		}

		public string[] GetTxtRecords(string domain)
		{
			_executedLookups++;
			return _resolver.GetTxtRecords( domain );
		}
	}
}