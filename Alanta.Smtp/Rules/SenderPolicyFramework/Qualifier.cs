namespace Alanta.Smtp.Rules.SenderPolicyFramework
{
	public enum Qualifier
	{
		pass, // +
		fail, // -
		softfail, // ~
		neutral // ?
	}
}