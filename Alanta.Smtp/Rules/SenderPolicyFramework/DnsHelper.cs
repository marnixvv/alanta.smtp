using System;
using System.Linq;
using System.Net;
using Alanta.Smtp.Dns;

namespace Alanta.Smtp.Rules.SenderPolicyFramework
{
	public class DnsHelper : IDnsHelper
	{
		private readonly IMeteredDnsResolver _resolver;

		public DnsHelper( IMeteredDnsResolver resolver )
		{
			if (resolver == null) throw new ArgumentNullException(nameof(resolver));
			_resolver = resolver;
		}

		public VerificationResult Verify(IPAddress ip, out string validatedDnsName)
		{
			validatedDnsName = null;
			var result = _resolver.ReverseLookup( ip );
			
			foreach ( var name in result )
			{
				if ( _resolver.LimitExceeded )
					return VerificationResult.DnsLookupLimitExceeded;

				var resolvedIp = _resolver.Resolve( name );
				if ( resolvedIp.Any( x => x.Equals( ip ) ) )
				{
					validatedDnsName = name;
					return VerificationResult.Succedeed;
				}
			}

			return VerificationResult.Failed;
		}
	}
}