﻿using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;

namespace Alanta.Smtp.Rules.SenderPolicyFramework
{
	/// <summary>
	/// Handles SPF macro expansion
	/// </summary>
	public class MacroExpander
	{
		private readonly IDnsHelper _dnsHelper;
		private static Regex _macroSplit = new Regex( @"(?:%{(?<macro>[a-z][^}]*)})" );

		public MacroExpander( IDnsHelper dnsHelper )
		{
			if (dnsHelper == null) throw new ArgumentNullException(nameof(dnsHelper));
			_dnsHelper = dnsHelper;
		}

		public string Expand(string input, string sender, IPAddress ip, string domain )
		{
			return _macroSplit.Replace( input, m => ExpandMacro( m.Groups["macro"].Value, sender, ip, domain ) );
		}

		private string ExpandMacro(string macro, string sender, IPAddress ip, string domain)
		{
			string baseValue = GetExpMacroValue( macro[0], sender, ip, domain );
			var postPRocessed = PostProcess(macro, baseValue);
			// trace - unprocessed macro
			return postPRocessed;
		}

		private string GetMacroValue(char macroletter, string sender, IPAddress ip, string domain )
		{
			switch ( macroletter )
			{
				case 's': //  < sender >
					return sender;
				case 'l': // local - part of<sender>
					return sender.Substring(0, sender.IndexOf("@")); // todo: move this into a proper class
				case 'o': // domain of<sender>
					return sender.Substring(sender.IndexOf("@") + 1); // todo: move this into a proper class
				case 'd': // < domain >
					return domain;
				case 'i': // < ip >
					return ip.ToDotNotation();
				case 'p': // the validated domain name of < ip > (do not use)
					string verifiedName;
					var result = _dnsHelper.Verify(ip, out verifiedName);
					return result == VerificationResult.Succedeed ? verifiedName : "";
				case 'v': // the string "in-addr" if < ip > is ipv4, or "ip6" if < ip > is ipv6
					return ip.AddressFamily == AddressFamily.InterNetwork ? "in-addr" : "ip6";
				case 'h': // HELO / EHLO domain
					throw new NotImplementedException();
				default:
					return null;
			}
		}

		private string GetExpMacroValue(char macroletter, string sender, IPAddress ip, string domain)
		{
			switch (macroletter)
			{
				case 'c': // SMTP client IP ( easily readable format)
					return ip.ToString();
				case 'r': // domain name of host performing the check
					throw new NotImplementedException();
				case 't': // current timestamp (unix epoch)
					return ( DateTime.UtcNow - new DateTime( 1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc ) ).TotalSeconds.ToString( CultureInfo.InvariantCulture );
				default:
					return GetMacroValue(macroletter, sender, ip, domain);
			}
		}

		private string PostProcess(string macro, string value)
		{
			if (null == value || macro.Length <= 1) return value;

			bool reverse = macro.Skip(1).Contains('r');
			int take = macro.Where(c => c >= '1' && c <= '9').Select(c => c - '0').FirstOrDefault();

			const string delimiters = ".-+,/_=";
			char delimiter = macro.FirstOrDefault(c => delimiters.Contains(c));

			if (delimiter == 0)
				delimiter = '.';

			if (reverse)
			{
				value = string.Join(".", value.Split(delimiter).Reverse());
				delimiter = '.';
			}

			if (take > 0)
			{
				value = string.Join(".", value.Split(delimiter).Reverse().Take(take).Reverse());
			}

			if (delimiter != '.' && take == 0 && !reverse)
				value = string.Join(".", value.Split(delimiter));
			return value;
		}
	}

	public static class IPAddressHelpers
	{
		public static string ToDotNotation(this IPAddress ip)
		{
			return  ip.AddressFamily == AddressFamily.InterNetworkV6
						? string.Join( ".", ip.GetAddressBytes().Select( b => (b>>4).ToString( "x", CultureInfo.InvariantCulture ) + "." + ( b & 0xf ).ToString( "x", CultureInfo.InvariantCulture ) ) )
						: ip.ToString();
		}
	}
}
