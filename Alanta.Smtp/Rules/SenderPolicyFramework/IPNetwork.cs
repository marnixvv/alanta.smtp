using System;
using System.Net;
using System.Net.Sockets;

namespace Alanta.Smtp.Rules.SenderPolicyFramework
{
	/// <summary>
	/// Really, really simple helper to allow parsing of IPv4 and IPv6 addresses with subnetmasks
	/// </summary>
	public class IPNetwork
	{
		// consider using https://github.com/lduchosal/ipnetwork
		public uint Mask { get; set; }
		public IPAddress Address { get; set; }

		public bool IsIPv4
		{
			get { return Address != null && Address.AddressFamily == AddressFamily.InterNetwork; }
		}

		public bool IsIPv6
		{
			get { return Address != null && Address.AddressFamily == AddressFamily.InterNetworkV6; }
		}

		public static bool TryParse(string address, out IPNetwork network)
		{
			network = null;
			var parts = address.Split('/');

			IPAddress ip = null;
			uint mask = 0xff;

			if (parts.Length < 1)
			{
				return false;
			}

			if (!IPAddress.TryParse(parts[0], out ip) 
			    || !(ip.AddressFamily == AddressFamily.InterNetwork || ip.AddressFamily == AddressFamily.InterNetworkV6) )
			{
				return false;
			}

			if (parts.Length >= 2)
			{
				if (!UInt32.TryParse(parts[1], out mask))
				{
					return false;
				}

				if (mask == 0)
					return false;
				if (mask >= 0x20 && ip.AddressFamily == AddressFamily.InterNetwork)
					return false; // invalid IPv4 mask must be 1 through 31
				if (mask >= 0x80 && ip.AddressFamily == AddressFamily.InterNetworkV6)
					return false; // invalid IPv4 mask must be 1 through 31
			}
			network = new IPNetwork
			{
				Address = ip,
				Mask = mask
			};
			return true;
		}
	}
}