using System;

namespace Alanta.Smtp.Rules.SenderPolicyFramework
{
	public class Mechanism : ITerm
	{
		private readonly Func<bool> _match;

		public Mechanism( Qualifier qualifier, Func<bool> match, string name )
		{
			Name = name;
			_match = match;
			Qualifier = qualifier;
		}

		public Result? Evaluate()
		{
			var match = _match();
			// todo: handle qualifier
			if ( !match )
				return null;

			switch ( Qualifier )
			{
				case Qualifier.pass:
					return Result.Pass;
				case Qualifier.softfail:
					return Result.Softfail;
				case Qualifier.fail:
					return Result.Fail;
				case Qualifier.neutral:
					return Result.Neutral;
				default:
					throw new InvalidOperationException( "Unknown qualifier: " + Qualifier );
			}
		}

		public Qualifier Qualifier { get; }

		public string Name { get; }

		public static Mechanism All( Qualifier qualifier )
		{
			return new Mechanism( qualifier, () => true, "all" );
		}

		internal static ITerm Include( Qualifier qualifier, string value )
		{
			return new Mechanism( qualifier, () => { throw new NotImplementedException(); }, "include" );
		}

		public static Mechanism A( Qualifier qualifier, string value )
		{
			return new Mechanism( qualifier, () => { throw new NotImplementedException(); }, "a" );
		}

		public static Mechanism Mx( Qualifier qualifier, string value )
		{
			return new Mechanism( qualifier, () => { throw new NotImplementedException(); }, "mx" );
		}

		public static Mechanism Ptr( Qualifier qualifier, string value )
		{
			return new Mechanism( qualifier, () => { throw new NotImplementedException(); }, "ptr" );
		}

		public static Mechanism Exists( Qualifier qualifier, string value )
		{
			return new Mechanism( qualifier, () => { throw new NotImplementedException(); }, "exists" );
		}

		public static Mechanism Ip4( Qualifier qualifier, IPNetwork value )
		{
			return new Mechanism( qualifier, () => { throw new NotImplementedException(); }, "ip4" );
		}

		public static Mechanism Ip6( Qualifier qualifier, IPNetwork value )
		{
			return new Mechanism( qualifier, () => { throw new NotImplementedException(); }, "ip6" );
		}
	}
}