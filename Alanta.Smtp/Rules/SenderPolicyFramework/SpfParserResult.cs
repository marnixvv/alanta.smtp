﻿namespace Alanta.Smtp.Rules.SenderPolicyFramework
{
	public enum SpfParserResult
	{
		NotAnSpfRecord,
		InvalidSpfRecord,
		Success
	}
}