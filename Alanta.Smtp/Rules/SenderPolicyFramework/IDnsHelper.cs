﻿using System.Net;

namespace Alanta.Smtp.Rules.SenderPolicyFramework
{
	public interface IDnsHelper
	{
		VerificationResult Verify(IPAddress ip, out string validatedDnsName);
	}

	public enum VerificationResult
	{
		Failed = 0,
		Succedeed = 1,
		DnsLookupLimitExceeded = 2
	}
}