﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Alanta.Smtp.Dns;

namespace Alanta.Smtp.Rules.SenderPolicyFramework
{
	public class SpfRecord : IEnumerable<ITerm>
	{
		private ITerm[] _terms;

		/// <summary>
		/// </summary>
		/// <param name="ip">the IP address of the SMTP client that is emitting the mail, either IPv4 or IPv6.</param>
		/// <param name="sender">the "MAIL FROM" or "HELO" identity.</param>
		/// <param name="domain"> the domain that provides the sought-after authorization information; initially, the domain portion of the "MAIL FROM" or "HELO" identity.</param>
		/// <returns></returns>
		public Result Evaluate(IPAddress ip, string sender, string domain, IMeteredDnsResolver resolver)
		{
			// RFC 7208 section 6
			var hasDuplicateModifiers = _terms.OfType<Modifier>().GroupBy(t => t.Name).Any(g => g.Count() > 1);
			if( hasDuplicateModifiers )
				return Result.Permerror;

			foreach ( var term in _terms )
			{
				var result = term.Evaluate();
				if ( result.HasValue )
					return result.Value;
			}
			return Result.Neutral; // default result per RFC 7208 section 4.7
		}

		public SpfRecord(ITerm[] terms)
		{
			if (terms == null) throw new ArgumentNullException(nameof(terms));
			_terms = terms;
		}

		public IEnumerator<ITerm> GetEnumerator()
		{
			return ((IEnumerable<ITerm>)_terms).GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return _terms.GetEnumerator();
		}
	}
}