﻿namespace Alanta.Smtp.Rules.SenderPolicyFramework
{
	public interface ITerm
	{
		Result? Evaluate();
	}
}