﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Alanta.Smtp.Rules.SenderPolicyFramework
{
   public static class SpfRecordParser
   {
      /// <summary>
      /// Parse a string that may represent an SPF record.
      /// </summary>
      /// <param name="spfRecord"></param>
      /// <returns></returns>
      public static SpfRecord Parse(string spfRecord)
      {
         if (spfRecord == null)
            return null;

         var parts = spfRecord.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
         // rfc-7208 section 4.6 - SPF record must start with version
         if (parts.Length < 1 || !string.Equals(parts[0], "v=spf1"))
            return null;

			// regex match the parts
			var terms = new List<ITerm>();
			var mechanismRegex = new Regex( @"(?<qualifier>[\+\-\?\~])?(?<term>[^:\s]+)(:(?<value>.*))?" );
			var modifierRegex = new Regex( @"(?<term>[^=\s]+)=(?<value>.*)" );
			foreach ( var part in parts.Skip(1))
			{
				var mechanism = mechanismRegex.Match( part );
				var modifier = modifierRegex.Match(part);

				if ( !mechanism.Success && !modifier.Success )
					throw new InvalidOperationException( "Syntax error in SPF record: "+part ?? "" );

				if (mechanism.Success)
				{
					var term = GetMechanism(mechanism.Groups["qualifier"].Value, mechanism.Groups["term"].Value, mechanism.Groups["value"].Value);
					if( term != null )
						terms.Add( term );
				}

				if ( modifier.Success )
				{
					var term = GetModifier( modifier, modifier.Groups["term"].Value, modifier.Groups["value"].Value);
					if ( term != null )
						terms.Add( term );
				}
			}

			return new SpfRecord( terms.ToArray() );
      }

	   private static ITerm GetMechanism(string qualifierText, string name, string value)
	   {
		   var qualifier = ParseQualifier( qualifierText );

		   switch (name)
		   {
			   case "all":
				   return Mechanism.All(qualifier);
			   case "a":
				   return Mechanism.A(qualifier, value);
			   case "include":
				   return Mechanism.Include(qualifier, value);
			   case "mx":
				   return Mechanism.Mx(qualifier, value);
			   case "ip4":
				   IPNetwork ip;
				   if (IPNetwork.TryParse(value, out ip) && ip.IsIPv4)
				   {
					   return Mechanism.Ip4(qualifier, ip);
				   }
				   break;
			   case "ip6":
				   IPNetwork ip6;
				   if (IPNetwork.TryParse(value, out ip6) && ip6.IsIPv6)
				   {
					   return Mechanism.Ip6(qualifier, ip6);
				   }
				   break;
			   case "ptr":
				   return Mechanism.Ptr(qualifier, value);
				default:
					// ignore & trace
				   break;
		   }

		   return null;
	   }

	   private static ITerm GetModifier(Match modifier, string name, string value)
		{
		   switch (name)
		   {
			   case "exp":
				   return Modifier.Exp(value);
			   case "redirect":
				   return Modifier.Redirect(value);
				default:
					// ignore & trace
				   break;
		   }
			return null;
		}


		private static Qualifier ParseQualifier(string raw)
		{
			switch ( raw )
			{
				case "+":
					return Qualifier.pass;
				case "-":
					return Qualifier.fail;
				case "~":
					return Qualifier.softfail;
				default:
					return Qualifier.neutral;
			}
		}
   }
}
