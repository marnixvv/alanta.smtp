using System;

namespace Alanta.Smtp.Rules.SenderPolicyFramework
{
	public class Modifier : ITerm
	{
		public Modifier( string name, string value )
		{
			if (string.IsNullOrWhiteSpace(name)) throw new ArgumentException("Name should not be null, emptyor whitespace", nameof(name));
			Name = name;
		}

		public string Name { get; private set; }

		public Result? Evaluate()
		{
			throw new NotImplementedException();
		}
 
		public static Modifier Exp(string value)
		{
			return new Modifier("exp", value);
		}

		public static Modifier Redirect( string value )
		{
			return new Modifier( "redirect", value );
		}
	}
	
}