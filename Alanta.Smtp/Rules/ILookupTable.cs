using System.Collections.Generic;

namespace Alanta.Smtp.Rules
{
   /// <summary>
   /// An ordered set of lookup rules.
   /// </summary>
   /// <remarks>
   /// This is (loosly) based on Postfix lookup tables. 
   /// See http://www.postfix.org/DATABASE_README.html.
   /// </remarks>
   public interface ILookupTable
   {
      /// <summary>
      /// Try to find a match for the specified keys.
      /// </summary>
      /// <param name="key">The keys to match.</param>
      /// <param name="value">The value associated with the keys, or <c>null</c> if not found or the value for a rule was <c>null</c>.</param>
      /// <returns><c>true</c> if a matching rule was found.</returns>
      bool TryLookup(string key, out string value);

      /// <summary>
      /// Returns true if all keys match with at least one rule in the set.
      /// </summary>
      /// <param name="keys">The keys to match.</param>
      /// <returns><c>true</c> if all keys in the set match at least one rule, or if keys is null or empty.</returns>
      bool MatchAll( IReadOnlyCollection<string> keys);
   }
}