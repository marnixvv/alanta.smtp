using System;
using System.Collections;
using System.Collections.Generic;

namespace Alanta.Smtp.Rules
{
   /// <summary>
   /// An ordered set of access rules.
   /// </summary>
   public class RuleSet : IEnumerable<IAccessRule>
   {
      private readonly RuleResult _defaultResult;
      private readonly List<IAccessRule> _rules = new List<IAccessRule>();

      /// <summary>
      /// Creates a new rule set.
      /// </summary>
      /// <param name="defaultResult">If none of the rules match, this result is returned.</param>
      public RuleSet( RuleResult defaultResult )
      {
         _defaultResult = defaultResult;
      }

      public void Add(IAccessRule rule)
      {
         _rules.Add(rule);
      }

      public void RemoveAll( Predicate<IAccessRule> rulePredicate  )
      {
         _rules.RemoveAll(rulePredicate); 
      }

      public RuleResult Evaluate(IRuleContext ruleContext)
      {
         foreach (var rule in _rules)
         {
            var result = rule.Evaluate(ruleContext);
            if (result.HasValue) return result.Value;
         }

         return _defaultResult;
      }

      IEnumerator<IAccessRule> IEnumerable<IAccessRule>.GetEnumerator()
      {
         return _rules.GetEnumerator();
      }

      public IEnumerator GetEnumerator()
      {
         return _rules.GetEnumerator();
      }

      public void Insert(int index, IAccessRule rule)
      {
         _rules.Insert(index, rule);
      }
   }
}