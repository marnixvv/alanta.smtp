﻿using System.Collections;
using System.Net;

namespace Alanta.Smtp.Dns
{
   public interface IDnsResolver
   {
      string Resolve( IPAddress ip );
		IPAddress[] Resolve( string domain );
		MxEntry[] GetMxRecords(string domain);
	   string[] ReverseLookup(IPAddress ip);
		string[] GetTxtRecords(string domain);
   }
}
