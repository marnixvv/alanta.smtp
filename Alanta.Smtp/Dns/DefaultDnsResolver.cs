﻿using System.Linq;
using System.Net;
using Alanta.DNS;

namespace Alanta.Smtp.Dns
{
	public class DefaultDnsResolver : IDnsResolver
	{
	    private DnsResolver _resolver;

	    public DefaultDnsResolver(string[] serverIps) : this( serverIps.Select( ToDnsServer ).ToArray() )
	    {
	        
	    }

	    public static IPEndPoint ToDnsServer(string ip)
	    {
	        return new IPEndPoint(IPAddress.Parse(ip), 53);
	    }

	    public DefaultDnsResolver(IPEndPoint[] servers)
	    {
            if (servers == null) 
                throw new System.ArgumentNullException(nameof(servers));
            
            if (servers.Length == 0)
                throw new System.ArgumentException("Specify at least 1 DNS server", nameof(servers));

            _resolver = new DnsResolver(new UdpDnsQueryResolver(servers));

	    }
		public string Resolve(IPAddress ip)
		{
			var entry = System.Net.Dns.GetHostEntryAsync(ip).Result;
			return entry?.HostName;
		}

		public IPAddress[] Resolve(string domain)
		{
			return System.Net.Dns.GetHostEntryAsync(domain).Result?.AddressList ?? new IPAddress[0];
		}

		public MxEntry[] GetMxRecords(string domain)
		{
		    var response = _resolver.Query(domain, QType.MX);

		    if (!response.Success)
		    {
		        return new MxEntry[0];
		    }

		    return response.Answers.Select(a => new MxEntry(a.NAME, (a.RECORD as RecordMX).PREFERENCE)).ToArray();

		}

		public string[] ReverseLookup(IPAddress ip)
		{
			throw new System.NotImplementedException();
		}

		public string[] GetTxtRecords(string domain)
		{
            var response = _resolver.Query(domain, QType.TXT);

            if (!response.Success || response.Answers.Count == 0)
            {
                return new string[0];
            }

            return response.Answers.Select(a => (a.RECORD as RecordTXT).TXT).ToArray();
        }
	}
}