﻿using System.Linq;

namespace Alanta.Smtp.Dns
{
	public static class DomainName
	{
		public static bool IsProperlyFormed( string domainName )
		{
			if (string.IsNullOrWhiteSpace(domainName))
				return false;

			var split = domainName.Split('.');
			if ( split.Length == 1 || split.Any(p => p.Length > 63 || p.Length == 0))
				return false;

			return true;
		}
	}
}
