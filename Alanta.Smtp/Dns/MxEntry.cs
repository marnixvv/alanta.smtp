﻿using System;

namespace Alanta.Smtp.Dns
{
   /// <summary>
   /// A DNS entry for a mail server (MX)
   /// </summary>
   public class MxEntry
   {
      public MxEntry( string name, UInt16 priority )
      {
         Name = name;
         Priority = priority;
      }

      /// <summary>
      /// The DNS name of the mail server
      /// </summary>
      public string Name { get; private set; }

      /// <summary>
      /// The priority of the mail server (lower number is higher priority)
      /// </summary>
      public uint Priority { get; private set; }
   }
}