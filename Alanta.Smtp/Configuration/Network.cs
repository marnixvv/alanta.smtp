﻿using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace Alanta.Smtp.Configuration
{
   public static class Network
   {
      private static NetworkInterfaceType[] defaultNetworkTypes = new NetworkInterfaceType[]
      {
         NetworkInterfaceType.Ethernet,
         NetworkInterfaceType.Ethernet3Megabit,
         NetworkInterfaceType.FastEthernetFx,
         NetworkInterfaceType.FastEthernetT,
         NetworkInterfaceType.GigabitEthernet,
         NetworkInterfaceType.Wireless80211,
         NetworkInterfaceType.Loopback, 

      };

      public static ActiveInterface[] FindActiveInterfaces()
      {
          

         return NetworkInterface.GetAllNetworkInterfaces()
            .Where( a => a.OperationalStatus == OperationalStatus.Up  && defaultNetworkTypes.Contains( a.NetworkInterfaceType) )
            .Select( a => new ActiveInterface
            {
               @Interace = a,
               IsLoopBack = a.NetworkInterfaceType == NetworkInterfaceType.Loopback,
               PublicAddresses = a.GetIPProperties().UnicastAddresses
                  .Where( p => p.Address.AddressFamily == AddressFamily.InterNetwork || p.Address.AddressFamily == AddressFamily.InterNetworkV6 )
                  .Select( ip => ip.Address ).ToArray(),
               DnsServers = a.GetIPProperties().DnsAddresses.ToArray()
            } )
            .ToArray();
      }

      public static IPAddress[] GetDnsServersForIp( IPAddress address )
      {
         var @interface = FindActiveInterfaces().FirstOrDefault( i => i.PublicAddresses.Any( ip => ip.Equals( address ) ) );
         return @interface != null ? @interface.DnsServers : new IPAddress[0];
      }

   }

   public class ActiveInterface
   {
      public bool IsLoopBack { get; set; }
      public NetworkInterface @Interace { get; set; }
      public IPAddress[] PublicAddresses { get; set; }
      public IPAddress[] DnsServers { get; set; }
   }
}
