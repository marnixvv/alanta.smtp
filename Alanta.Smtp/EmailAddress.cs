﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alanta.Smtp
{
	public static class EmailAddress
	{
		public static string LocalPart(string emailAddress)
		{
			if (string.IsNullOrWhiteSpace( emailAddress ) )
				return "";

			var atIndex = emailAddress.IndexOf("@");

			return atIndex <= 0 ? "" : emailAddress.Substring(0, atIndex);
		}


		public static string Domain( string emailAddress )
		{
			if ( string.IsNullOrWhiteSpace( emailAddress ) )
				return "";

			var atIndex = emailAddress.IndexOf( "@", StringComparison.OrdinalIgnoreCase );

			if (atIndex == emailAddress.Length - 1)
				return "";

			return atIndex < 0 ? emailAddress : emailAddress.Substring( atIndex+1 );
		}

		public static bool HasLocalPart(string sender)
		{
			return !string.IsNullOrEmpty(sender) && sender.Contains("@");
		}

		public static string SetLocalPart(string sender, string localPart)
		{
			return localPart + "@" + Domain(sender);
		}
	}
}
