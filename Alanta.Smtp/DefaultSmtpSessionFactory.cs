﻿using System;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using Alanta.Sasl;
using Alanta.Smtp.ContentProcessing;
using Alanta.Smtp.Delivery;
using Alanta.Smtp.Dns;
using Alanta.Smtp.Logging;
using Alanta.Smtp.Routing;

namespace Alanta.Smtp
{
   public class DefaultSmtpSessionFactory : ISmtpSessionFactory
   {
        private readonly IDnsResolver _dnsResolver;
	   private readonly Func<SmtpSessionContext,ILogger> _loggingFactory;
        public MessageRouteTable Routes { get; } = new MessageRouteTable();

	   public DefaultSmtpSessionFactory()
         : this( new DefaultDnsResolver(DNS.DnsUtilities.DefaultDnsServers), ctx => new ConsoleLogger() )
      {
      }

      public DefaultSmtpSessionFactory(IDnsResolver dnsResolver, Func<SmtpSessionContext,ILogger> loggingFactory )
      {
          _dnsResolver = dnsResolver ?? throw new ArgumentNullException(nameof(dnsResolver));
	      _loggingFactory = loggingFactory ?? throw new ArgumentNullException(nameof(loggingFactory));
      }

        public void SetupConsoleDelivery()
        {
            Routes.Add(new DeliveryAgentRoute(new ConsoleDeliveryAgent(new TraceInformation(_dnsResolver))));
        }

      public SmtpSession CreateSession( IServerConnection connection, IPAddress localAddress, IPAddress remoteAddress )
      {
         var pipeline = SmtpPipelineBuilder.CreatePipeline();
         if (Certificate != null)
         {
            pipeline.EnableTls();
         }
         if (AuthenticationService != null)
         {
            pipeline.EnableAuthentication(AuthenticationService);
         }
         var context = new SmtpSessionContext( connection, pipeline )
            {
					Id = Guid.NewGuid(),
                HostName = _dnsResolver.Resolve(localAddress),
               ServerCertificate = Certificate,
               RemoteIP = remoteAddress,
               LocalIP = localAddress
            };
	      var logger = _loggingFactory(context);

			return new SmtpSession( context, new DefaultMessageProcessor( Routes, logger), logger );
      }

      public X509Certificate Certificate { get; set; }
      public IAuthenticationService AuthenticationService { get; set; }
   }
}