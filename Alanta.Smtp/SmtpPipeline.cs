﻿using System.Collections.Generic;
using System.Linq;
using Alanta.Smtp.Commands;
using Alanta.Smtp.Rules;

namespace Alanta.Smtp
{
   public class SmtpPipeline : ISmtpPipeline
   {
      private readonly List<ISmtpCommand> _pipelineCommands = new List<ISmtpCommand>();
      private readonly List<IEhloExtension> _ehloExtensions = new List<IEhloExtension>();
      private readonly RuleSet _relayRestrictions =  new RuleSet( RuleResult.Rejected );
      private ILookupTable _relayAccess = new LookupTable();
      private readonly RuleSet _clientRestrictions =  new RuleSet( RuleResult.Accepted );
      private readonly RuleSet _heloRestrictions = new RuleSet( RuleResult.Accepted );
      private readonly RuleSet _senderRestrictions = new RuleSet( RuleResult.Accepted );
      private readonly RuleSet _recipientRestrictions = new RuleSet( RuleResult.Accepted );

      public SmtpPipeline( IEnumerable<ISmtpCommand> commands)
      {
         if ( null != commands )
         {
            foreach (var smtpCommand in commands)
            {
               RegisterCommand( smtpCommand );
            }
         }
      }

      public bool TryInvokePipeline( ISmtpSessionContext context, string line )
      {
         foreach ( var smtpCommand in _pipelineCommands )
         {
            if ( smtpCommand.TryMatchCommand( line ) )
            {
               if ( !smtpCommand.TryProcessCommand( context, line ) )
               {
                  context.Respond( "503 bad sequence of commands" );
               }
               return true;
            }
         }

         return false;
      }

      public void RegisterCommand( ISmtpCommand command )
      {
         _pipelineCommands.Add( command );

         if( command is IEhloExtension && !_ehloExtensions.Contains( command as IEhloExtension ))
         {
            _ehloExtensions.Add( command as IEhloExtension );
         }
      }

      public RuleSet ClientRestrictions { get { return _clientRestrictions; } }

      public RuleSet HeloRestrictions { get { return _heloRestrictions; } }

      public RuleSet SenderRestrictions { get { return _senderRestrictions; } }

      public RuleSet RelayRestrictions { get { return _relayRestrictions; } }

      public RuleSet RecipientRestrictions { get { return _recipientRestrictions; } }

      public ILookupTable RelayAccess { get { return _relayAccess; } }

      public IRuleContext CreateRuleContext(ISmtpSessionContext smtpSessionContext, string recipient)
      {
         return new RuleContext( smtpSessionContext, RelayAccess, recipient );
      }

      public IEnumerable<string> GetEhloContent( ISmtpSessionContext context )
      {
         return _ehloExtensions.Select( t => t.GetEhloContent( context ) ).Where( s => !string.IsNullOrEmpty( s ) );
      }
   }
}
