using System;
using System.Collections.Generic;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Alanta.Smtp
{
   /// <summary>
   /// The context for an incoming SMTP connection.
   /// </summary>
   public interface ISmtpSessionContext : IDisposable
   {
      string Receive();
      void Respond(string response, SmtpSession.States? nextState = new SmtpSession.States?());
      bool IsConnected { get; }
      bool IsEncrypted { get; }

      bool IsAuthenticated { get; set; }
      string AuthorizedUser { get; set; }

      SmtpSession.States State { get; }
      void SetState(SmtpSession.States value);

      uint? MaximumMessageSize { get; set; }
      string HostName { get; }

      IEnumerable<string> GetEhloContent();

      MessageTransaction CurrentTransaction { get; }
      X509Certificate ServerCertificate { get; }
      string ProtocolName { get; set; }
      string ProvidedRemoteName { get; set; }
      IPAddress LocalIP { get; }
      IPAddress RemoteIP { get; }
      ISmtpPipeline Pipeline { get; }

      MessageTransaction BeginTransaction();
      void AbortTransaction();

      Task<bool> StartTls();
      void Reset();
      bool PerformRestrictionChecks(string recipient);
   }
}