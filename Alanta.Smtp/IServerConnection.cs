using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Alanta.Smtp
{
   public interface IServerConnection : INetworkConnection
   {
        /// <summary>
        /// Start TLS for the server end of a connection.
        /// </summary>
        /// <param name="serverCertificate">The server certificate.</param>
        /// <param name="allowedTlsProtocols">The allowed TLS protocols. Defaults to TLS 1.2 since older versions are considered insecure.</param>
        /// <returns><c>true</c> if TLS was started succesfully.</returns>
        Task<bool> StartTls(X509Certificate serverCertificate, SslProtocols allowedTlsProcols = 0);
   }
}