using System;

namespace Alanta.Smtp.Logging
{
   public interface ILogger
   {
      void Debug( string format, params object[] parameters );
      void Info( string format, params object[] parameters);
      void Warn( string format, params object[] parameters );
      void Error( string message, Exception exception );
      void Error( string format, params object[] parameters );
   }
}