using System;
using System.Diagnostics;

namespace Alanta.Smtp.Logging
{
   public class TraceLogger : ILogger
   {
      private readonly TraceSource _traceSource;

      public TraceLogger( TraceSource traceSource )
      {
         if ( traceSource == null ) throw new ArgumentNullException( nameof(traceSource) );
         _traceSource = traceSource;
      }

      public void Debug( string format, params object[] parameters )
      {
         _traceSource.TraceEvent( TraceEventType.Verbose, 0, format, parameters );
      }

      public void Info( string format, params object[] parameters)
      {
         _traceSource.TraceEvent( TraceEventType.Information, 0, format, parameters );
      }

      public void Warn( string format, params object[] parameters )
      {
         _traceSource.TraceEvent( TraceEventType.Warning, 0, format, parameters );
      }

      public void Error( string format, params object[] parameters )
      {
         _traceSource.TraceEvent( TraceEventType.Error, 0, format, parameters );
      }

      public void Error( string message, Exception exception )
      {
         _traceSource.TraceData( TraceEventType.Error, 0, message, exception );
      }
   }
}
