using System;

namespace Alanta.Smtp.Logging
{
   public class PrefixLoggerDecorator : ILogger
   {
      private readonly ILogger _inner;
      private readonly string _prefix;

      public PrefixLoggerDecorator( ILogger inner, string prefix )
      {
         _inner = inner;
         _prefix = prefix;
      }

      public void Debug(string format, params object[] parameters)
      {
         _inner.Debug(_prefix+" "+format, parameters);
      }

      public void Info(string format, params object[] parameters)
      {
         _inner.Info(_prefix + " " + format, parameters);
      }

      public void Warn(string format, params object[] parameters)
      {
         _inner.Warn(_prefix + " " + format, parameters);
      }

      public void Error(string message, Exception exception)
      {
         _inner.Error(_prefix + " " + message, exception);
      }

      public void Error(string format, params object[] parameters)
      {
         _inner.Error(_prefix + " " + format, parameters);
      }
   }
}