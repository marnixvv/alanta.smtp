using System;

namespace Alanta.Smtp.Logging
{
   public class ConsoleLogger : ILogger
   {
      public void Debug( string format, params object[] parameters )
      {
        Write( format, parameters );
      }

      public void Info( string format, params object[] parameters )
      {
         Write( format, parameters );
      }

      public void Warn( string format, params object[] parameters )
      {
         Write( "WARN: " + format, parameters );
      }

      public void Error( string message, Exception exception )
      {
         Write( "ERROR:" + message );
         Console.WriteLine(exception.ToString());
      }

      public void Error( string format, params object[] parameters )
      {
         Write( "ERROR: " + format, parameters );
      }

      private void Write(string format, params object[] parameters)
      {
         Console.WriteLine( "[" + Environment.CurrentManagedThreadId.ToString() + "] " + format, parameters );
      }
   }
}