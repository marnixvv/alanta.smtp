﻿using System;
using System.Globalization;

namespace Alanta.Smtp
{
   public class SmtpStatusMessage
   {
      public SmtpStatusMessage( int statusCode, string message )
      {
         if (string.IsNullOrWhiteSpace(message)) throw new ArgumentException( "Message should not be null or empty", nameof(message));
         if( statusCode < 200 || statusCode > 600 ) throw new ArgumentOutOfRangeException(nameof(statusCode), statusCode, "Status code must be between 200 and 600");
         StatusCode = statusCode;
         Message = message;
      }

      public int StatusCode { get; }
      public string Message { get; }

      public override string ToString()
      {
         return string.Format( CultureInfo.InvariantCulture, "{0} {1}", StatusCode, Message);
      }
   }

   public class SmtpExtendedStatusMessage : SmtpStatusMessage
   {
      public SmtpExtendedStatusMessage(int statusCode, string message, string extendedStatusCode) : base(statusCode, message)
      {
         ExtendedStatusCode = extendedStatusCode;
      }

      public string ExtendedStatusCode { get; set; }

      public override string ToString()
      {
         if ( string.IsNullOrWhiteSpace( ExtendedStatusCode ) )
         {
            return base.ToString();
         }
         return string.Format( CultureInfo.InvariantCulture, "{0} {1} {2}", StatusCode, ExtendedStatusCode, Message );
      }
   }
}
