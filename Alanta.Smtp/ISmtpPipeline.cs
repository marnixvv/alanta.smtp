using System;
using System.Collections.Generic;
using Alanta.Smtp.Commands;
using Alanta.Smtp.Rules;

namespace Alanta.Smtp
{
   public interface ISmtpPipeline
   {
      bool TryInvokePipeline( ISmtpSessionContext context, string line );
      IEnumerable<string> GetEhloContent( ISmtpSessionContext context );
      void RegisterCommand( ISmtpCommand command );
      RuleSet ClientRestrictions { get; }
      RuleSet HeloRestrictions { get; }
      RuleSet SenderRestrictions { get; }
      RuleSet RelayRestrictions { get; }
      RuleSet RecipientRestrictions { get; }
      
      ILookupTable RelayAccess { get; }
      IRuleContext CreateRuleContext(ISmtpSessionContext smtpSessionContext, string recipient);
   }
}