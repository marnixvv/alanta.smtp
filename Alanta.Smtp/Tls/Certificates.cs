using System;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace Alanta.Smtp.Tls
{
   /* Based on "Certificates to DB and Back" by Mario Majcica (@mmajcica) 
    * 
    * Article : http://www.codeproject.com/Articles/162194/Certificates-to-DB-and-Back
    * 
    * Licensed under The Code Project Open License (CPOL)
    */

   public static class Certificates
   {
      public static X509Certificate2 LoadFromPem( string pem )
      {
         if ( string.IsNullOrWhiteSpace(pem) ) 
            throw new ArgumentNullException( nameof(pem) );

         byte[] certBuffer = GetBytesFromPEM( pem, "CERTIFICATE" );

         if( null == certBuffer )
         {
            throw new InvalidOperationException("Certificate not found in PEM data.");
         }

         var certificate = new X509Certificate2( certBuffer );

         byte[] keyBuffer = GetBytesFromPEM( pem, "RSA PRIVATE KEY" );
         if ( null != keyBuffer )
         {
#if NETCORE
				throw new NotSupportedException("Need to fix this...");
#else
				certificate.PrivateKey = DecodeRsaPrivateKey( keyBuffer );
#endif
         }

         return certificate;
      }

      /// <summary>
      /// This helper function parses an RSA private key using the ASN.1 format
      /// </summary>
      /// <param name="privateKeyBytes">Byte array containing PEM string of private key.</param>
      /// <returns>An instance of <see cref="RSACryptoServiceProvider"/> representing the requested private key.
      /// Null if method fails on retriving the key.</returns>
      private static RSACryptoServiceProvider DecodeRsaPrivateKey( byte[] privateKeyBytes )
      {
         var ms = new MemoryStream( privateKeyBytes );
         var rd = new BinaryReader( ms );

         try
         {
            var shortValue = rd.ReadUInt16();

            switch ( shortValue )
            {
               case 0x8130:
                  // If true, data is little endian since the proper logical seq is 0x30 0x81
                  rd.ReadByte(); //advance 1 byte
                  break;
               case 0x8230:
                  rd.ReadInt16();  //advance 2 bytes
                  break;
               default:
                  throw new FormatException("Improper ASN.1 format while decoding key.");
            }

            shortValue = rd.ReadUInt16();
            if ( shortValue != 0x0102 ) // (version number)
            {
               throw new FormatException( "Improper ASN.1 format, unexpected version number." );
            }

            var byteValue = rd.ReadByte();
            if ( byteValue != 0x00 )
            {
               throw new FormatException( "Improper ASN.1 format while decoding key." );
            }

            // The data following the version will be the ASN.1 data itself, which in our case
            // are a sequence of integers.

            // In order to solve a problem with instancing RSACryptoServiceProvider
            // via default constructor on .net 4.0 this is a hack
            var parms = new CspParameters
                           {
                              Flags = CspProviderFlags.NoFlags,
                              KeyContainerName = Guid.NewGuid().ToString().ToUpperInvariant(),
                              ProviderType = 0x18
                           };

            var rsa = new RSACryptoServiceProvider( parms );
            var rsAparams = new RSAParameters
                               {
                                  Modulus = rd.ReadBytes( DecodeIntegerSize( rd ) )
                               };

            // Argh, this is a pain.  From emperical testing it appears to be that RSAParameters doesn't like byte buffers that
            // have their leading zeros removed.  The RFC doesn't address this area that I can see, so it's hard to say that this
            // is a bug, but it sure would be helpful if it allowed that. So, there's some extra code here that knows what the
            // sizes of the various components are supposed to be.  Using these sizes we can ensure the buffer sizes are exactly
            // what the RSAParameters expect.  Thanks, Microsoft.
            var traits = new RSAParameterTraits( rsAparams.Modulus.Length * 8 );

            rsAparams.Modulus = AlignBytes( rsAparams.Modulus, traits.size_Mod );
            rsAparams.Exponent = AlignBytes( rd.ReadBytes( DecodeIntegerSize( rd ) ), traits.size_Exp );
            rsAparams.D = AlignBytes( rd.ReadBytes( DecodeIntegerSize( rd ) ), traits.size_D );
            rsAparams.P = AlignBytes( rd.ReadBytes( DecodeIntegerSize( rd ) ), traits.size_P );
            rsAparams.Q = AlignBytes( rd.ReadBytes( DecodeIntegerSize( rd ) ), traits.size_Q );
            rsAparams.DP = AlignBytes( rd.ReadBytes( DecodeIntegerSize( rd ) ), traits.size_DP );
            rsAparams.DQ = AlignBytes( rd.ReadBytes( DecodeIntegerSize( rd ) ), traits.size_DQ );
            rsAparams.InverseQ = AlignBytes( rd.ReadBytes( DecodeIntegerSize( rd ) ), traits.size_InvQ );

            rsa.ImportParameters( rsAparams );
            return rsa;
         }
         catch ( FormatException )
         {
            throw;
         }
         catch( Exception ex )
         {
            throw new FormatException( "Unable to decode private key.", ex );   
         }
         finally
         {
            rd.Dispose();
         }
      }

      /// <summary>
      /// This helper function parses an integer size from the reader using the ASN.1 format
      /// </summary>
      /// <param name="rd"></param>
      /// <returns></returns>
      private static int DecodeIntegerSize( BinaryReader rd )
      {
         int count;

         var byteValue = rd.ReadByte();
         if ( byteValue != 0x02 )        // indicates an ASN.1 integer value follows
            return 0;

         byteValue = rd.ReadByte();
         if ( byteValue == 0x81 )
         {
            count = rd.ReadByte();    // data size is the following byte
         }
         else if ( byteValue == 0x82 )
         {
            byte hi = rd.ReadByte();  // data size in next 2 bytes
            byte lo = rd.ReadByte();
            count = BitConverter.ToUInt16( new[] { lo, hi }, 0 );
         }
         else
         {
            count = byteValue;        // we already have the data size
         }

         //remove high order zeros in data
         while ( rd.ReadByte() == 0x00 )
         {
            count -= 1;
         }
         rd.BaseStream.Seek( -1, SeekOrigin.Current );

         return count;
      }

      /// <summary>
      /// Extracts bytes from PEM encoded file.
      /// </summary>
      /// <param name="pemString">The pem string.</param>
      /// <param name="section">The type.</param>
      /// <returns>The bytes in the specified section or <c>null</c> if the section is not found.</returns>
      private static byte[] GetBytesFromPEM( string pemString, string section )
      {
         var header = String.Format("-----BEGIN {0}-----", section);
         var footer = String.Format("-----END {0}-----", section);

         var start = pemString.IndexOf(header, StringComparison.Ordinal) + header.Length;
         var end = pemString.IndexOf(footer, start, StringComparison.Ordinal) - start;

         if( start < 0 || end < 0 )
         {
            return null;
         }

         return Convert.FromBase64String( pemString.Substring( start, end ) );
      }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="inputBytes"></param>
      /// <param name="alignSize"></param>
      /// <returns></returns>
      private static byte[] AlignBytes( byte[] inputBytes, int alignSize )
      {
         var inputBytesSize = inputBytes.Length;

         if ( ( alignSize != -1 ) && ( inputBytesSize < alignSize ) )
         {
            var buf = new byte[alignSize];
            for ( int i = 0; i < inputBytesSize; ++i )
            {
               buf[i + ( alignSize - inputBytesSize )] = inputBytes[i];
            }
            return buf;
         }
         
         return inputBytes;      // Already aligned, or doesn't need alignment
      }
   }
}