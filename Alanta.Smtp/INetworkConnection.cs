using System;
using Alanta.Smtp.Delivery;

namespace Alanta.Smtp
{
   /// <summary>
   /// Abstraction for network connection. This enables protocol level testing and
   /// prevents tight coupling to networking code.
   /// </summary>
   public interface INetworkConnection : IDisposable
   {
      string ReadLine();
      void Send( string data );
      bool Connected { get; }
      void Disconnect();
   }
}