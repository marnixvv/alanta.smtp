﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Alanta.Smtp.Logging;

namespace Alanta.Smtp
{
   /// <summary>
   /// Simple SMTP server (not suited for production use, not yet anyway)
   /// </summary>
   public class SmtpServer
   {
      private readonly ISmtpSessionFactory _sessionFactory;
      private readonly ILogger _log;
      private readonly IPEndPoint _endPoint;
      
      private Thread _mainThread;
      private TcpListener _listener;

      public SmtpServer( ISmtpSessionFactory sessionFactory, ILogger log, IPAddress address, int port )
         : this( sessionFactory, log, new IPEndPoint( address, port ) )
      {
      }

      public SmtpServer( ISmtpSessionFactory sessionFactory, ILogger log, IPEndPoint endPoint )
      {
          if (sessionFactory == null) throw new ArgumentNullException(nameof(sessionFactory));
         if (log == null) throw new ArgumentNullException(nameof(log));
         if (endPoint == null) throw new ArgumentNullException(nameof(endPoint));
         _sessionFactory = sessionFactory;
          _log = log;
          _endPoint = endPoint;
      }

      public void Stop()
      {
         if ( _listener != null )
         {
            _listener.Stop();
         }
         IsRunning = false;
         if( null != _mainThread )
         {
            if( !_mainThread.Join( 5000 ) )
            {
#if !NETCORE
					_mainThread.Abort();
#endif
                }
            }
      }

      public bool IsRunning { get; private set; }

      /// <summary>
      /// Starts the server on a separate thread.
      /// </summary>
      public void Start()
      {
         if ( IsRunning )
            throw new InvalidOperationException( "Server already started." );

         _mainThread = new Thread( Run );
         _mainThread.Start();
      }

      /// <summary>
      /// Runs the server on the current thread.
      /// </summary>
      public void Run()
      {
         _listener = new TcpListener(_endPoint) { ExclusiveAddressUse = false };
         _listener.Start();

         IsRunning = true;

         try
         {
            while ( IsRunning )
            {
               Socket clientSocket;
               try
               {
                  clientSocket = _listener.Server.Accept();
               }
               catch (SocketException ex)
               {
                  // ignore SocketError.Interrupted. This will always be thrown when server is stopped.
                  if ( ex.SocketErrorCode != System.Net.Sockets.SocketError.Interrupted )
                     throw;
                  break;
               }
               
               var connection = new NetworkConnection( clientSocket, _log );
               var session = _sessionFactory.CreateSession(connection,
                                                            ((IPEndPoint) _listener.LocalEndpoint).Address,
                                                            ((IPEndPoint) clientSocket.RemoteEndPoint).Address);

               var task = Task.Factory
                  .StartNew( s => ((SmtpSession)s).Process(), session );

               var logTask = task.ContinueWith( t => _log.Error( "SMTP session failed", t.Exception ), TaskContinuationOptions.OnlyOnFaulted );
               
               // always clean up
               logTask.ContinueWith( t => session.Dispose() /*could throw */ );

            }
         }
         catch ( InvalidOperationException ) { }
         finally
         {
            _listener.Stop();
         }
      }     
   }
}
