﻿using System.Text.RegularExpressions;

namespace Alanta.Smtp.Commands
{
   public class HeloCommand : SmtpCommandBase
   {
      private static readonly Regex match = new Regex( "HELO\\s+(.*)", RegexOptions.IgnoreCase );
      public HeloCommand( ) : base( match, SmtpSession.States.WaitForEhlo )
      {
      }

      public override void ProcessCommand( ISmtpSessionContext context, string line )
      {
         context.ProtocolName = "SMTP";
         context.ProvidedRemoteName = match.Match( line ).Groups[1].Value;
         context.Respond( "250 Ok" );
      }
   }
}
