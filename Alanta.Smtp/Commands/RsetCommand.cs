﻿using System.Text.RegularExpressions;

namespace Alanta.Smtp.Commands
{
   public class RsetCommand : SmtpCommandBase
   {
      private static readonly Regex match = new Regex( "RSET", RegexOptions.IgnoreCase );

      public RsetCommand( ) : base( match, null )
      {
      }

      public override void ProcessCommand( ISmtpSessionContext context, string line )
      {
         context.AbortTransaction();

         switch ( context.State )
         {
            case SmtpSession.States.RcptToReceived:
            case SmtpSession.States.MailFromReceived:
               context.Respond( "250 OK", SmtpSession.States.Ready );
               break;
            default:
               context.Respond( "250 OK" );
               break;
         }
      }
   }
}
