﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace Alanta.Smtp.Commands
{
   public abstract class SmtpCommandBase : ISmtpCommand
   {
      private readonly Regex _regex;
      private readonly SmtpSession.States[] _validStates;

      protected SmtpCommandBase( Regex regex, params SmtpSession.States[] validStates )
      {
         if ( regex == null ) throw new ArgumentNullException( nameof(regex) );
         _regex = regex;
         _validStates = validStates;
      }

      public virtual bool TryMatchCommand( string line )
      {
         return _regex.IsMatch( line );
      }

      public virtual bool TryProcessCommand( ISmtpSessionContext context, string line )
      {
         if( null != _validStates && _validStates.Length > 0 && !_validStates.Contains( context.State ) )
         {
            return false;
         }
         ProcessCommand( context, line );
         return true;
      }

      public abstract void ProcessCommand( ISmtpSessionContext context, string line );
   }
}
