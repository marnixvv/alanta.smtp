﻿namespace Alanta.Smtp.Commands
{
   public interface ISmtpCommand
   {
      bool TryMatchCommand( string line );
      bool TryProcessCommand( ISmtpSessionContext context, string line );
   }
}
