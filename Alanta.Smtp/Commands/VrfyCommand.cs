using System.Text.RegularExpressions;

namespace Alanta.Smtp.Commands
{
   public class VrfyCommand : SmtpCommandBase, IEhloExtension
   {
      private static Regex _regex = new Regex( "VRFY\\s+.*", RegexOptions.IgnoreCase );

      public VrfyCommand(): base( _regex, SmtpSession.States.WaitForEhlo, SmtpSession.States.Ready )
      {
      }

      public override void ProcessCommand( ISmtpSessionContext context, string line )
      {
         context.Respond( "252 Not allowed" );
      }

      public string GetEhloContent( ISmtpSessionContext context )
      {
         return "VRFY";
      }
   }
}