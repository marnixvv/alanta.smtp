﻿using System.Text.RegularExpressions;

namespace Alanta.Smtp.Commands
{
   public class MailFromCommand : SmtpCommandBase, IEhloExtension
   {
      private static readonly Regex _regex = new Regex( "MAIL FROM:.*", RegexOptions.IgnoreCase );
      private static readonly Regex _mailFromParser = new Regex( @"MAIL FROM:\s*\<(?<path>[^\>]+)\>(?:\s+BODY=(?<body>\w+))?", RegexOptions.IgnoreCase );

      public MailFromCommand() : base( _regex, SmtpSession.States.Ready )
      {
      }

      public override void ProcessCommand( ISmtpSessionContext context, string line )
      {
         var match = _mailFromParser.Match( line );
         if ( !match.Success )
         {
            context.Respond( "501 Syntax error in parameters or arguments" );
            return;
         }

         var body = match.Groups["body"].Value;

         if ( !string.IsNullOrEmpty( body ) && body != "7BIT" && body != "8BITMIME" )
         {
            context.Respond( "501 Syntax error: invalid BODY argument" );
         }

         var transaction = context.BeginTransaction();
         transaction.From = match.Groups["path"].Value;
         transaction.IsMime = body.Equals( "8BITMIME" );

         context.Respond( string.Format( "250 <{0}>... Sender {1}ok", transaction.From, transaction.IsMime ? "and 8BITMIME " : "" ), SmtpSession.States.MailFromReceived );
      }

      public string GetEhloContent( ISmtpSessionContext context )
      {
         return "8BITMIME";
      }
   }
}
