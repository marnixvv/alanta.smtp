﻿using System;
using System.Text.RegularExpressions;

namespace Alanta.Smtp.Commands
{
   public class StartTlsCommand : SmtpCommandBase, IEhloExtension
   {
      private static Regex _regex = new Regex( "STARTTLS", RegexOptions.IgnoreCase );
      public StartTlsCommand() : base( _regex, SmtpSession.States.Ready )
      {
      }

      public override void ProcessCommand( ISmtpSessionContext context, string line )
      {
         if ( context.ServerCertificate == null || context.IsEncrypted )
         {
            context.Respond( "454 TLS not available due to temporary reason" );
            return;
         }

         if( !"STARTTLS".Equals( line, StringComparison.OrdinalIgnoreCase) )
         {
            context.Respond( "501 Syntax error (no parameters allowed)" );
            return;
         }

         context.Respond( "220 Ready to start TLS" );
         if( context.StartTls().Result ) // TODO : Async all the way down...
         {
            context.Reset();
         }
      }

      public string GetEhloContent( ISmtpSessionContext context )
      {
         return ( context.ServerCertificate != null && !context.IsEncrypted ) ? "STARTTLS" : null;
      }
   }
}
