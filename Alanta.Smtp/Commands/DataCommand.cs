﻿using System.Text.RegularExpressions;

namespace Alanta.Smtp.Commands
{
   public class DataCommand : SmtpCommandBase, IEhloExtension
   {
      private static readonly Regex match = new Regex( "DATA", RegexOptions.IgnoreCase );
      public DataCommand() : base( match, SmtpSession.States.RcptToReceived )
      {
      }

      public override void ProcessCommand( ISmtpSessionContext context, string line )
      {
         context.Respond( 
            string.Format( "354 {0}, end with <CRLF>.<CRLF>", context.CurrentTransaction.IsMime ? "Send 8BITMIME message" : "Start mail input" ), 
            SmtpSession.States.ReceivingData );
      }

      public string GetEhloContent( ISmtpSessionContext context )
      {
         return context.MaximumMessageSize.HasValue ? string.Format( "SIZE {0}", context.MaximumMessageSize ) : null;
      }
   }
}
