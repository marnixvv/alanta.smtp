using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Alanta.Sasl;

namespace Alanta.Smtp.Commands
{
   public class AuthCommand : SmtpCommandBase, IEhloExtension
   {
      private readonly ISaslFactory _mechanisms;

      public AuthCommand( ISaslFactory mechanisms ) : base( new Regex( "AUTH .+" ), SmtpSession.States.Ready )
      {
         _mechanisms = mechanisms;
      }

      public override void ProcessCommand(ISmtpSessionContext context, string line)
      {
         if (context.IsAuthenticated)
         {
            context.Respond( "503 Bad sequence of commands. Already authenticated." );
            return;
         }

         // parse out the mechanism
         var mechanismName = line.Substring(5);
         var mechanism =
            _mechanisms.GetMechanism( mechanismName );

         if (null == mechanism)
         {
            context.Respond("501 Syntax error in parameters or arguments");
            return;
         }

         if (!context.IsEncrypted && mechanism.RequiresTls)
         {
            context.Respond("538 5.7.11 Encryption required for requested authentication mechanism");
            return;
         }

         bool done;
         SaslResult result = SaslResult.Unknown;
         string authorizedUser = null;

         EventHandler<SaslEventArgs> handler = (m, args) =>
         {
            done = true;
            authorizedUser = args.AuthorizedUser;
            result = args.Result;
         };

         try
         {
            mechanism.AuthorizationResult += handler;

            // Initial challenge (may be empty)
            var challenge = "334 " + mechanism.GetChallenge(null, out done);

            while (!done)
            {
               // SASL challenge response loop
               context.Respond(challenge);
               var response = context.Receive();
               challenge = mechanism.GetChallenge(response, out done);
            }

         }
         finally
         {
            mechanism.AuthorizationResult -= handler;
         }

         context.IsAuthenticated = result == SaslResult.Success;
         context.AuthorizedUser = authorizedUser;

         switch (result)
         {
            case SaslResult.Success:
               context.Respond("235 2.7.0 Authentication successful");
               break;
            case SaslResult.Unknown:
               context.Respond("454 4.7.0 Temporary authentication failure");
               break;
            default:
               context.Respond("535 5.7.8 Authentication credentials invalid");
               break;
         }
      }

      public string GetEhloContent(ISmtpSessionContext context)
      {
         var mechanisms = _mechanisms.GetSupportedMechanisms( context.IsEncrypted );

         return mechanisms.Any() ? "AUTH " + string.Join(" ", mechanisms) : null;
      }
   }
}