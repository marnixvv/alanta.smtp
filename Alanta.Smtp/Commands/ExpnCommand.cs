using System.Text.RegularExpressions;

namespace Alanta.Smtp.Commands
{
   public class ExpnCommand : ISmtpCommand, IEhloExtension
   {
      private static Regex _regex = new Regex( "EXPN\\s+.*", RegexOptions.IgnoreCase );

      public bool TryMatchCommand( string line )
      {
         return _regex.IsMatch( line );
      }

      public bool TryProcessCommand( ISmtpSessionContext context, string line )
      {
         if ( context.State == SmtpSession.States.WaitForEhlo || context.State == SmtpSession.States.Ready )
         {
            context.Respond( "252 Not allowed" );
            return true;
         }
         return false;
      }

      public string GetEhloContent( ISmtpSessionContext context )
      {
         return "EXPN";
      }
   }
}