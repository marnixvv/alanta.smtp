namespace Alanta.Smtp.Commands
{
   public interface IEhloExtension
   {
      string GetEhloContent( ISmtpSessionContext context );
   }
}