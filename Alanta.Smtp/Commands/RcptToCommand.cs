﻿using System;
using System.Text.RegularExpressions;

namespace Alanta.Smtp.Commands
{
   public class RcptToCommand : SmtpCommandBase
   {
      private static readonly Regex _regex = new Regex( "RCPT TO:.*", RegexOptions.IgnoreCase );

      public RcptToCommand()
         : base( _regex, SmtpSession.States.MailFromReceived )
      {
      }

      public override void ProcessCommand( ISmtpSessionContext context, string line )
      {
         var match = Regex.Match( line, @"RCPT TO:\s*\<?(?<Recipient>[^\s\>\n]*)\>?" );
         if ( !match.Success )
         {
            context.Respond( "501 Syntax error in parameters or arguments" );
            return;
         }

         var recipient = match.Groups["Recipient"].Value;

         var targetAddress = Regex.Match( recipient, @"(?:(@[a-z0-9]+(\.[a-z0-9][a-z0-9\-]*[a-z0-9])+,?)+:)?\""?(?<LocalPart>[a-z0-9\-+=!#$%^&*()~:;,\.]+)\""?@(?<Domain>[a-z0-9]+(\.[a-z0-9][a-z0-9\-]*[a-z0-9])+)" ); // Local-part @ doman (LDH rule)
         if ( targetAddress.Success )
         {
            var sanitizedRecipient = targetAddress.Groups["LocalPart"] + "@" + targetAddress.Groups["Domain"];
            if ( !context.PerformRestrictionChecks( sanitizedRecipient ) )
               return;

            context.CurrentTransaction.To.Add( sanitizedRecipient );
         }
         else if ( "postmaster".Equals( recipient, StringComparison.OrdinalIgnoreCase ) )
         {
            context.CurrentTransaction.To.Add( "postmaster" );
         }
         else
         {
            context.Respond( "501 Invalid recipient address" );
            return;
         }
         context.Respond( "250 OK", SmtpSession.States.RcptToReceived );
      }
   }
}
