﻿using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Alanta.Smtp.Commands
{
   public class EhloCommand : SmtpCommandBase
   {
      private static readonly Regex match = new Regex( "EHLO\\s+(.*)", RegexOptions.IgnoreCase );
      public EhloCommand() : base( match, SmtpSession.States.WaitForEhlo )
      {
      }

      public override void ProcessCommand( ISmtpSessionContext context, string line )
      {
         context.ProtocolName = "ESMTP";
         context.ProvidedRemoteName = match.Match( line ).Groups[1].Value;
         context.Respond( GetEhloResponse( context ), SmtpSession.States.Ready);
      }

      private string GetEhloResponse( ISmtpSessionContext context )
      {
         var headers = new List<string> { context.HostName };

         headers.AddRange( context.GetEhloContent() );

         return FormatMultiLineResponse( "250", headers );
      }

      private static string FormatMultiLineResponse( string statusCode, IList<string> lines )
      {
         var sb = new StringBuilder();
         for ( int index = 0; index < lines.Count; index++ )
         {
            sb.Append( statusCode );
            sb.Append( index == lines.Count - 1 ? " " : "-" );
            sb.AppendLine( lines[index] );
         }
         return sb.ToString();
      }
   }
}
