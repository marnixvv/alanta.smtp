using System;
using System.Collections.Generic;
using System.Text;

namespace Alanta.Smtp
{
   /// <summary>
   /// Represents an SMTP request to transfer a message.
   /// </summary>
   public class MessageTransaction
   {
      public MessageTransaction()
      {
         To = new List<string>();
         Content = new StringBuilder();
         MessageId = Guid.NewGuid();
      }
      public Guid MessageId { get; set; }
      public bool IsMime { get; set; }
      public string From { get; set; }
      public ICollection<string> To { get; private set; }
      public StringBuilder Content { get; private set; }
      public DateTime Received { get; set; }
      public bool SenderRestrictionsVerified { get; set; }
      public bool RelayRestrictionsVerified { get; set; }
      public bool RecipientRestrictionsVerified { get; set; }
   }
}