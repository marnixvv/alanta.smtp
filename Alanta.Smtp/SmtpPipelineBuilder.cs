﻿using System;
using Alanta.Sasl;
using Alanta.Smtp.Commands;
using Alanta.Smtp.Rules;

namespace Alanta.Smtp
{
   public static class SmtpPipelineBuilder
   {
      /// <summary>
      /// Creates a pipeline with the minimal required commands according to RFC 2801.
      /// </summary>
      /// <returns>An Smtp Pipeline</returns>
      public static ISmtpPipeline CreatePipeline()
      {
         return
            new SmtpPipeline(new ISmtpCommand[]
            {
               new HeloCommand(),
               new EhloCommand(),
               new RsetCommand(),
               new VrfyCommand(),
               new ExpnCommand(),
               new MailFromCommand(),
               new RcptToCommand(),
               new DataCommand(),
            })
            {
               RelayRestrictions =
               {
                  new Permit_My_Networks(),
                  new Reject_Unauth_Destination()
               }
            };
      }

      /// <summary>
      /// Enables TLS encryption support.
      /// </summary>
      /// <param name="pipeline">The pipeline.</param>
      /// <returns>The pipeline</returns>
      public static ISmtpPipeline EnableTls( this ISmtpPipeline pipeline )
      {
         if ( pipeline == null ) 
            throw new ArgumentNullException( nameof(pipeline) );

         pipeline.RegisterCommand( new StartTlsCommand() );

         return pipeline;
      }

      public static ISmtpPipeline EnableAuthentication(this ISmtpPipeline pipeline, IAuthenticationService authenticationService, IAuthorizationService authorizationService = null)
      {
         return pipeline.EnableAuthentication(new DefaultSaslFactory( authenticationService, authorizationService ?? new DefaultAuthorizationService() ));
      }

      public static ISmtpPipeline EnableAuthentication( this ISmtpPipeline pipeline, ISaslFactory saslFactory )
      {
         if ( pipeline == null ) throw new ArgumentNullException(nameof(pipeline));
         if (saslFactory == null) throw new ArgumentNullException(nameof(saslFactory));

         pipeline.RegisterCommand( new AuthCommand( saslFactory ) );
         pipeline.RelayRestrictions.Insert(0, new Accept_Authenticated());

         return pipeline;
      }
   }
}
