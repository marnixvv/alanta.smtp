﻿using System;
using Alanta.Smtp.Logging;
using Alanta.Smtp.Routing;

namespace Alanta.Smtp.ContentProcessing
{
   public class DefaultMessageProcessor : IMessageProcessor
   {
	   private readonly ILogger _logger;
	   private readonly MessageRouteTable _messageRoutes;

      public DefaultMessageProcessor( MessageRouteTable messageRoutes, ILogger logger )
      {
         if ( messageRoutes == null ) throw new ArgumentNullException( nameof(messageRoutes) );
         _messageRoutes = messageRoutes;
			if (logger == null) throw new ArgumentNullException(nameof(logger));
			_logger = logger;
		}

      public void Submit( SmtpMessage message )
      {
         if ( message == null ) throw new ArgumentNullException( nameof(message) );
         // todo: add sanity checks
         _logger.Info("Submit message <{0:N}> from {1} to {2}", message.MessageId, message.From, string.Join( ", ", message.To ?? new string [0] ));

         // todo: support content scanning

         // simple routing
         foreach ( var recipient in message.To )
         {
            foreach (var route in _messageRoutes )
            {
               var context = new MessageRoutingContext( message, recipient, this );
               if ( route.CanHandleMessage( context ) )
               {
                  route.HandleMessage( context );
               }
            }
         }
      }
   }
}
