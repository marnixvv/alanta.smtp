﻿using System.Globalization;
using Alanta.Smtp.Dns;

namespace Alanta.Smtp.ContentProcessing
{
   // RFC 2821 section 4.4
   public class TraceInformation
   {
      private readonly IDnsResolver _dnsResolver;

      public TraceInformation( IDnsResolver dnsResolver )
      {
         _dnsResolver = dnsResolver;
      }

      public string GetReceivedHeaderTemplate( SmtpMessage message, string recipient )
      {
         return string.Format( CultureInfo.InvariantCulture,
                               @"from {0} ({1} [{2}])
	by {3} (Alanta.Smtp) with {4} id {5}
	for {6}; {7:ddd, dd MMM yyyy HH:mm:ss zzz}",
                               message.ProvidedRemoteName, _dnsResolver.Resolve( message.RemoteIP ), 
                               message.RemoteIP, message.LocalHostName, message.ProtocolName,
                               message.MessageId,
                               recipient,
                               message.Received );
      }
   }
}
