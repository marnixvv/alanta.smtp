﻿namespace Alanta.Smtp.ContentProcessing
{
   public interface IMessageProcessor
   {
      void Submit( SmtpMessage message );
   }
}
