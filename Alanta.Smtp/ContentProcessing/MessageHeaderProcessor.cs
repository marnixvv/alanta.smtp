using System.Text;
using System.Text.RegularExpressions;

namespace Alanta.Smtp.ContentProcessing
{
   public class MessageContentProcessor
   {
      private readonly StringBuilder _content;

      public MessageContentProcessor( StringBuilder content )
      {
         _content = content;
      }

      private class HeaderAndPosition
      {
         public int start;
         public int end;
         public string line;
      }

      /// <summary>
      /// Add required headers for delivery.
      /// </summary>
      /// <param name="returnPath"></param>
      /// <param name="overwriteReturnPath"></param>
      /// <param name="traceInformation"></param>
      /// <returns>The prepared message contents.</returns>
      public StringBuilder PrepareForDelivery( string returnPath, bool overwriteReturnPath, string traceInformation )
      {
         var processed = new StringBuilder( _content.Length + ( traceInformation != null ? traceInformation.Length + 4 : 0 ) );
         var header = GetHeaderInternal( "Return-Path" );
         if( null == header || overwriteReturnPath )
         {
            AddHeader( processed, "Return-Path", returnPath );
         }
         else
         {
            processed.Append( NormalizeAndTerminateWithCrLf( header.line ) );
         }
         if ( !string.IsNullOrEmpty( traceInformation ) )
         {
            AddHeader( processed, "Received", traceInformation );
         }

         if( header != null && header.start > 0 )
         {
            processed.Append( _content.ToString( 0, header.start ) );
         }
         var start = header == null ? 0 : header.end + 1;
         processed.Append( _content.ToString( start, _content.Length - start ) );
         return processed;
      }

      private void AddHeader( StringBuilder sb, string name, string content )
      {
         sb.Append( name );
         sb.Append( ": " );
         sb.Append( NormalizeAndTerminateWithCrLf( content ) );
      }

      private static string NormalizeAndTerminateWithCrLf( string input )
      {
         if( string.IsNullOrEmpty( input ))
         {
            return string.Empty;
         }

         var normalized = Regex.Replace( input, @"\r\n|\n\r|\n|\r", "\r\n" ).Trim(new[]{' ','\t'});
         // todo: break long lines
         // todo: ensure wrapped lines start with spacing
         // todo: remove duplicate \r\n to prevent empty lines
         return normalized.EndsWith( "\r\n" ) ? normalized : normalized + "\r\n";
      }

      private HeaderAndPosition GetHeaderInternal( string name )
      {
         int index = 0;
         int lineStart = 0;

         while( index < _content.Length )
         {
            if( _content[index] != '\n')
            {
               index++;
               continue;
            }

            // Verify header line
            var length = index - lineStart;

            if( length < 2 )
            {
               break; // empty line marks end of header section - RFC 2822 section 2.1
            }

            // Look ahead to handle folded lines
            if ( index < _content.Length - 1 && ( _content[index + 1] == ' ' || _content[index + 1] == '\t' ) )
            {
               index++; // folded line, continue reading
               continue;
            }

            if( length < name.Length || _content[lineStart+name.Length] != ':' )
            {
               index++;
               lineStart = index;
               continue;
            }

            var found = true;
            for ( var j = 0; j < name.Length; j++ )
            {
               if( _content[j+lineStart] != name[j])
               {
                  found = false;
                  break;
               }
            }
            if( found )
            {
               var result = new char[length];
               _content.CopyTo( lineStart, result, 0, result.Length );
               return new HeaderAndPosition
                         {
                            start = lineStart,
                            end = index,
                            line = new string( result ).Replace( "\r", "" ).Replace( "\n", "" ).Trim() // unfold line
                         };
            }

            index++;
            lineStart = index;
         }
         return null;
      }
   }
}