using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Alanta.Smtp.ContentProcessing
{
   public class SmtpMessage
   {
      public Guid MessageId { get; set; }
      public string From { get; set; }
      public ICollection<string> To { get; set; }
      public StringBuilder Content { get; set; }
      public DateTime Received { get; set; }
      public string LocalHostName { get; set; }
      public string ProtocolName { get; set; }
      public string ProvidedRemoteName { get; set; }
      public IPAddress RemoteIP { get; set;  }
   }
}