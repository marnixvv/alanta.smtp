﻿using System;

namespace Alanta.Smtp
{
   public class SmtpException : Exception
   {
      public SmtpException( string message )
         : base( message )
      {

      }

      public SmtpException( string message, Exception innerException )
         : base( message, innerException )
      {

      }
   }
}
