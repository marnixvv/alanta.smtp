using System;
using System.Text.RegularExpressions;
using Alanta.Smtp.ContentProcessing;
using Alanta.Smtp.Logging;

namespace Alanta.Smtp
{
   /// <summary>
   /// A state machine that processes a single SMTP session.
   /// Within a session, multiple message transactions can be performed.
   /// </summary>
   public class SmtpSession : IDisposable
   {
      private readonly IMessageProcessor _messageProcessor;
	   private readonly ILogger _logger;
	   private readonly ISmtpSessionContext _context;

	   public SmtpSession(ISmtpSessionContext context, IMessageProcessor messageProcessor, ILogger logger)
	   {
		   if (context == null) throw new ArgumentNullException(nameof(context));
		   if (messageProcessor == null) throw new ArgumentNullException(nameof(messageProcessor));
		   if (logger == null) throw new ArgumentNullException(nameof(logger));
		   _context = context;
		   _messageProcessor = messageProcessor;
		   _logger = logger;

		   _context.SetState(States.Connect);
	   }

	   public void Process()
      {
         try
         {
            while ( _context.IsConnected )
            {
               if ( _context.State == States.Connect )
               {
                  Initiate();
               }
               string line = _context.Receive();
               if ( string.IsNullOrEmpty( line ) )
                  continue;

               Command command = Command.NOOP;

               if ( _context.State != States.ReceivingData )
               {
                  if ( !SimpleCommandParser.TryParse( line, out command ) )
                  {
                     if ( !_context.Pipeline.TryInvokePipeline( _context, line ) )
                     {
                        _context.Respond( "500 Syntax error, command unrecognized" );
                     }
                     continue;
                  }
               }

               if ( _context.State != States.ReceivingData && command == Command.QUIT )
               {
                  _context.Respond( "221 Service closing transmission channel", States.Disconnected );
                  break;
               }

               switch (_context.State)
               {
                  case States.WaitForEhlo:
                     break;
                  case States.Ready:
                     break;
                  case States.MailFromReceived:
                     break;
                  case States.RcptToReceived:
                     break;
                  case States.ReceivingData:
                     if ( line.Trim() == "." )
                     {
                        // Verify content
                        // May need to process mime
                        var message = new SmtpMessage
                                         {
                                            Content = _context.CurrentTransaction.Content,
                                            From = _context.CurrentTransaction.From,
                                            LocalHostName = _context.HostName,
                                            MessageId = _context.CurrentTransaction.MessageId,
                                            ProtocolName = _context.ProtocolName,
                                            ProvidedRemoteName = _context.ProvidedRemoteName,
                                            Received = DateTime.Now,
                                            RemoteIP = _context.RemoteIP,
                                            To = _context.CurrentTransaction.To
                                         };
                        try
                        {
                           _messageProcessor.Submit( message );
                        }
                        catch (Exception ex)
                        {
                           _logger.Error("Message processing failed!", ex);
                           _context.Respond( "451 Requested action aborted: local error in processing", States.Ready );
                           continue;
                        }
                        
                        _context.Respond( "250 OK", States.Ready );
                     }
                     else
                     {
                        _context.CurrentTransaction.Content.AppendLine( line );
                     }
                     continue;
                  // States.WaitForQuit
                  default:
                     break;
               }
               if ( command == Command.NOOP )
               {
                  _context.Respond( "250 OK" );
                  continue;
               }
               _context.Respond( "503 bad sequence of commands" );
            }

            Disconnect();
         }
         catch ( Exception exception )
         {
            throw new SmtpException( "Error while processing SMTP session", exception );
         }
      }

      private enum Command
      {
         NOOP,
         QUIT
      }

      private static class SimpleCommandParser
      {
         private static readonly Regex _quit = new Regex(@"\s?QUIT\s?", RegexOptions.IgnoreCase);
         private static readonly Regex _noop = new Regex( @"\s?NOOP\s?", RegexOptions.IgnoreCase );

         public static bool TryParse( string input, out Command command )
         {
            command = Command.NOOP;

            if ( _quit.IsMatch( input ) )
            {
               command = Command.QUIT;
               return true;
            }
            if ( _noop.IsMatch( input ) )
            {
               command = Command.NOOP;
               return true;
            }

            return false;
         }
      }

      public void Dispose()
      {
         _context.Dispose();
      }

      private void Initiate()
      {
         if ( _acceptConnections )
         {
            _context.Respond( "220 Server Ready", States.WaitForEhlo );
         }
         else
         {
            _context.Respond( "554 Server not accepting connections", States.WaitForQuit );
         }
      }

      private void Disconnect()
      {
         _context.SetState( States.Disconnected );
      }

      public enum States
      {
         Connect,
         WaitForQuit,
         WaitForEhlo,
         Disconnected,
         MailFromReceived,
         RcptToReceived,
         Ready,
         ReceivingData
      }

      private bool _acceptConnections = true;

      public ISmtpSessionContext SmtpSessionContext { get { return _context; } }
   }
}