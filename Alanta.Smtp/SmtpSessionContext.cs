using System;
using System.Collections.Generic;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Alanta.Smtp.Rules;

namespace Alanta.Smtp
{
   public class SmtpSessionContext : ISmtpSessionContext
   {
      private readonly IServerConnection _connection;

      public SmtpSessionContext( IServerConnection connection, ISmtpPipeline pipeline )
      {
         if ( connection == null ) throw new ArgumentNullException( nameof(connection) );
         if ( pipeline == null ) throw new ArgumentNullException( nameof(pipeline) );

         _connection = connection;
         Pipeline = pipeline;
         ProtocolName = "SMTP";
         MessageId = Guid.NewGuid();
      }

      public string Receive()
      {
          return _connection.ReadLine();
      }

      public void Respond( string response, SmtpSession.States? nextState = null )
      {
         _connection.Send( response );
         if ( nextState.HasValue )
         {
            State = nextState.Value;
         }
      }

      public ISmtpPipeline Pipeline { get; }
      public X509Certificate ServerCertificate { get; set; }
      public string ProtocolName { get; set; }
      public string ProvidedRemoteName { get; set; }
      public Guid MessageId { get; set; }

      public SmtpSession.States State { get; private set; }
      public UInt32? MaximumMessageSize { get; set; }
      public string HostName { get; set; }
      public bool IsEncrypted { get; private set; }

      public bool IsAuthenticated { get; set; }
      public string AuthorizedUser { get; set; }

      public MessageTransaction BeginTransaction()
      {
         CurrentTransaction = new MessageTransaction();
         return CurrentTransaction;
      }

      public void AbortTransaction()
      {
         CurrentTransaction = null;
      }

      public async Task<bool> StartTls()
      {
         if ( null == ServerCertificate )
            throw new InvalidOperationException("ServerCertificate not set.");

         if ( IsEncrypted )
         {
            throw new InvalidOperationException( "Connection is already encrypted." );
         }

         IsEncrypted = await _connection.StartTls( ServerCertificate );
         return IsEncrypted;
      }

      public void Reset()
      {
         State = SmtpSession.States.WaitForEhlo;
      }

      private bool _sessionRestrictionsChecked { get; set; }

      public bool PerformRestrictionChecks( string recipient )
      {
         var ruleContext = Pipeline.CreateRuleContext( this, recipient );

         if ( !PerformSessionChecks( ruleContext ) )
            return false;
         if ( !PerformMailFromChecks( ruleContext ) )
            return false;

         var result = Pipeline.RelayRestrictions.Evaluate( ruleContext );
         switch ( result )
         {
            case RuleResult.Rejected:
               Respond( "551 Relay access denied" );
               return false;
            case RuleResult.Deferred:
               Respond( "421 Try again later" );
               return false;
            case RuleResult.Accepted:
               break;
         }

         result = Pipeline.RecipientRestrictions.Evaluate( ruleContext );
         switch ( result )
         {
            case RuleResult.Rejected:
               Respond( "541 Recipient address rejected: "+recipient );
               return false;
            case RuleResult.Deferred:
               Respond( "421 Try again later" );
               return false;
            case RuleResult.Accepted:
               break;
         }

         return true;
      }

      private bool PerformSessionChecks(IRuleContext context)
      {
         if (_sessionRestrictionsChecked)
            return true;

         var result = Pipeline.ClientRestrictions.Evaluate( context );
         switch ( result )
         {
            case RuleResult.Rejected:
               Respond( "550 Access Denied to You" );
               return false;
            case RuleResult.Deferred:
               Respond( "421 Try again later" );
               return false;
            case RuleResult.Accepted:
               break;
         }

         result = Pipeline.HeloRestrictions.Evaluate( context );
         switch ( result )
         {
            case RuleResult.Rejected:
               Respond( "550 Access Denied to You" );
               return false;
            case RuleResult.Deferred:
               Respond( "421 Try again later" );
               return false;
            case RuleResult.Accepted:
               break;
         }

         _sessionRestrictionsChecked = true;
         return true;
      }

      private bool PerformMailFromChecks(IRuleContext context)
      {
         var result = Pipeline.SenderRestrictions.Evaluate( context );
        switch ( result )
        {
           case RuleResult.Rejected:
              Respond( "554 Relay access denied for "+context.From );
              return false;
           case RuleResult.Deferred:
              Respond( "421 Try again later" );
              return false;
           case RuleResult.Accepted:
              break;
        }

         return true;
      }

      public IEnumerable<string> GetEhloContent()
      {
         return Pipeline.GetEhloContent( this );
      }

      public MessageTransaction CurrentTransaction { get; private set; }
      public bool IsConnected { get { return _connection.Connected; } }
      public IPAddress LocalIP { get; set; }
      public IPAddress RemoteIP { get; set; }
		
		/// <summary>
		/// The session id. This is mainly for traceability in logs.
		/// </summary>
		public Guid Id { get; set; }

	   public void SetState( SmtpSession.States value )
      {
         State = value;
         if (value == SmtpSession.States.Disconnected)
         {
            _connection.Disconnect();
         }
      }

       public void Dispose()
       {
           _connection.Dispose();
       }
   }
}